INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 45 1 * * MON-FRI', 'marketStatus', 'Update', '{\"time\":\"014500\",\"market\":\"HNX\",\"status\":\"ATO\",\"type\":\"DERIVATIVES\"}', NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 0 2 * * MON-FRI', 'marketStatus', 'Update', '{\"time\":\"020000\",\"market\":\"HNX\",\"status\":\"LO\",\"type\":\"DERIVATIVES\"}', NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 30 4 * * MON-FRI', 'marketStatus', 'Update', '{\"time\":\"043000\",\"market\":\"HNX\",\"status\":\"INTERMISSION\",\"type\":\"DERIVATIVES\"}', NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 0 6 * * MON-FRI', 'marketStatus', 'Update', '{\"time\":\"060000\",\"market\":\"HNX\",\"status\":\"LO\",\"type\":\"DERIVATIVES\"}', NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 30 7 * * MON-FRI', 'marketStatus', 'Update', '{\"time\":\"073000\",\"market\":\"HNX\",\"status\":\"ATC\",\"type\":\"DERIVATIVES\"}', NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 45 7 * * MON-FRI', 'marketStatus', 'Update', '{\"time\":\"074500\",\"market\":\"HNX\",\"status\":\"CLOSED\",\"type\":\"DERIVATIVES\"}', NULL, b'1');


INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 0 11 * * MON-FRI', 'marketJob-v2', '/job/storeDailyFuturesList', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 30 0 * * MON-FRI', 'marketJob-v2', '/job/updateAdjustedPrice', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 0 18,19,20 * * MON-FRI', 'marketJob-v2', '/job/removeAutoData', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 30 */1 * * MON-FRI', 'marketJob-v2', '/job/updateDividend', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 0 11 * * MON-FRI', 'marketJob-v2', '/job/storeAutoData', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 */30 * * * MON-FRI', 'marketJob-v2', '/job/syncIndexStockList', NULL, NULL, b'1');


INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 40 1 * * MON-FRI', 'collector-v2', '/job/realtime/start', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 35 4 * * MON-FRI', 'collector-v2', '/job/realtime/stop', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 55 5 * * MON-FRI', 'collector-v2', '/job/realtime/start', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 15 8 * * MON-FRI', 'collector-v2', '/job/realtime/stop', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 30 1 * * MON-FRI', 'collector-v2', '/job/getTblFile', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 15 1 * * MON-FRI', 'collector-v2', '/job/getTblFile', NULL, NULL, b'1');

INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 0 18,19,20 * * MON-FRI', 'realtimeJob-v2', '/job/resetRedisCache', NULL, NULL, b'1');
INSERT INTO `tradex-job`.`t_cron`(`cron`, `destination_topic`, `destination_uri`, `destination_data_json`, `script_to_run`, `enable`) VALUES ('0 15 8,9 * * MON-FRI', 'realtimeJob-v2', '/job/saveRedisToDatabase', NULL, NULL, b'1');

