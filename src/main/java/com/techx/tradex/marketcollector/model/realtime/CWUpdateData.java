package com.techx.tradex.marketcollector.model.realtime;

import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;

@Data
public class CWUpdateData extends StockUpdateData {
    private SymbolTypeEnum type = SymbolTypeEnum.CW;
}
