package com.techx.tradex.marketcollector.model.realtime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.ImmutableMap;
import com.techx.tradex.common.model.kafka.DefaultPartitionBody;
import com.techx.tradex.common.utils.Transform;
import com.techx.tradex.htsconnection.socket.message.receive.FuturesBidOfferItem;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.utils.NumberUtils;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FuturesBidOfferData extends TransformData<FuturesBidOfferItem, FuturesBidOfferData> implements DefaultPartitionBody {
    static Map<String, String> sessionControlMap = ImmutableMap.of(
            "P", "ATO",
            "A", "ATC",
            "O", "CONTINUOUS"
    );
    private double bidPrice;
    private double offerPrice;
    private long bidVolume;
    private long offerVolume;
    private List<PriceItem> bidOfferList;
    private long totalOfferVolume;
    private long totalOfferCount;
    private long totalBidVolume;
    private long totalBidCount;
    private Double expectedPrice;
    private String session;
    private SymbolTypeEnum type = SymbolTypeEnum.FUTURES;

    @Override
    public void parse(FuturesBidOfferItem item) {
        this.setCode(item.getCode().getValue());
        this.setTime(item.getTime().getValue());
        this.setBidPrice(NumberUtils.round2Decimal(item.getBidPrice().getValue()));
        this.setOfferPrice(NumberUtils.round2Decimal(item.getOfferPrice().getValue()));
        this.setBidVolume(item.getBidVolume().getValue());
        this.setOfferVolume(item.getOfferVolume().getValue());
        this.setBidOfferList(item.getBidOfferList().stream().map(bo -> new PriceItem().from(bo)).collect(Collectors.toList()));
        this.setTotalOfferVolume(item.getAccumulateOfferVolume().getValue());
        this.setTotalOfferCount(item.getAccumulateOfferCount().getValue());
        this.setTotalBidVolume(item.getAccumulateBidVolume().getValue());
        this.setTotalBidCount(item.getAccumulateBidCount().getValue());
        this.setSession(sessionControlMap.get(item.getControlCode().getValue()));
        String session = this.getSession();
        double expectedPrice = item.getProjectOpen().getValue();
        if (!(session.equalsIgnoreCase("ATO") || session.equalsIgnoreCase("ATC"))
                || expectedPrice <= 0) {
            this.expectedPrice = null;
        } else {
            this.expectedPrice = NumberUtils.round2Decimal(expectedPrice);
        }
    }

    @Data
    public static class PriceItem implements Transform<FuturesBidOfferItem.SubItem, PriceItem> {
        private double bidPrice;
        private double offerPrice;
        private long bidVolume;
        private long offerVolume;
        private long bidVolumeChange;
        private long offerVolumeChange;

        @Override
        public void parse(FuturesBidOfferItem.SubItem item) {
            this.setOfferPrice(NumberUtils.round2Decimal(item.getOfferPrice().getValue()));
            this.setBidPrice(NumberUtils.round2Decimal(item.getBidPrice().getValue()));
            this.setBidVolume(item.getBidVolume().getValue());
            this.setOfferVolume(item.getOfferVolume().getValue());
            this.setBidVolumeChange(item.getBidVolumeChange().getValue());
            this.setOfferVolumeChange(item.getOfferVolumeChange().getValue());
        }
    }
}
