package com.techx.tradex.marketcollector.model.response;

import com.techx.tradex.marketcollector.model.db.EstimatedData;
import lombok.Data;

@Data
public class EstimatedCeilFloorResponse {
    private double ceilingPrice;
    private double floorPrice;

    public EstimatedData toEstimatedData() {
        EstimatedData estimatedData = new EstimatedData();
        estimatedData.setCeilingPrice(this.getCeilingPrice());
        estimatedData.setFloorPrice(this.getFloorPrice());
        return estimatedData;
    }
}
