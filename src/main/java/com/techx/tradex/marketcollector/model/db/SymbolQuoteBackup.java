package com.techx.tradex.marketcollector.model.db;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document(collection = "c_symbol_quote_backup")
public class SymbolQuoteBackup extends SymbolQuote {

}
