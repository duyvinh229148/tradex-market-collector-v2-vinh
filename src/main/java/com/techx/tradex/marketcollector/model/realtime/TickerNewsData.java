package com.techx.tradex.marketcollector.model.realtime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techx.tradex.common.constants.MarketTypeEnum;
import com.techx.tradex.common.model.kafka.DefaultPartitionBody;
import com.techx.tradex.htsconnection.socket.message.receive.A78AutoItem;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

/* Market Status*/
@Data
public class TickerNewsData extends TransformData<A78AutoItem, TickerNewsData> implements DefaultPartitionBody {
    private static final Logger log = LoggerFactory.getLogger(TickerNewsData.class);
    @JsonIgnore
    private byte click;
    @JsonIgnore
    private byte color;
    @JsonIgnore
    private String childType;
    @JsonIgnore
    private int id;
    @JsonIgnore
    private int sendDate;
    @JsonIgnore
    private int tickerTime;
    @JsonIgnore
    private String shortCode;
    @JsonIgnore
    private String support;

    private String title;
    private String market;
    private String status;
    private String type = "EQUITY";


    @Override
    public void parse(A78AutoItem item) {
        this.setClick(item.getClick().getValue());
        this.setColor(item.getColor().getValue());
        this.setChildType(item.getChildType().getValue());
        this.setId(item.getId().getValue());
        this.setSendDate(item.getDate().getValue());
        this.setTickerTime(item.getTime().getValue());
        this.setCode(item.getCode().getValue());
        this.setTitle(item.getTitle().getValue());
        this.setSupport(item.getSupport().getValue());
        this.setTime(item.getTime().getValue() + "");
    }

    @Override
    public void formatTime() {
    }

    @Override
    public void parseStatus(Map<String, String> statusMap) {
        this.setType("EQUITY");
        String market;
        if (this.getTitle().contains(MarketTypeEnum.HNX.name())) {
            market = MarketTypeEnum.HNX.name();
        } else if (this.getTitle().contains(MarketTypeEnum.HOSE.name())) {
            market = MarketTypeEnum.HOSE.name();
        } else if (this.getTitle().contains(MarketTypeEnum.UPCOM.name())) {
            market = MarketTypeEnum.UPCOM.name();
        } else {
            log.error("Ignore A78 cause of market not found");
            throw new IgnoreException();
        }
        String status = statusMap.get(this.getTitle());
        if (status == null) {
            log.error("Ignore cause of status not found");
            throw new IgnoreException();
        }
        this.setStatus(status);
        this.setMarket(market);
        String dateStr = DateFormatUtils.format(new Date(), "yyyyMMdd");
        String timeStr = StringUtils.leftPad(this.getTime(), 6, "0");
        try {
            Date date = DateUtils.parseDate(dateStr + timeStr, "yyyyMMddHHmmss");
            Date utcDate = DateUtils.addHours(date, -7);
            String utcTime = DateFormatUtils.format(utcDate, "HHmmss");
            this.setTime(utcTime);
        } catch (ParseException e) {
            log.error("Error parsing time {0}", e);
        }
    }
}
