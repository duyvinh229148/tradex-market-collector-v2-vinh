package com.techx.tradex.marketcollector.model.realtime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CWBidOfferData extends BidOfferData {
    private SymbolTypeEnum type = SymbolTypeEnum.CW;
}
