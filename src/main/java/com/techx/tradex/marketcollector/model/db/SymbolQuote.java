package com.techx.tradex.marketcollector.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Data
@Document(collection = "c_symbol_quote")
public class SymbolQuote implements MappingMongo {
    @Id
    @JsonIgnore
    protected String id;
    protected String code;
    protected String refCode;
    protected SymbolTypeEnum type;
    protected String time;
    protected double open;
    protected double high;
    protected double low;
    protected double last;
    protected double change;
    protected double rate;
    protected String highTime;
    protected String lowTime;
    protected int bidPrice;
    protected int offerPrice;
    protected int ceilingPrice;
    protected int floorPrice;
    protected int averagePrice;
    protected int referencePrice;
    protected long tradingVolume;
    protected long tradingValue;
    protected double turnoverRate;
    protected int matchingVolume;
    protected int bidVolume;
    protected int offerVolume;
    protected long totalBidVolume; //accumulateBidVolume
    protected long totalBidCount; //accumulateBidCount
    protected long totalOfferVolume; //accumulateOfferVolume
    protected long totalOfferCount; //accumulateOfferCount
    protected long foreignerBuyVolume;
    protected long foreignerSellVolume;
    protected long foreignerTotalRoom;
    protected long foreignerCurrentRoom;
    protected String matchedBy;
    protected String ceilingFloorEqual;
    protected long holdVolume; //Total Room - Remaining Room
    protected double holdRatio; //Hold Volume / Listed Quantity
    protected double buyAbleRatio; //Remaining Room/Total Room
    protected Date date;
    protected long milliseconds;
    protected int sequence;

}
