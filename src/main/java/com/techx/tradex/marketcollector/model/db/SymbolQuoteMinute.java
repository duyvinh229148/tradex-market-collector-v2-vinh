package com.techx.tradex.marketcollector.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Data
@Document(collection = "c_symbol_quote_minute")
public class SymbolQuoteMinute implements MappingMongo {
    @Id
    @JsonIgnore
    protected String id;
    protected String code;
    protected String refCode;
    protected double open;
    protected double high;
    protected double low;
    protected double last;
    protected Long tradingVolume;
    protected Long tradingValue;
    protected Long periodTradingVolume;
    protected Date date;
}
