package com.techx.tradex.marketcollector.model.request;

import com.techx.tradex.common.utils.DefaultUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.List;

@Data
public class DownloadRequest {
    private String fromDate;
    private String toDate;
    private boolean isOverride = true;
    private List<String> symbolList;

    public String getFromDate() {
        return StringUtils.isBlank(this.fromDate) ? DefaultUtils.DATE_FORMAT().format(new Date()) : this.fromDate;
    }

    public String getToDate() {
        return StringUtils.isBlank(this.toDate) ? DefaultUtils.DATE_FORMAT().format(new Date()) : this.toDate;
    }

}
