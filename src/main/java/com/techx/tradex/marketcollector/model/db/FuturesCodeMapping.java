package com.techx.tradex.marketcollector.model.db;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "c_futures_code_mapping")
public class FuturesCodeMapping {
    @Id
    private String refCode;
    private String code;
    private String oldCode;
    private Date updatedDate;
}
