package com.techx.tradex.marketcollector.model.db;

import lombok.Data;

@Data
public class EstimatedData {
    private double ceilingPrice;
    private double floorPrice;
}
