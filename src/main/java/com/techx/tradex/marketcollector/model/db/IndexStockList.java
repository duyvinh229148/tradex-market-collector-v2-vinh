package com.techx.tradex.marketcollector.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "c_index_stock_list")
public class IndexStockList implements MappingMongo {
    @Id
    private String indexCode;
    private List<String> stockList;
    private Date updatedAt;


    @Override
    public String getId() {
        return this.indexCode;
    }

    public Map<String, Object> toMapUpdate() {
        Map<String, Object> map = new HashMap<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(this));
            } catch (Exception e) {
                System.out.println("error while convert IndexStockList to map: {0}" + e);
            }
        }
        return map;
    }
}
