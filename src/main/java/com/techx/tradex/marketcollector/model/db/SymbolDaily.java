package com.techx.tradex.marketcollector.model.db;

import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@Document(collection = "c_symbol_daily")
public class SymbolDaily implements MappingMongo {
    @Id
    private String id;
    private String code;
    private Date date;
    //    private boolean isAdjusted;
    private Double price;
    private Double change;
    private Double rate;
    private Long tradingVolume;
    private Long tradingValue;
    private Double open;
    private Double high;
    private Double low;
    private Double last;
    private String marketType;
    private String securitiesType;
    private String ceilingFloorEqual;
    private SymbolTypeEnum type;


    // FUTURES
    private Double mBasis;
    private Double tBasis;
    private Double tPrice;
    private Long openInterest;
    private Double index;

    // CW
    private Long underlyingPrice;
    private Double underlyingRate;

    // INDEX
    private String refCode;

    private Date createdAt;
    private Date updatedAt;

    public Map<String, Object> toMapUpdate() {
        Map<String, Object> map = new HashMap<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(this));
            } catch (Exception e) {
                System.out.println("error while convert symbolDaily to map: {0}" + e);
            }
        }
        return map;
    }
}
