package com.techx.tradex.marketcollector.model.realtime;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuoteData {
    private String s; // symbol code
    private String t; // symbol type : INDEX/STOCK/FUTURES/CW
    private String ti; // time
    private Double o; // open price
    private Double h; // high price
    private Double l; // low price
    private Double c; // close price
    private Double a; // average price
    private Double ep; // expected price
    private Double ch; // change
    private Double ra; // rate
    private Double ba; // basis
    private Long vo; // volume
    private Long va; // value
    private Long mv; // matching volume
    private String mb; // match by
    private List<BestBid> bb; // best bid
    private List<BestOffer> bo; // best offer
    private String ss; // session
    private Long tb; // total bid volume
    private Long to; // total offer volume
    private IndexChange ic; // index change
    private ForeignerData fr; // foreigner data

    public static Object fromQuoteDataUpdate(TransformData data) {
        if (data instanceof StockUpdateData || data instanceof FuturesUpdateData || data instanceof IndexUpdateData) {
            QuoteData quoteData = new QuoteData();
            quoteData.s = data.getCode();
            quoteData.ti = data.getTime();
            quoteData.o = data.getOpen();
            quoteData.h = data.getHigh();
            quoteData.l = data.getLow();
            quoteData.c = data.getLast();
            quoteData.ch = data.getChange();
            quoteData.ra = data.getRate();
            quoteData.vo = data.getTradingVolume();
            quoteData.va = data.getTradingValue();
            quoteData.mv = data.getMatchingVolume();

            if (data instanceof StockUpdateData) {
                quoteData.t = ((StockUpdateData) data).getType().name();
                quoteData.a = ((StockUpdateData) data).getAveragePrice();
                quoteData.mb = ((StockUpdateData) data).getMatchedBy();
                quoteData.tb = ((StockUpdateData) data).getTotalBidVolume();
                quoteData.to = ((StockUpdateData) data).getTotalOfferVolume();
                ForeignerData fr = new ForeignerData();
                fr.setBv(((StockUpdateData) data).getForeignerBuyVolume());
                fr.setSv(((StockUpdateData) data).getForeignerSellVolume());
                fr.setTr(((StockUpdateData) data).getForeignerTotalRoom());
                fr.setCr(((StockUpdateData) data).getForeignerCurrentRoom());
                quoteData.fr = fr;
            }
            if (data instanceof IndexUpdateData) {
                quoteData.t = ((IndexUpdateData) data).getType().name();
                IndexChange ic = new IndexChange();
                ic.setCe(((IndexUpdateData) data).getCeilingCount());
                ic.setFl(((IndexUpdateData) data).getFloorCount());
                ic.setDown(((IndexUpdateData) data).getDownCount());
                ic.setUp(((IndexUpdateData) data).getUpCount());
                ic.setUc(((IndexUpdateData) data).getUnchangedCount());
                quoteData.ic = ic;
            }
            if (data instanceof FuturesUpdateData) {
                quoteData.t = ((FuturesUpdateData) data).getType().name();
                quoteData.a = ((FuturesUpdateData) data).getAveragePrice();
                quoteData.mb = ((FuturesUpdateData) data).getMatchedBy();
                quoteData.tb = ((FuturesUpdateData) data).getTotalBidVolume();
                quoteData.to = ((FuturesUpdateData) data).getTotalOfferVolume();
                ForeignerData fr = new ForeignerData();
                fr.setBv(((FuturesUpdateData) data).getForeignerBuyVolume());
                fr.setSv(((FuturesUpdateData) data).getForeignerSellVolume());
                quoteData.fr = fr;
                quoteData.ba = ((FuturesUpdateData) data).getBasis();
            }
            return quoteData;
        } else if (data instanceof BidOfferData || data instanceof FuturesBidOfferData) {
            QuoteData quoteData = new QuoteData();
            quoteData.s = data.getCode();
            quoteData.ti = data.getTime();

            if (data instanceof BidOfferData) {
                quoteData.t = ((BidOfferData) data).getType().name();
                quoteData.tb = ((BidOfferData) data).getTotalBidVolume();
                quoteData.to = ((BidOfferData) data).getTotalOfferVolume();
                List<BestBid> bestBids = new ArrayList<>();
                List<BestOffer> bestOffers = new ArrayList();
                List<BidOfferData.PriceItem> bidOfferList = ((BidOfferData) data).getBidOfferList();
                if (bidOfferList != null && bidOfferList.size() > 0) {
                    for (int i = 0; i < bidOfferList.size(); i++) {
                        BidOfferData.PriceItem priceItem = bidOfferList.get(i);
                        BestBid bb = new BestBid();
                        bb.setP(priceItem.getBidPrice());
                        bb.setV(priceItem.getBidVolume());
                        bb.setC(priceItem.getBidVolumeChange());
                        bestBids.add(bb);

                        BestOffer bo = new BestOffer();
                        bo.setP(priceItem.getOfferPrice());
                        bo.setV(priceItem.getOfferVolume());
                        bo.setC(priceItem.getOfferVolumeChange());
                        bestOffers.add(bo);

                    }
                    quoteData.bb = bestBids;
                    quoteData.bo = bestOffers;
                }
                quoteData.ep = ((BidOfferData) data).getExpectedPrice();
                quoteData.ss = ((BidOfferData) data).getSession();
            }
            if (data instanceof FuturesBidOfferData) {
                quoteData.t = ((FuturesBidOfferData) data).getType().name();
                quoteData.tb = ((FuturesBidOfferData) data).getTotalBidVolume();
                quoteData.to = ((FuturesBidOfferData) data).getTotalOfferVolume();
                List<BestBid> bestBids = new ArrayList<>();
                List<BestOffer> bestOffers = new ArrayList();
                List<FuturesBidOfferData.PriceItem> bidOfferList = ((FuturesBidOfferData) data).getBidOfferList();
                if (bidOfferList != null && bidOfferList.size() > 0) {
                    for (int i = 0; i < bidOfferList.size(); i++) {
                        FuturesBidOfferData.PriceItem priceItem = bidOfferList.get(i);
                        BestBid bb = new BestBid();
                        bb.setP(priceItem.getBidPrice());
                        bb.setV(priceItem.getBidVolume());
                        bb.setC(priceItem.getBidVolumeChange());
                        bestBids.add(bb);

                        BestOffer bo = new BestOffer();
                        bo.setP(priceItem.getOfferPrice());
                        bo.setV(priceItem.getOfferVolume());
                        bo.setC(priceItem.getOfferVolumeChange());
                        bestOffers.add(bo);
                    }
                }
                quoteData.bb = bestBids;
                quoteData.bo = bestOffers;
                quoteData.ss = ((FuturesBidOfferData) data).getSession();
                quoteData.ep = ((FuturesBidOfferData) data).getExpectedPrice();
            }
            return quoteData;
        }
        return data;
    }

}

@Data
class ForeignerData {
    private Long bv; // buy volume
    private Long sv; // sell volume
    private Long cr; // current room
    private Long tr; // total room
}

@Data
class IndexChange {
    private int ce; // ceiling count
    private int fl; // floor count
    private int up; // up count
    private int down; // down count
    private int uc; // unChange count
}

@Data
class BestBid {
    private Double p; // price
    private Long v; // volume
    private Long c; // volume change
}

@Data
class BestOffer {
    private Double p; // price
    private Long v; // volume
    private Long c; // volume change
}