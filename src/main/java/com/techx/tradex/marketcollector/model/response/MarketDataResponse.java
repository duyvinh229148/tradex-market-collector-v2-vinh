package com.techx.tradex.marketcollector.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.techx.tradex.common.utils.DefaultUtils;
import com.techx.tradex.marketcollector.constants.Constants;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import com.techx.tradex.marketcollector.model.db.SymbolPrevious;
import com.techx.tradex.marketcollector.utils.NumberUtils;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MarketDataResponse {
    private String s; // code
    private String r; // refCode
    private String m; // market
    private String n1; // name
    private String n2; // nameEn
    private String t; // type (INDEX/STOCK/CW/FUTURES/ETF/FUND)
    private double re; // reference price
    private double ce; // ceiling price
    private double fl; // floor price
    private double pc; // previous close price
    public Double ep; // exercise price
    public String er; // exercise ratio
    public Double de; // delta
    public Long lq; // listed quantity

    private String md; // maturity date or endDate (only for futures and cw)

    // only for index
    private Boolean i; // isHighlight

    // only for futures
    private String ftd; // first trading date
    private String b; // baseCode or cw underlying symbol
    private String bs; // baseCodeSecuritiesType

    // only for cw
    private String ltd; // last trading date (only for cw)
    private String is; // issuer name


    public static MarketDataResponse fromSymbolInfo(SymbolInfo symbolInfo, SymbolPrevious symbolPrevious) {
        MarketDataResponse res = new MarketDataResponse();
        res.setS(symbolInfo.getCode());
        res.setM(symbolInfo.getMarketType());
        res.setN1(symbolInfo.getName());
        res.setN2(symbolInfo.getNameEn());
        res.setT(symbolInfo.getType().name());
        res.setLq(symbolInfo.getListedQuantity());

        if (symbolInfo.getType().equals(SymbolTypeEnum.INDEX)) {
            res.setI(symbolInfo.getIsHighlight() != 0 && symbolInfo.getIsHighlight() != Constants.DEFAULT_HIGHLIGHT_NUMBER.intValue());
        } else {
            res.setRe(NumberUtils.round2Decimal(symbolInfo.getReferencePrice()));
            res.setCe(NumberUtils.round2Decimal(symbolInfo.getCeilingPrice()));
            res.setFl(NumberUtils.round2Decimal(symbolInfo.getFloorPrice()));
            if (symbolInfo.getType().equals(SymbolTypeEnum.STOCK)) {
                if (StringUtils.isNoneBlank(symbolInfo.getSecuritiesType())) {
                    res.setT(symbolInfo.getSecuritiesType());
                }
            }
            if (symbolInfo.getType().equals(SymbolTypeEnum.CW)) {
                res.setMd(DefaultUtils.DATE_FORMAT().format(symbolInfo.getMaturityDate()));
                res.setLtd(DefaultUtils.DATE_FORMAT().format(symbolInfo.getLastTradingDate()));
                res.setB(symbolInfo.getUnderlyingSymbol());
                res.setBs(SymbolTypeEnum.STOCK.name());
                res.setIs(symbolInfo.getIssuerName());
                res.setEp(symbolInfo.getExercisePrice());
                res.setEr(symbolInfo.getExerciseRatio());
                res.setDe(NumberUtils.round2Decimal(symbolInfo.getDelta()));
            }
            if (symbolInfo.getType().equals(SymbolTypeEnum.FUTURES)) {
                res.setMd(DefaultUtils.DATE_FORMAT().format(symbolInfo.getMaturityDate()));
                res.setFtd(DefaultUtils.DATE_FORMAT().format(symbolInfo.getFirstTradingDate()));
                res.setB(symbolInfo.getBaseCode());
                res.setBs(symbolInfo.getBaseCodeSecuritiesType());
                res.setR(symbolInfo.getRefCode());
            }
        }
        if (symbolPrevious != null) {
            if (symbolPrevious.getPreviousClose() != null) {
                res.setPc(NumberUtils.round2Decimal(symbolPrevious.getPreviousClose()));
            }
        }
        return res;
    }

}
