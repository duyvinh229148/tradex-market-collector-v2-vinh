package com.techx.tradex.marketcollector.model.db;

import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Field;
import java.util.*;


@Data
@Document(collection = "c_symbol_info")
public class SymbolInfo implements MappingMongo {
    // common
    @Id
    private String code;
    private SymbolTypeEnum type; // STOCK/INDEX/FUTURES/CW

    private String name;
    private String nameEn;
    private String marketType;
    private String securitiesType;
    private String time;
    private Double open;
    private Double high;
    private Double low;
    private Double last;
    private Double change;
    private Double rate;
    private Long tradingVolume;
    private Long tradingValue;

    private int sequence; // count quote update

    private Date updatedAt;
    private Date createdAt;

    // FUTURES/STOCK/CW
    private Double expectedPrice;
    private String ceilingFloorEqual;
    private Double ceilingPrice;
    private Double floorPrice;
    private Double referencePrice;
    private Double averagePrice;
    private String highTime;
    private String lowTime;
    private Long ptVolume;
    private Double turnoverRate;
    private Long listedQuantity;
    private Double highPrice52Week;
    private Double lowPrice52Week;
    private Double bidPrice;
    private Double offerPrice;
    private Long totalBidVolume;
    private Long totalOfferVolume;
    private String industry;

    // STOCK + CW + INDEX
    private Long priorTradingVolume;


    // STOCK + FUTURES
    private String rights;
    private Integer parValue;
    private Long foreignerBuyVolume;
    private Long foreignerSellVolume;
    private Long foreignerTotalRoom;
    private Long foreignerCurrentRoom;

    // STOCK
    private Long ptValue;
    private Integer bidVolume;
    private Integer offerVolume;
    private Long totalBidCount;
    private Long totalOfferCount;
    private EstimatedData estimatedData;


    // FIX data:
    private String fixSecurityType;
    private String cfiCode;
    private String currency;
    private String securityExchange;
    private Integer roundLot;
    private Long minTradeVolume;
    private Integer contractMultiplier;


    // CW
    private String issuerName;
    private Double exercisePrice;
    private String exerciseRatio;
    private Double breakEven;
    private Long impliedVolatility;
    private Long parity;
    private Double tPrice;
    private Double delta;
    private Double gearingRt;
    private Double capitalFulcrumPoint;
    private String underlyingSymbol;
    private Integer underlyingPrice;
    private Integer underlyingChange;
    private Double underlyingRate;
    private Date lastTradingDate;

    // FUTURES/CW
    private Date maturityDate;
    private Long totalTradingValue;
    private Long totalTradingVolume;
    private String controlCode;
    private Integer changeOfTotalBidVolume;
    private Integer changeOfTotalOfferVolume;
    private Integer diffBidOffer;
    private Long accumulateBidVolume;
    private Long accumulateBidCount;
    private Long accumulateOfferVolume;
    private Long accumulateOfferCount;


    // FUTURES/INDEX
    private String refCode;

    // FUTURES
    private Date firstTradingDate;
    private String marketName;
    private Long priorVolume;
    private Long ptTradingValue;
    private List<HighLowYearItem> highLowYearData = new ArrayList<>();
    private List<BidOfferItem> bidOfferList = new ArrayList<>();
    private String baseCode;
    private String baseCodeSecuritiesType;
    private Integer openInterest;
    private Integer openInterestChange;
    private Long normalForeignerBuyVolume;
    private Long normalForeignerBuyValue;
    private Long normalForeignerSellVolume;
    private Long normalForeignerSellValue;
    private Long ptForeignerTotalBuyVolume;
    private Long ptForeignerTotalBuyValue;
    private Long ptForeignerTotalSellVolume;
    private Long ptForeignerTotalSellValue;
    private Integer remainDate;
    private Double theoryPrice;
    private Double basis;
    private Double theoryBasis;
    private Double marketBasis;
    private Double disparate;
    private Double disparateRate;
    private String exchange;

    // INDEX
    private Integer upCount;
    private Integer ceilingCount;
    private Integer downCount;
    private Integer floorCount;
    private Integer unchangedCount;
    private Integer isHighlight;

    @Data
    public static class HighLowYearItem {
        private Integer highPrice;
        private String dateOfHighPrice;
        private Integer lowPrice;
        private String dateOfLowPrice;
    }

    @Data
    public static class BidOfferItem {
        private Double bidPrice;
        private Integer bidVolume;
        private Integer bidVolumeChange;
        private Double offerPrice;
        private Integer offerVolume;
        private Integer offerVolumeChange;
    }

    @Data
    public static class SymbolInfoOverride {
        private String companyName;
        private String companyNameEn;
        private String market;
        private String lastTradingDate;
        private String securitiesType;
        private String industry;
        private Integer referencePrice;
        private Integer ceilingPrice;
        private Integer floorPrice;
        private Long listedQuantity;
        private Integer parValue;
        private Long priorTradingVolume;
        private String rights;
        private Long ptVolume;
        private Long ptValue;
        private Integer open;
        private Integer high;
        private Integer low;
        private Integer last;
        private Integer averagePrice;
        private String highTime;
        private String lowTime;
        private Integer change;
        private Double rate;
        private Long tradingVolume;
        private Long tradingValue;
        private Double turnoverRate;
        private Long foreignerBuyVolume;
        private Long foreignerSellVolume;
        private Long foreignerTotalRoom;
        private Long foreignerCurrentRoom;
        private Integer bidPrice;
        private Integer offerPrice;
        private Integer bidVolume;
        private Integer offerVolume;
        private Long totalBidVolume;
        private Long totalBidCount;
        private Long totalOfferVolume;
        private Long totalOfferCount;
        private Double highPrice52Week;
        private Double lowPrice52Week;

        // FIX data:
        private String fixSecurityType;
        private String cfiCode;
        private String currency;
        private String securityExchange;
        private Integer roundLot;
        private Long minTradeVolume;
        private Integer contractMultiplier;
    }

    @Override
    public String getId() {
        return this.code;
    }

    public Map<String, Object> toMapUpdate() {
        Map<String, Object> map = new HashMap<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(this));
            } catch (Exception e) {
                System.out.println("error while convert symbolInfo to map: {0}" + e);
            }
        }
        return map;
    }
}
