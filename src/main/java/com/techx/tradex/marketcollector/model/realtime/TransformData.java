package com.techx.tradex.marketcollector.model.realtime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techx.tradex.common.model.kafka.Body;
import com.techx.tradex.common.utils.Transform;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.services.CacheService;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Map;


public abstract class TransformData<S, D extends TransformData> implements Transform<S, D>, Body {
    private static final Logger log = LoggerFactory.getLogger(TransformData.class);

    private static Integer fromHour = null;
    private static Integer toHour = null;
    private static Integer hourOffset = null;
    protected static final DecimalFormat format = new DecimalFormat("##.00");
    protected static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    protected static final SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");

    public static void setAppConf(AppConf appConf) {
        fromHour = appConf.getRealtime().getMinHour();
        toHour = appConf.getRealtime().getMaxHour();
        hourOffset = appConf.getServerHourOffset();
    }

    @Getter
    @Setter
    protected String code;
    @Getter
    @Setter
    protected String time;
    @Getter
    @Setter
    protected Double open;
    @Getter
    @Setter
    protected Double high;
    @Getter
    @Setter
    protected Double low;
    @Getter
    @Setter
    protected Double last;
    @Getter
    @Setter
    protected Double change;
    @Getter
    @Setter
    protected Double rate;
    @Getter
    @Setter
    protected Long tradingVolume;
    @Getter
    @Setter
    protected Long tradingValue;
    @Getter
    @Setter
    protected Long matchingVolume;
    @Getter
    @Setter
    protected Integer sequence;
    @JsonIgnore
    @Getter
    @Setter
    private long timeInsertToQueue;


    @JsonIgnore
    @Getter
    @Setter
    protected String sendOut;

    public void formatTime() {
        try {
            StringBuilder sb = new StringBuilder(6);
            String[] temps = this.time.split(":");
            int hour = Integer.valueOf(temps[0]);
            if (hour < fromHour || hour > toHour) {
                log.error("out of working time");
                throw new IgnoreException();
            }
            hour = hour - hourOffset;
            if (hour < 10) {
                sb.append("0");
            }
            sb.append(hour);
            if (temps[1].length() < 2) {
                sb.append("0");
            }
            sb.append(temps[1]);
            if (temps[2].length() < 2) {
                sb.append("0");
            }
            sb.append(temps[2]);
            this.time = sb.toString();
        } catch (Exception e) {
            log.error("error on format time: {} {}", this, e);
            throw new IgnoreException();
        }
    }

    public void validate() {
    }

    public void formatRefCode(CacheService cacheService) {
    }

    public static class IgnoreException extends RuntimeException {
    }

    public void parseStatus(Map<String, String> statusMap) {
    }

    public Object toRealObject() {
        return null;
    }

}
