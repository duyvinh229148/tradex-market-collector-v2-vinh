package com.techx.tradex.marketcollector.model.realtime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techx.tradex.common.constants.StockQuoteCeilingFloorEqEnum;
import com.techx.tradex.common.constants.StockQuoteMatchByEnum;
import com.techx.tradex.common.model.kafka.DefaultPartitionBody;
import com.techx.tradex.htsconnection.socket.message.receive.StockAutoItem;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.utils.NumberUtils;
import com.techx.tradex.marketcollector.utils.QuoteUtils;
import lombok.Data;

@Data
public class StockUpdateData extends TransformData<StockAutoItem, StockUpdateData> implements DefaultPartitionBody {
    protected String highTime;
    protected String lowTime;
    protected double ceilingPrice;
    protected double floorPrice;
    protected double referencePrice;
    protected double averagePrice;
    protected double turnoverRate;
    protected double bidPrice;
    protected double offerPrice;
    protected long bidVolume;
    protected long offerVolume;
    protected long totalBidVolume; //accumulateBidVolume
    protected long totalBidCount; //accumulateBidCount
    protected long totalOfferVolume; //accumulateOfferVolume
    protected long totalOfferCount; //accumulateOfferCount

    protected long foreignerBuyVolume;
    protected long foreignerSellVolume;
    protected long foreignerTotalRoom;
    protected long foreignerCurrentRoom;

    protected String matchedBy;
    protected String ceilingFloorEqual;
    protected SymbolTypeEnum type = SymbolTypeEnum.STOCK;

    @JsonIgnore
    protected long timeInsertToQueue;

    @Override
    public Object toRealObject() {
        return this;
    }

    @Override
    public void validate() {
        if (this.open == 0 || this.high == 0 || this.low == 0 || this.last == 0) {
            throw new IgnoreException();
        }
    }

    @Override
    public void parse(StockAutoItem stockAutoItem) {
        this.setCode(stockAutoItem.getStockCode().getValue());
        this.setTime(stockAutoItem.getTime().getValue());
        this.setHighTime(QuoteUtils.convertAutoTime(stockAutoItem.getHighTime().getValue()));
        this.setLowTime(QuoteUtils.convertAutoTime(stockAutoItem.getLowTime().getValue()));
        this.setCeilingPrice(stockAutoItem.getCeiling().getValue());
        this.setFloorPrice(stockAutoItem.getFloor().getValue());
        this.setReferencePrice(stockAutoItem.getRefPrice().getValue());
        this.setAveragePrice(stockAutoItem.getAvgPrice().getValue());
        this.setOpen((double) stockAutoItem.getOpen().getValue());
        this.setHigh((double) stockAutoItem.getHigh().getValue());
        this.setLow((double) stockAutoItem.getLow().getValue());
        this.setLast((double) stockAutoItem.getLast().getValue());
        this.setChange((double) stockAutoItem.getChange().getValue());
        this.setRate(NumberUtils.round2Decimal(stockAutoItem.getRate().getValue()));
        this.setTurnoverRate(NumberUtils.round2Decimal(stockAutoItem.getTurnOverRate().getValue()));
        this.setMatchingVolume((long) stockAutoItem.getMatchVolume().getValue());
        this.setTradingVolume(stockAutoItem.getVolume().getValue());
        this.setTradingValue(stockAutoItem.getValue().getValue());
        this.setBidPrice(stockAutoItem.getBid().getValue());
        this.setOfferPrice(stockAutoItem.getOffer().getValue());
        this.setBidVolume(stockAutoItem.getBidVolume().getValue());
        this.setOfferVolume(stockAutoItem.getOfferVolume().getValue());
        this.setTotalBidVolume(stockAutoItem.getTotalBidVolume().getValue());
        this.setTotalBidCount(stockAutoItem.getTotalBidCount().getValue());
        this.setTotalOfferVolume(stockAutoItem.getTotalOfferVolume().getValue());
        this.setTotalOfferCount(stockAutoItem.getTotalOfferCount().getValue());
        this.setForeignerBuyVolume(stockAutoItem.getForeignerBuyVolume().getValue());
        this.setForeignerSellVolume(stockAutoItem.getForeignerSellVolume().getValue());
        this.setForeignerTotalRoom(stockAutoItem.getForeignerTotalRoom().getValue());
        this.setForeignerCurrentRoom(stockAutoItem.getForeignerCurrentRoom().getValue());

        if (stockAutoItem.getMatchVolume().getIndex() == 66) {
            this.matchedBy = StockQuoteMatchByEnum.BID.name();
        } else if (stockAutoItem.getMatchVolume().getIndex() == 83) {
            this.matchedBy = StockQuoteMatchByEnum.OFFER.name();
        } else {
            this.matchedBy = StockQuoteMatchByEnum.OFFER.name();
        }

        if (this.last == this.ceilingPrice) {
            this.ceilingFloorEqual = StockQuoteCeilingFloorEqEnum.CEILING.name();
        } else if (this.last == this.floorPrice) {
            this.ceilingFloorEqual = StockQuoteCeilingFloorEqEnum.FLOOR.name();
        } else {
            this.ceilingFloorEqual = null;
        }
    }

}
