package com.techx.tradex.marketcollector.model.realtime;

import com.techx.tradex.common.model.kafka.DefaultPartitionBody;
import com.techx.tradex.htsconnection.socket.message.receive.AdvertisedAutoItem;
import com.techx.tradex.marketcollector.utils.NumberUtils;
import lombok.Data;

@Data
public class AdvertisedData extends TransformData<AdvertisedAutoItem, AdvertisedData> implements DefaultPartitionBody {
    private String secId;
    private String traderId;
    private String sellBuyType;
    private double price;
    private int quantity;
    private int ptVolume;
    private int ptValue;
    private boolean isCancel;
    private String contact;

    @Override
    public void parse(AdvertisedAutoItem item) {
        this.setCode(item.getStockCode().getValue());
        this.setTime(item.getTime().getValue());
        this.setSecId(item.getSecId().getValue());
        this.setTraderId(item.getTraderId().getValue());
        this.setSellBuyType(item.getSellBuyType().getValue());
        this.setPrice(NumberUtils.round2Decimal(item.getPrice().getValue()));
        this.setQuantity(item.getQuantity().getValue());
        this.setPtVolume(item.getPutThroughVolume().getValue());
        this.setPtValue(item.getPutThroughValue().getValue());
        this.setContact(item.getContact().getValue());
        System.out.println("advertised cancel" + item.getAddOrCancelFlag().getValue());
        this.setCancel(false);
    }
}
