package com.techx.tradex.marketcollector.model.db;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@Document(collection = "c_foreigner_daily")
public class ForeignerDaily implements MappingMongo {
    @Id
    private String id;
    private String code;
    private Date date;
    private long foreignerChangeVolume;
    private double foreignerBuyAbleRatio;
    private long foreignerCurrentRoom;
    private long foreignerTotalRoom;
    private long foreignerTradingVolume;
    private long foreignerBuyVolume;
    private long foreignerSellVolume;
    private long foreignerHoldVolume;
    private double foreignerHoldRatio;
    private double listedQuantity;
    private Date createdAt;
    private Date updatedAt;


    public Map<String, Object> toMapUpdate() {
        Map<String, Object> map = new HashMap<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(this));
            } catch (Exception e) {
                System.out.println("error while convert foreignerDaily to map: {0}" + e);
            }
        }
        return map;
    }
}
