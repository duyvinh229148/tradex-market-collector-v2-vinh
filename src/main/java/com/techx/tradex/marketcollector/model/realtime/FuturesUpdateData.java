package com.techx.tradex.marketcollector.model.realtime;

import com.techx.tradex.common.constants.StockQuoteCeilingFloorEqEnum;
import com.techx.tradex.common.constants.StockQuoteMatchByEnum;
import com.techx.tradex.common.model.kafka.DefaultPartitionBody;
import com.techx.tradex.htsconnection.socket.message.receive.FuturesAutoItem;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.services.CacheService;
import com.techx.tradex.marketcollector.utils.NumberUtils;
import com.techx.tradex.marketcollector.utils.QuoteUtils;
import lombok.Data;

@Data
public class FuturesUpdateData extends TransformData<FuturesAutoItem, FuturesUpdateData> implements DefaultPartitionBody {
    private String refCode;
    private String highTime;
    private String lowTime;
    private double ceilingPrice;
    private double floorPrice;
    private String filler;
    private double referencePrice;
    private double averagePrice;
    private double basis;
    private double mBasis;
    private double tBasis;
    private double tPrice;
    private double disparity;
    private double disparityRate;
    private String filler2;
    private double bidPrice;
    private double offerPrice;
    private String filler3;
    private long bidVolume;
    private long offerVolume;
    private long totalBidVolume;
    private long totalBidCount;
    private long totalOfferVolume;
    private long totalOfferCount;
    private long foreignerBuyVolume;
    private long foreignerSellVolume;
    private String matchedBy;
    private String ceilingFloorEqual;
    private SymbolTypeEnum type = SymbolTypeEnum.FUTURES;

    @Override
    public Object toRealObject() {
        return this;
    }

    @Override
    public void validate() {
        if (this.open == 0 || this.high == 0 || this.low == 0 || this.last == 0) {
            throw new IgnoreException();
        }
    }

    @Override
    public void parse(FuturesAutoItem item) {
        this.setCode(item.getCode().getValue());
        this.setTime(item.getTime().getValue());
        this.setHighTime(QuoteUtils.convertAutoTime(item.getHighTime().getValue()));
        this.setLowTime(QuoteUtils.convertAutoTime(item.getLowTime().getValue()));
        this.setCeilingPrice(NumberUtils.round2Decimal(item.getCeilingPrice().getValue()));
        this.setFloorPrice(NumberUtils.round2Decimal(item.getFloorPrice().getValue()));
        this.setOpen(NumberUtils.round2Decimal(item.getOpen().getValue()));
        this.setHigh(NumberUtils.round2Decimal(item.getHigh().getValue()));
        this.setLow(NumberUtils.round2Decimal(item.getLow().getValue()));
        this.setLast(NumberUtils.round2Decimal(item.getLast().getValue()));
        this.setFiller(item.getFiller().getValue());
        this.setChange(NumberUtils.round2Decimal(item.getChange().getValue()));
        this.setReferencePrice(NumberUtils.round2Decimal(item.getReferencePrice().getValue()));
        this.setAveragePrice(NumberUtils.round2Decimal(item.getAveragePrice().getValue()));
        this.setBasis(NumberUtils.round2Decimal(item.getBasis().getValue()));
        this.setMBasis(NumberUtils.round2Decimal(item.getMBasis().getValue()));
        this.setTBasis(NumberUtils.round2Decimal(item.getTBasis().getValue()));
        this.setTPrice(NumberUtils.round2Decimal(item.getTheoryPrice().getValue()));
        this.setDisparity(NumberUtils.round2Decimal(item.getDisparate().getValue()));
        this.setDisparityRate(NumberUtils.round2Decimal(item.getDisparateRate().getValue()));
        this.setRate(NumberUtils.round2Decimal(item.getRate().getValue()));
        this.setMatchingVolume((long) item.getMatchVolume().getValue());
        this.setFiller2(item.getFiller2().getValue());
        this.setTradingVolume(item.getTradingVolume().getValue());
        this.setTradingValue(item.getTradingValue().getValue());
        this.setBidPrice(NumberUtils.round2Decimal(item.getBidPrice().getValue()));
        this.setOfferPrice(NumberUtils.round2Decimal(item.getOfferPrice().getValue()));
        this.setFiller3(item.getFiller3().getValue());
        this.setBidVolume(item.getBidVolume().getValue());
        this.setOfferVolume(item.getOfferVolume().getValue());
        this.setTotalBidVolume(item.getAccumulateBidVolume().getValue());
        this.setTotalBidCount(item.getAccumulateBidCount().getValue());
        this.setTotalOfferVolume(item.getAccumulateOfferVolume().getValue());
        this.setTotalOfferCount(item.getAccumulateOfferCount().getValue());
        this.setForeignerBuyVolume(item.getForeignerBuyVolume().getValue());
        this.setForeignerSellVolume(item.getForeignerSellVolume().getValue());

        if (item.getMatchVolume().getIndex() == 66) {
            this.matchedBy = StockQuoteMatchByEnum.BID.name();
        } else if (item.getMatchVolume().getIndex() == 83) {
            this.matchedBy = StockQuoteMatchByEnum.OFFER.name();
        } else {
            this.matchedBy = StockQuoteMatchByEnum.OFFER.name();
        }

        if (this.last == this.ceilingPrice) {
            this.ceilingFloorEqual = StockQuoteCeilingFloorEqEnum.CEILING.name();
        } else if (this.last == this.floorPrice) {
            this.ceilingFloorEqual = StockQuoteCeilingFloorEqEnum.FLOOR.name();
        } else {
            this.ceilingFloorEqual = null;
        }
    }

    @Override
    public void formatRefCode(CacheService cacheService) {
        this.refCode = cacheService.getFuturesCodeRefMap().get(this.code);
    }
}
