package com.techx.tradex.marketcollector.model.db;

import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@Document(collection = "c_symbol_previous")
public class SymbolPrevious implements MappingMongo {
    @Id
    private String code;
    private Date lastTradingDate;
    private Date previousTradingDate;
    private Double close;
    private Double previousClose;
    private String marketType;
    private SymbolTypeEnum type;
    private String refCode;
    private String note;
    private Date createdAt;
    private Date updatedAt;

    @Override
    public String getId() {
        return this.code;
    }

    public Map<String, Object> toMapUpdate() {
        Map<String, Object> map = new HashMap<>();
        for (Field field : this.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(this));
            } catch (Exception e) {
                System.out.println("error while convert SymbolPrevious to map: {0}" + e);
            }
        }
        return map;
    }
}
