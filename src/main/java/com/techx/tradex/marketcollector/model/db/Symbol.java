package com.techx.tradex.marketcollector.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.techx.tradex.common.constants.StockSecuritiesTypeEnum;
import com.techx.tradex.common.utils.Pair;
import com.techx.tradex.marketcollector.constants.Constants;
import com.techx.tradex.marketcollector.constants.MarketExchangeEnum;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "c_symbol")
public class Symbol {
    @Id
    private String code;
    private String refCode;
    private String exchange;
    private String marketType;
    private String name;
    private String nameEn;
    private SymbolTypeEnum type;
    private Integer isHighlight;
    private Date createdAt = new Date();

    public boolean isValid() {
        return this.code != null && this.name != null && this.nameEn != null
                && this.exchange != null && this.type != null;
    }

    public void setName(String name, boolean isEn) {
        if (isEn) {
            this.nameEn = name;
        } else {
            this.name = name;
        }
    }

    public String getName() {
        if (StringUtils.isBlank(this.name)) {
            this.name = this.getValidCode();
        }
        return this.name;
    }

    public String getNameEn() {
        if (StringUtils.isBlank(this.nameEn)) {
            this.nameEn = this.getValidCode();
        }
        return this.nameEn;
    }

    @JsonIgnore
    public String getSecuritiesType() {
        if (this.type.equals(SymbolTypeEnum.STOCK)) {
            if (this.exchange.equals(MarketExchangeEnum.HNX.getValue()) || this.exchange.equals(MarketExchangeEnum.UPCOM.getValue())
                    || this.exchange.equals(MarketExchangeEnum.HOSE.getValue())) {
                return StockSecuritiesTypeEnum.STOCK.getValue();
            }
            if (this.exchange.equals(MarketExchangeEnum.ETF_HOSE.getValue())) {
                return StockSecuritiesTypeEnum.ETF.getValue();
            }
            if (this.exchange.equals(MarketExchangeEnum.FUND_HOSE.getValue())) {
                return StockSecuritiesTypeEnum.FUND.getValue();
            }
        }
        return "";
    }

    @JsonIgnore
    public String getMarketType() {
        switch (type) {
            case INDEX: {
                return Constants.INDEX_EXCHANGES_MAP.get(this.getExchange());
            }
            case STOCK: {
                Pair<String, String> pair = Constants.STOCK_EXCHANGES_MAP.get(this.getExchange());
                if (pair != null) {
                    return pair.getLeft();
                }
                return "";
            }
            case CW: {
                return Constants.CW_EXCHANGES_MAP.get(this.getExchange());
            }
            case FUTURES: {
                return Constants.FUTURES_EXCHANGES_MAP.get(this.getExchange());
            }
            default:
                return "";
        }
    }

    @JsonIgnore
    public String getValidCode() {
        return this.refCode != null ? this.refCode : this.code;
    }
}
