package com.techx.tradex.marketcollector.model.realtime;

import com.techx.tradex.common.model.kafka.DefaultPartitionBody;
import com.techx.tradex.htsconnection.socket.message.receive.DealNoticeAutoItem;
import lombok.Data;

import java.util.Date;

@Data
public class DealNoticeData extends TransformData<DealNoticeAutoItem, DealNoticeData> implements DefaultPartitionBody {
    private String confirmNumber;
    private int matchPrice;
    private int matchVolume;
    private int ptVolume;
    private int ptValue;
    private boolean isCancel;

    @Override
    public void parse(DealNoticeAutoItem item) {
        this.setCode(item.getStockCode().getValue());
        this.setTime(item.getTime().getValue());
        this.setConfirmNumber(String.valueOf(item.getConfirmNo().getValue()));
        this.setMatchPrice(item.getPutThroughMatchPrice().getValue());
        this.setMatchVolume(item.getPutThroughMatchVolume().getValue());
        this.setPtVolume(item.getPutThroughVolume().getValue());
        this.setPtValue(item.getPutThrough().getValue());
        System.out.println("Deal notice is cancel" + item.getAddCancelFlag().getValue());
        this.setCancel(false);
    }

    public void formatTime() {
        Date date = new Date();
        this.time = sdfTime.format(date);
    }
}
