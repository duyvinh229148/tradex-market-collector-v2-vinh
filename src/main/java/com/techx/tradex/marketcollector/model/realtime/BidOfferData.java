package com.techx.tradex.marketcollector.model.realtime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.techx.tradex.common.model.kafka.DefaultPartitionBody;
import com.techx.tradex.common.utils.NumberUtils;
import com.techx.tradex.common.utils.Transform;
import com.techx.tradex.htsconnection.socket.message.receive.BidOfferAutoItem;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BidOfferData extends TransformData<BidOfferAutoItem, BidOfferData> implements DefaultPartitionBody {
    protected double bidPrice;
    protected double offerPrice;
    protected long bidVolume;
    protected long offerVolume;
    protected List<PriceItem> bidOfferList;
    protected long totalBidVolume;
    protected long totalOfferVolume;
    protected long totalBidCount;
    protected long totalOfferCount;
    protected long diffBidOffer;
    protected Double expectedPrice;
    protected String session;
    protected SymbolTypeEnum type = SymbolTypeEnum.STOCK;

    @Override
    public void parse(BidOfferAutoItem item) {
        this.setCode(item.getStockCode().getValue());
        this.setTime(item.getTime().getValue());
        this.setBidPrice(item.getBidPrice().getValue());
        this.setOfferPrice(item.getOfferPrice().getValue());
        this.setBidVolume(item.getBidVolume().getValue());
        this.setOfferVolume(item.getOfferVolume().getValue());
        this.setTotalBidVolume(item.getAccumulativeBidVolume().getValue());
        this.setTotalOfferVolume(item.getAccumulativeOfferVolume().getValue());
        this.setTotalBidCount(item.getAccumulativeBidCount().getValue());
        this.setTotalOfferCount(item.getAccumulativeOfferCount().getValue());
        this.setDiffBidOffer(item.getDiffBidOffer().getValue());
        this.setBidOfferList(item.getBidOfferPrices().stream().map(bo -> new PriceItem().from(bo)).collect(Collectors.toList()));
        this.setSession(FuturesBidOfferData.sessionControlMap.get(item.getControlCode().getValue()));
        String session = this.getSession();
        double expectedPrice = item.getProjectOpen().getValue();
        if (!(session.equalsIgnoreCase("ATO") || session.equalsIgnoreCase("ATC"))
                || expectedPrice <= 0) {
            this.expectedPrice = null;
        } else {
            this.expectedPrice = NumberUtils.round2Decimal(expectedPrice);
        }
    }

    @Data
    public static class PriceItem implements Transform<BidOfferAutoItem.SubItem, PriceItem> {
        private double bidPrice;
        private double offerPrice;
        private long bidVolume;
        private long offerVolume;
        private long bidVolumeChange;
        private long offerVolumeChange;

        @Override
        public void parse(BidOfferAutoItem.SubItem item) {
            this.setBidPrice(item.getBidPrice().getValue());
            this.setOfferPrice(item.getOfferPrice().getValue());
            this.setBidVolume(item.getBidVolume().getValue());
            this.setOfferVolume(item.getOfferVolume().getValue());
            this.setBidVolumeChange(item.getBidVolumeChange().getValue());
            this.setOfferVolumeChange(item.getOfferVolumeChange().getValue());
        }
    }
}
