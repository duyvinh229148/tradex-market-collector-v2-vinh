package com.techx.tradex.marketcollector.model.db;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "c_symbol_name")
public class SymbolName {
    @Id
    private String code;
    private String name;
    private String nameEn;
}
