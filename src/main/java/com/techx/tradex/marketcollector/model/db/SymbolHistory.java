package com.techx.tradex.marketcollector.model.db;

import com.techx.tradex.common.utils.DefaultUtils;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "c_symbol_history")
public class SymbolHistory {
    @Id
    private String id;
    private String code;
    private String refCode;
    private String exchange;
    private String marketType;
    private String name;
    private String nameEn;
    private SymbolTypeEnum type;
    private Date createdAt = new Date();

    public static SymbolHistory fromSymbol(Symbol symbol) {
        SymbolHistory symbolHistory = new SymbolHistory();
        symbolHistory.setId(DefaultUtils.DATE_FORMAT().format(new Date()) + "_" + symbol.getCode());
        symbolHistory.setCode(symbol.getCode());
        symbolHistory.setRefCode(symbol.getRefCode());
        symbolHistory.setExchange(symbol.getExchange());
        symbolHistory.setMarketType(symbol.getMarketType());
        symbolHistory.setName(symbol.getName());
        symbolHistory.setNameEn(symbol.getNameEn());
        symbolHistory.setType(symbol.getType());
        return symbolHistory;
    }
}
