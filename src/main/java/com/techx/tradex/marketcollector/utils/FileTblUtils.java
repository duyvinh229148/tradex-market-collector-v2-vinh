package com.techx.tradex.marketcollector.utils;

import com.techx.tradex.marketcollector.constants.Constants;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.Symbol;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class FileTblUtils {
    public static void parseMasterTblFile(Map<String, Symbol> stockIndexMap, File file, boolean isEn) throws IOException {
        FileUtils.readLines(file, "UTF-8").forEach(line -> {
            String exchange = line.substring(0, 2);
            String code = line.substring(2, 14).trim();
            String middle = line.substring(14, 20).trim();
            String name = line.substring(20).trim();
            if (ArrayUtils.contains(Constants.STOCK_EXCHANGES, exchange)) {
                Symbol symbol = stockIndexMap.get(code);
                if (symbol != null) {
                    symbol.setName(name, isEn);
                } else {
                    symbol = new Symbol();
                    symbol.setExchange(exchange);
                    symbol.setCode(code);
                    symbol.setName(name, isEn);
                    symbol.setType(SymbolTypeEnum.STOCK);
                    stockIndexMap.put(code, symbol);
                }
            } else if (ArrayUtils.contains(Constants.CW_EXCHANGES, exchange)) {
                Symbol symbol = stockIndexMap.get(code);
                if (symbol != null) {
                    symbol.setName(name, isEn);
                } else {
                    symbol = new Symbol();
                    symbol.setExchange(exchange);
                    symbol.setCode(code);
                    symbol.setName(name, isEn);
                    symbol.setType(SymbolTypeEnum.CW);
                    stockIndexMap.put(code, symbol);
                }
            } else if (ArrayUtils.contains(Constants.INDEX_EXCHANGES, exchange)) {
                Symbol symbol = stockIndexMap.get(code);
                if (symbol != null) {
                    symbol.setName(name, isEn);
                } else {
                    symbol = new Symbol();
                    symbol.setExchange(exchange);
                    symbol.setRefCode(code);
                    symbol.setName(name, isEn);
                    symbol.setType(SymbolTypeEnum.INDEX);
                    stockIndexMap.put(code, symbol);
                }
                if (!isEn) {
                    symbol.setCode(name.replace(" ", "").replace("Index", ""));
                }
            } else if (ArrayUtils.contains(Constants.FUTURES_EXCHANGES, exchange)) {
                Symbol symbol = stockIndexMap.get(code);
                if (symbol != null) {
                    symbol.setName(name, isEn);
                } else {
                    symbol = new Symbol();
                    symbol.setExchange(exchange);
                    symbol.setCode(code);
                    symbol.setName(name, isEn);
                    symbol.setType(SymbolTypeEnum.FUTURES);
                    stockIndexMap.put(code, symbol);
                }
            } else if (ArrayUtils.contains(Constants.BOND_EXCHANGES, exchange)) {
                Symbol symbol = stockIndexMap.get(code);
                if (symbol != null) {
                    symbol.setName(name, isEn);
                } else {
                    symbol = new Symbol();
                    symbol.setExchange(exchange);
                    symbol.setCode(code.substring(2));
                    symbol.setName(name, isEn);
                    symbol.setType(SymbolTypeEnum.BOND);
                    stockIndexMap.put(code, symbol);
                }
            }
        });
    }

    public static File decompressGzipFile(File gzipFile, String destination) throws IOException {
        File extractFile = new File(destination);
        try (FileInputStream fis = new FileInputStream(gzipFile);
             GZIPInputStream gis = new GZIPInputStream(fis);
             FileOutputStream fos = new FileOutputStream(extractFile)) {

            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        }
        return extractFile;
    }
}
