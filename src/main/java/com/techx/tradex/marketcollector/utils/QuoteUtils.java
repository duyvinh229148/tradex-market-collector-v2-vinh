package com.techx.tradex.marketcollector.utils;

import com.techx.tradex.marketcollector.constants.Constants;
import org.apache.commons.lang3.StringUtils;

public class QuoteUtils {
    public static String convertAutoTime(String time) {
        if (StringUtils.isBlank(time)) {
            return null;
        }
        StringBuilder sb = new StringBuilder(6);
        String[] temps = time.split(":");
        int hour = Integer.valueOf(temps[0]);
        hour = hour - Constants.MARKET_TIMEZONE;
        if (hour < 10) {
            sb.append("0");
        }
        sb.append(hour);
        sb.append(temps[1]);
        sb.append(temps[2]);
        time = sb.toString();
        return time;
    }
}
