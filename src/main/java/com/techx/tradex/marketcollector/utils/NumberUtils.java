package com.techx.tradex.marketcollector.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NumberUtils {
    public static double round2Decimal(double number) {
        return ((double) Math.round(number * 100)) / 100;
    }
}

