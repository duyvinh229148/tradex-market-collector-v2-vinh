package com.techx.tradex.marketcollector.configurations;


import com.techx.tradex.common.utils.StringUtils;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "app")
@Data
public class AppConf {
    private Map<String, SystemConf> sysConf;
    private String domainBos;
    private String clusterId;
    private String kafkaUrl;
    private String producerPattern;
    private String consumerPattern;
    private Topics topics;
    private Channels channels;
    private RealtimeConf realtime;
    private List<String> etfCodes;
    private Account accountDownload;
    private List<Account> accounts;
    private IndexStockCompareConf indexStockCompare;
    private UpdateEtfConf updateEtf;
    private SocketClusterConf socketCluster;
    private FileUploads fileUploads;
    private int serverHourOffset;
    private boolean tblUploadEnable;
    private boolean marketDataUploadEnable;
    private Map<String, String> tblUpdateNotificationTopics;
    private boolean logRealtimePacket = false;
    private SecurityInfoConf securityInfo = new SecurityInfoConf();
    private Map<String, String> statusMap = new HashMap<>();
    private Map<String, Integer> highlightMap = new HashMap<>();
    private boolean enableMonitorLostBidOffer = false;
    private boolean enableBackup;

    private Minio minio;
    private S3 s3;
    private boolean enableS3;
    private boolean enableMinio;
    private String timeStartReceiveBidAsk;
    private String timeStopReceiveBidAsk;

    private List<String> indexListActive;

    @Data
    public static class Minio {
        private String baseUrl;
        private String bucketName;
        private String accessKey;
        private String privateKey;
        private String urlRewriteTo;
        private String bucketPolicy;
    }

    @Data
    public static class S3 {
        private String region;
        private String bucketName;
        private String accessKey;
        private String privateKey;
    }

    public Account findAccount(String system, String username) {
        Account account;
        if (StringUtils.isEmpty(system) && StringUtils.isEmpty(username)) {
            account = this.getAccounts().get(0);
        } else {
            account = this.getAccounts().stream().filter(acc
                    -> (StringUtils.isEmpty(system) || system.equals(acc.getSystem()))
                    && (StringUtils.isEmpty(username) || username.equals(acc.getUsername()))).findFirst().orElse(null);
        }
        return account;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class SystemConf {
        private String host;
        private Integer loginPort;
        private Integer dataPort;
        private Integer marketPort;
        private String secCode;
    }

    @Data
    public static class Topics {
        private String tblUpdate;
        private String configuration;
        private String fix;
        private String realtime;
        private String requestResponseListener;
        private String requestListener;
        private String userUtility;
    }

    @Data
    public static class Channels {
        private String refreshData;
    }

    @Data
    public static class Account {
        protected int id;
        protected String system;
        protected String username;
        protected String password;
        protected int limit = 0; // unlimited
    }

    @Data
    public static class RealtimeConf {
        private List<RealtimeAccountConf> accounts;
        private Map<String, SendOut> topics;
        private Map<String, SubscribeFilter> subscribeFilter;
        private int maxSendItems;
        private int numberCodesToRegister;
        private RealtimeWorkingTimeConf workingTime;
        private int minHour;
        private int maxHour;
        private int maxRetry = 3;
        private boolean count = false;
    }

    @Data
    public static class RealtimeWorkingTimeConf {
        private String from;
        private String to;
        private List<Integer> weekDays;
    }

    @Data
    public static class SendOut {
        private String topic;
        private String uri;
        private String transformTo;
        private boolean scPublishToCommonChannel;
        private String scPublishTo;
    }

    @Data
    public static class RealtimeAccountConf extends Account {
        private String codeType;
        private List<RealtimeAccountConf> partitions;
        private Map<String, String> topicMapping;
        private List<String> topics;
        protected List<String> onlyCodes;
        protected Integer repeat;
    }

    @Data
    public static class SubscribeFilter {
        private SubscribeFilterItem includes;
    }

    @Data
    public static class SubscribeFilterItem {
        private List<String> exchanges;
    }

    @Data
    public static class IndexStockCompareConf {
        private Account account;
    }

    @Data
    public static class UpdateEtfConf {
        private Account account;
    }

    @Data
    public static class SocketClusterConf {
        public static final String CODEC_MIN_BIN = "codecMinBin";
        private boolean enable = true;
        private String hostname = "localhost";
        private int port = 8000;
        private String path = "socketcluster/socketcluster/";
        private String codec = CODEC_MIN_BIN;
        private boolean autoReconnection = true;
        private boolean logMessage = false;
    }

    @Data
    public static class FileUploads {
        private String symbolStaticFile = "symbol_static_data.json";
        private String symbolStaticGzipFile = "symbol_static_data_gzip.json";
    }

    @Data
    public static class SecurityInfoConf {
        private boolean enable = false;
        private OverrideSymbolInfoConf overrideValues = new OverrideSymbolInfoConf();
    }

    @Data
    public static class OverrideSymbolInfoConf {
        private Map<String, SymbolInfo.SymbolInfoOverride> symbolMap = new HashMap<>();
    }

    public String getKafkaBootstraps() {
        return this.kafkaUrl.replace(";", ",");
    }
}
