package com.techx.tradex.marketcollector.constants;

public enum FixCfiCodeEnum {
    STOCK("ES"), FUND("CF"), ETF("EU"), CW("RW"), INDEX_FUTURES("FFI"), BOND_FUTURES("FFD");

    private String cfiCode;

    FixCfiCodeEnum(String cfiCode) {
        this.cfiCode = cfiCode;
    }

    public String getCfiCode() {
        return cfiCode;
    }

    public void setCfiCode(String cfiCode) {
        this.cfiCode = cfiCode;
    }
}
