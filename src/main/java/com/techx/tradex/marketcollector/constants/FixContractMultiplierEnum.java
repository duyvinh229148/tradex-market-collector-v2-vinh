package com.techx.tradex.marketcollector.constants;

public enum FixContractMultiplierEnum {
    STOCK(1), CW(1), BOND_FUTURES(10000), VN30_FUTURES(100000);

    private Integer contractMultiplier;

    FixContractMultiplierEnum(Integer contractMultiplier) {
        this.contractMultiplier = contractMultiplier;
    }

    public Integer getContractMultiplier() {
        return contractMultiplier;
    }

    public void setContractMultiplier(Integer contractMultiplier) {
        this.contractMultiplier = contractMultiplier;
    }
}
