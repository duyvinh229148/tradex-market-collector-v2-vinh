package com.techx.tradex.marketcollector.constants;

public enum SecurityTypeEnum {
    FUTURES, STOCK, INDEX, BOND, CW
}
