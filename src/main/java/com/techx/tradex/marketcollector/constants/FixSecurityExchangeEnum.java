package com.techx.tradex.marketcollector.constants;

public enum FixSecurityExchangeEnum {
    HOSE("XSTC"), UPCOM("XHNX"), HNX("HSTC");

    private String securityExchange;

    FixSecurityExchangeEnum(String securityExchange) {
        this.securityExchange = securityExchange;
    }

    public String getSecurityExchange() {
        return securityExchange;
    }

    public void setSecurityExchange(String securityExchange) {
        this.securityExchange = securityExchange;
    }
}
