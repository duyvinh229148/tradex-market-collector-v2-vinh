package com.techx.tradex.marketcollector.constants;

import com.techx.tradex.common.constants.StockQuoteCeilingFloorEqEnum;
import com.techx.tradex.common.utils.DefaultUtils;
import com.techx.tradex.htsconnection.socket.message.receive.*;
import com.techx.tradex.marketcollector.model.db.ForeignerDaily;
import com.techx.tradex.marketcollector.model.db.SymbolDaily;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import com.techx.tradex.marketcollector.model.db.SymbolQuote;
import com.techx.tradex.marketcollector.model.response.EstimatedCeilFloorResponse;
import com.techx.tradex.marketcollector.utils.QuoteUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.functions.Func1;

import java.text.ParseException;
import java.util.Date;
import java.util.function.BiConsumer;

public interface MappingHTS {
    Logger log = LoggerFactory.getLogger(MappingHTS.class);

    String DEFAULT_TIME = "120000";

    Func1<EstimatedCeilFloorRcv, EstimatedCeilFloorResponse> QUERY_ESTIMATED_CEIL_FLOOR = res -> {
        EstimatedCeilFloorResponse estimatedCeilFloorResponse = new EstimatedCeilFloorResponse();
        estimatedCeilFloorResponse.setCeilingPrice(res.getEstimatedCeilingPrice().getValue());
        estimatedCeilFloorResponse.setFloorPrice(res.getEstimatedFloorPrice().getValue());
        return estimatedCeilFloorResponse;
    };

    BiConsumer<MarketStockForeignerItem, ForeignerDaily> MAP_STOCK_FOREIGNER_DAILY =
            (item, stockForeignerDaily) -> {
                int foreignerChangeVolume = item.getChangedHoldVolume().getValue();
                double foreignerBuyAbleRatio = item.getBuyAbleRate().getValue();
                long foreignerCurrentRoom = item.getBuyAbleVolume().getValue();
                long foreignerTotalRoom = item.getTotalBuyAbleVolume().getValue();
                long foreignerSellVolume = item.getForeignerSellVolume().getValue();
                long foreignerBuyVolume = item.getForeignerBuyVolume().getValue();
                long foreignerHoldVolume = 0L;
                if (foreignerTotalRoom != 0) {
                    foreignerHoldVolume = foreignerTotalRoom - foreignerCurrentRoom;
                }
                if (foreignerBuyVolume == 0 && foreignerSellVolume == 0 && foreignerChangeVolume != 0) {
                    if (foreignerChangeVolume < 0) {
                        foreignerSellVolume = foreignerChangeVolume;
                    } else {
                        foreignerBuyVolume = foreignerChangeVolume;
                    }
                }
                long foreignerTradingVolume = foreignerBuyVolume - foreignerSellVolume;
                double listedQuantity = (foreignerBuyAbleRatio == 0d) ? 0 : (double) foreignerCurrentRoom / foreignerBuyAbleRatio;
                double foreignerHoldRatio = (listedQuantity == 0d) ? 0 : (double) foreignerHoldVolume / listedQuantity;
                stockForeignerDaily.setForeignerChangeVolume(foreignerChangeVolume);
                stockForeignerDaily.setForeignerBuyAbleRatio(foreignerBuyAbleRatio);
                stockForeignerDaily.setForeignerCurrentRoom(foreignerCurrentRoom);
                stockForeignerDaily.setForeignerTotalRoom(foreignerTotalRoom);
                stockForeignerDaily.setForeignerTradingVolume(foreignerTradingVolume);
                stockForeignerDaily.setForeignerBuyVolume(foreignerBuyVolume);
                stockForeignerDaily.setForeignerSellVolume(foreignerSellVolume);
                stockForeignerDaily.setForeignerHoldRatio(foreignerHoldRatio);
                stockForeignerDaily.setForeignerHoldVolume(foreignerHoldVolume);
                stockForeignerDaily.setListedQuantity(listedQuantity);
                try {
                    stockForeignerDaily.setDate(DefaultUtils.DATETIME_FORMAT().parse(item.getDate().getValue() + DEFAULT_TIME));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            };

    Func1<MarketStockForeignerRcv, ForeignerDaily> QUERY_STOCK_FOREIGNER_DAILY = res -> {
        ForeignerDaily stockForeignerDaily = new ForeignerDaily();
        MAP_STOCK_FOREIGNER_DAILY.accept(res.getItems().get(0), stockForeignerDaily);
        return stockForeignerDaily;
    };


    BiConsumer<MarketStockDailyItem, SymbolDaily> MAP_STOCK_DAILY =
            (item, it) -> {
                it.setType(SymbolTypeEnum.STOCK);
                it.setPrice((double) Math.abs(item.getPrice().getValue()));
                it.setChange((double) item.getChange().getValue());
                it.setRate((double) item.getRate().getValue());
                it.setTradingVolume(item.getTradingVolume().getValue());
                it.setTradingValue(item.getTradingValue().getValue());
                it.setOpen((double) Math.abs(item.getOpen().getValue()));
                it.setHigh((double) Math.abs(item.getHigh().getValue()));
                it.setLow((double) Math.abs(item.getLow().getValue()));
                it.setLast((double) Math.abs(item.getLast().getValue()));

                String ceilingFloorEqual = "";
                if (item.getChange().getIndex() == 1) {
                    ceilingFloorEqual = StockQuoteCeilingFloorEqEnum.CEILING.name();
                } else if (item.getChange().getIndex() == 4) {
                    ceilingFloorEqual = StockQuoteCeilingFloorEqEnum.FLOOR.name();
                }
                it.setCeilingFloorEqual(ceilingFloorEqual);
                try {
                    it.setDate(DefaultUtils.DATETIME_FORMAT().parse(item.getDate().getValue() + DEFAULT_TIME));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            };

    BiConsumer<MarketStockQuoteItem, SymbolQuote> MAP_STOCK_MATCH_LIST =
            (item, it) -> {
                String time = QuoteUtils.convertAutoTime(item.getTime().getValue());
                String date = DefaultUtils.DATE_FORMAT().format(new Date());
                it.setTime(time);
                it.setOpen(item.getOpen().getValue());
                it.setHigh(item.getHigh().getValue());
                it.setLow(item.getLow().getValue());
                it.setLast(item.getLast().getValue());
                it.setChange(item.getChange().getValue());
                it.setRate(item.getRate().getValue());
                it.setTradingVolume(item.getTradingVolume().getValue());
                it.setMatchingVolume(item.getMatchVolume().getValue());
                try {
                    it.setDate(DefaultUtils.DATETIME_FORMAT().parse(date + time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            };

    BiConsumer<MarketCWQuoteItem, SymbolQuote> MAP_CW_MATCH_LIST =
            (item, it) -> {
                String time = QuoteUtils.convertAutoTime(item.getTime().getValue());
                String date = DefaultUtils.DATE_FORMAT().format(new Date());
                it.setTime(time);
                it.setOpen(item.getOpen().getValue());
                it.setHigh(item.getHigh().getValue());
                it.setLow(item.getLow().getValue());
                it.setLast(item.getLast().getValue());
                it.setChange(item.getChange().getValue());
                it.setRate(item.getRate().getValue());
                it.setTradingVolume(item.getTradingVolume().getValue());
                it.setMatchingVolume(item.getMatchVolume().getValue());
                try {
                    it.setDate(DefaultUtils.DATETIME_FORMAT().parse(date + time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            };

    BiConsumer<MarketIndexQuoteItem, SymbolQuote> MAP_INDEX_MATCH_LIST =
            (item, it) -> {
                String time = QuoteUtils.convertAutoTime(item.getTime().getValue());
                String date = DefaultUtils.DATE_FORMAT().format(new Date());
                it.setTime(time);
                it.setOpen(item.getOpen().getValue());
                it.setHigh(item.getHigh().getValue());
                it.setLow(item.getLow().getValue());
                it.setLast(item.getLast().getValue());
                it.setChange(item.getChange().getValue());
                it.setRate(item.getRate().getValue());
                it.setTradingVolume(item.getTradingVolume().getValue());
                try {
                    it.setDate(DefaultUtils.DATETIME_FORMAT().parse(date + time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            };

    BiConsumer<MarketFuturesQuoteItem, SymbolQuote> MAP_FUTURES_MATCH_LIST =
            (item, it) -> {
                String time = QuoteUtils.convertAutoTime(item.getTime().getValue());
                String date = DefaultUtils.DATE_FORMAT().format(new Date());
                it.setTime(time);
                it.setOpen(item.getOpen().getValue());
                it.setHigh(item.getHigh().getValue());
                it.setLow(item.getLow().getValue());
                it.setLast(item.getLast().getValue());
                it.setChange(item.getChange().getValue());
                it.setRate(item.getRate().getValue());
                it.setTradingVolume(item.getTradingVolume().getValue());
                it.setMatchingVolume(item.getMatchVolume().getValue());
                try {
                    it.setDate(DefaultUtils.DATETIME_FORMAT().parse(date + time));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            };

    Func1<MarketStockDailyRcv, SymbolDaily> QUERY_STOCK_DAILY = res -> {
        SymbolDaily dailyStock = new SymbolDaily();
        MAP_STOCK_DAILY.accept(res.getItems().get(0), dailyStock);
        return dailyStock;
    };

    BiConsumer<MarketIndexDailyItem, SymbolDaily> MAP_DAILY_INDEX =
            (item, it) -> {
                it.setType(SymbolTypeEnum.INDEX);
                it.setOpen((double) (item.getOpen().getValue()));
                it.setHigh((double) (item.getHigh().getValue()));
                it.setLow((double) (item.getLow().getValue()));
                it.setLast((double) item.getLast().getValue());
                it.setChange((double) item.getChange().getValue());
                it.setRate((double) item.getRate().getValue());
                it.setTradingVolume(item.getTradingVolume().getValue());
                it.setTradingValue(item.getTradingValue().getValue());
                try {
                    it.setDate(DefaultUtils.DATETIME_FORMAT().parse(item.getDate().getValue() + DEFAULT_TIME));
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            };

    Func1<MarketIndexDailyRcv, SymbolDaily> QUERY_DAILY_INDEX = res -> {
        SymbolDaily dailyIndex = new SymbolDaily();
        MAP_DAILY_INDEX.accept(res.getItems().get(0), dailyIndex);
        return dailyIndex;
    };

    BiConsumer<MarketFuturesDailyItem, SymbolDaily> MAP_DAILY_FUTURES = (item, it) -> {
        it.setType(SymbolTypeEnum.FUTURES);
        it.setLast((double) item.getPrice().getValue());
        it.setChange((double) item.getChange().getValue());
        it.setRate((double) item.getRate().getValue());
        it.setOpen((double) (item.getOpen().getValue()));
        it.setHigh((double) (item.getHigh().getValue()));
        it.setLow((double) (item.getLow().getValue()));
        it.setTradingVolume(item.getVolume().getValue());
        it.setTradingValue(item.getValue().getValue());
        it.setMBasis((double) item.getMBasis().getValue());
        it.setTBasis((double) item.getTBasis().getValue());
        it.setTPrice((double) item.getTheoryPrice().getValue());
        it.setOpenInterest((long) item.getOpenInterest().getValue());
        it.setIndex((double) item.getIndex().getValue());
        try {
            it.setDate(DefaultUtils.DATETIME_FORMAT().parse(item.getDate().getValue() + DEFAULT_TIME));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    };

    Func1<MarketFuturesDailyRcv, SymbolDaily> QUERY_DAILY_FUTURES = res -> {
        SymbolDaily it = new SymbolDaily();
        MAP_DAILY_FUTURES.accept(res.getItems().get(0), it);
        return it;
    };

    Func1<MarketCWCurrentPriceRcv, SymbolInfo> QUERY_CW_INFO = res -> {
        SymbolInfo cwInfo = new SymbolInfo();
        cwInfo.setCeilingPrice((double) res.getCeilingPrice().getValue());
        cwInfo.setFloorPrice((double) res.getFloorPrice().getValue());
        cwInfo.setReferencePrice((double) res.getReferencePrice().getValue());
        cwInfo.setAveragePrice((double) res.getAveragePrice().getValue());
        cwInfo.setOpen((double) res.getOpen().getValue());
        cwInfo.setHigh((double) res.getHigh().getValue());
        cwInfo.setHighTime(res.getHighTime().getValue());
        cwInfo.setLow((double) res.getLow().getValue());
        cwInfo.setLowTime(res.getLowTime().getValue());
        cwInfo.setLast((double) res.getLast().getValue());
        cwInfo.setChange((double) res.getChange().getValue());
        cwInfo.setRate((double) res.getRate().getValue());
        cwInfo.setTradingVolume(res.getTradingVolume().getValue());
        cwInfo.setPtVolume(res.getPtVolume().getValue());
        cwInfo.setTotalTradingVolume(res.getTotalTradingVolume().getValue());
        cwInfo.setPriorTradingVolume(res.getPriorVolume().getValue());
        cwInfo.setTurnoverRate((double) res.getTurnoverRate().getValue());
        cwInfo.setTradingValue(res.getTradingValue().getValue());
        cwInfo.setTotalTradingValue(res.getTotalTradingValue().getValue());
        cwInfo.setListedQuantity(res.getListedQuantity().getValue());
        cwInfo.setExpectedPrice((double) res.getProjectOpen().getValue());
        cwInfo.setControlCode(res.getControlCode().getValue());
        cwInfo.setHighPrice52Week((double) res.getHighPrice52Week().getValue());
        cwInfo.setLowPrice52Week((double) res.getLowPrice52Week().getValue());
        cwInfo.setBidPrice((double) res.getBidPrice().getValue());
        cwInfo.setOfferPrice((double) res.getOfferPrice().getValue());
        cwInfo.setTotalBidVolume((long) res.getTotalBidVolume().getValue());
        cwInfo.setTotalOfferVolume((long) res.getTotalOfferVolume().getValue());
        cwInfo.setChangeOfTotalBidVolume(res.getChangeOfTotalBidVolume().getValue());
        cwInfo.setChangeOfTotalOfferVolume(res.getChangeOfTotalOfferVolume().getValue());
        cwInfo.setDiffBidOffer(res.getDiffBidOffer().getValue());
        cwInfo.setAccumulateBidVolume(res.getAccumulateBidVolume().getValue());
        cwInfo.setAccumulateBidCount(res.getAccumulateBidCount().getValue());
        cwInfo.setAccumulateOfferVolume(res.getAccumulateOfferVolume().getValue());
        cwInfo.setAccumulateOfferCount(res.getAccumulateOfferCount().getValue());
        cwInfo.setIndustry(res.getIndustry().getValue());
        cwInfo.setIssuerName(res.getIssuerName().getValue());
        cwInfo.setExercisePrice((double) res.getExercisePrice().getValue());
        cwInfo.setExerciseRatio(res.getExerciseRatio().getValue());
        cwInfo.setBreakEven((double) res.getBreakEven().getValue());
        cwInfo.setImpliedVolatility(res.getImpliedVolatility().getValue());
        cwInfo.setParity(res.getParity().getValue());
        try {
            cwInfo.setMaturityDate(DefaultUtils.DATE_FORMAT().parse("" + res.getMaturityDate().getValue()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        cwInfo.setTPrice((double) res.getTPrice().getValue());
        cwInfo.setDelta((double) res.getDelta().getValue());
        cwInfo.setGearingRt((double) res.getGearingRt().getValue());
        cwInfo.setCapitalFulcrumPoint((double) res.getCapitalFulcrumPoint().getValue());
        cwInfo.setUnderlyingSymbol(res.getUnderlyingSymbol().getValue());
        cwInfo.setUnderlyingPrice(res.getUnderlyingPrice().getValue());
        cwInfo.setUnderlyingChange(res.getUnderlyingChange().getValue());
        cwInfo.setUnderlyingRate((double) res.getUnderlyingRate().getValue());
        if (res.getMarketName().getValue().equals("HSX")) {
            cwInfo.setMarketType("HOSE");
        } else {
            cwInfo.setMarketType(res.getMarketName().getValue());
        }
        if (res.getChange().getIndex() == 1) {
            cwInfo.setCeilingFloorEqual(StockQuoteCeilingFloorEqEnum.CEILING.name());
        } else if (res.getChange().getIndex() == 4) {
            cwInfo.setCeilingFloorEqual(StockQuoteCeilingFloorEqEnum.FLOOR.name());
        }
        cwInfo.setType(SymbolTypeEnum.CW);
        try {
            cwInfo.setLastTradingDate(DefaultUtils.DATE_FORMAT().parse("" + res.getLastTradingDate().getValue()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (res.getBidOfferData() != null) {
            res.getBidOfferData().forEach(item -> {
                SymbolInfo.BidOfferItem bidOfferItem = new SymbolInfo.BidOfferItem();
                bidOfferItem.setBidPrice((double) item.getBidPrice().getValue());
                bidOfferItem.setBidVolume(item.getBidVolume().getValue());
                bidOfferItem.setBidVolumeChange(item.getBidVolumeChange().getValue());
                bidOfferItem.setOfferPrice((double) item.getOfferPrice().getValue());
                bidOfferItem.setOfferVolume(item.getOfferVolume().getValue());
                bidOfferItem.setOfferVolumeChange(item.getOfferVolumeChange().getValue());
                cwInfo.getBidOfferList().add(bidOfferItem);
            });
        }
        if (res.getHighLowYearData() != null) {
            res.getHighLowYearData().forEach((item) -> {
                SymbolInfo.HighLowYearItem highLowYearItem = new SymbolInfo.HighLowYearItem();
                highLowYearItem.setHighPrice(item.getHighPrice().getValue());
                highLowYearItem.setDateOfHighPrice(item.getDateOfHighPrice().getValue());
                highLowYearItem.setLowPrice(item.getLowPrice().getValue());
                highLowYearItem.setDateOfLowPrice(item.getDateOfLowPrice().getValue());
                cwInfo.getHighLowYearData().add(highLowYearItem);
            });
        }
        return cwInfo;
    };
    BiConsumer<MarketCWDailyItem, SymbolDaily> MAP_DAILY_CW = (item, it) -> {
        it.setType(SymbolTypeEnum.CW);
        it.setOpen((double) (item.getOpen().getValue()));
        it.setHigh((double) (item.getHigh().getValue()));
        it.setLow((double) (item.getLow().getValue()));
        it.setLast((double) item.getLast().getValue());
        it.setChange((double) item.getChange().getValue());
        it.setRate((double) item.getRate().getValue());
        it.setTradingVolume(item.getTradingVolume().getValue());
        it.setTradingValue(item.getTradingValue().getValue());
        it.setUnderlyingPrice(item.getUnderlyingPrice().getValue());
        it.setUnderlyingRate((double) item.getUnderlyingRate().getValue());
        try {
            it.setDate(DefaultUtils.DATETIME_FORMAT().parse(item.getDate().getValue() + DEFAULT_TIME));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    };

    Func1<MarketCWDailyRcv, SymbolDaily> QUERY_DAILY_CW = res -> {
        SymbolDaily dailyCW = new SymbolDaily();
        MAP_DAILY_CW.accept(res.getItems().get(0), dailyCW);
        return dailyCW;
    };

    Func1<MarketStockCurrentPriceRcv, SymbolInfo> QUERY_STOCK_INFO = res -> {
        SymbolInfo stockInfo = new SymbolInfo();
        stockInfo.setOpen((double) res.getOpen().getValue());
        stockInfo.setLast((double) res.getLast().getValue());
        if (res.getLast().getValue() == 0) {
            stockInfo.setLast((double) res.getReferencePrice().getValue());
        }
        stockInfo.setHigh((double) res.getHigh().getValue());
        stockInfo.setLow((double) res.getLow().getValue());
        stockInfo.setChange((double) res.getChange().getValue());
        stockInfo.setRate((double) res.getRate().getValue());
        stockInfo.setTradingVolume(res.getTotalTradingVolume().getValue());
        stockInfo.setTradingValue(res.getTotalTradingValue().getValue());
        stockInfo.setCode(res.getStockCode().getValue());
        if (res.getMarketName().getValue().equals("HSX")) {
            stockInfo.setMarketType("HOSE");
            stockInfo.setTotalOfferVolume(0L);
            stockInfo.setTotalBidVolume(0L);
        } else {
            stockInfo.setMarketType(res.getMarketName().getValue());
            stockInfo.setTotalOfferVolume(res.getAccumulateOfferVolume().getValue());
            stockInfo.setTotalBidVolume(res.getAccumulateBidVolume().getValue());
        }
        stockInfo.setIndustry(StringUtils.trim(res.getIndustry().getValue()));
        stockInfo.setCeilingPrice((double) res.getCeilingPrice().getValue());
        stockInfo.setFloorPrice((double) res.getFloorPrice().getValue());
        stockInfo.setReferencePrice((double) res.getReferencePrice().getValue());
        stockInfo.setAveragePrice((double) res.getAveragePrice().getValue());
        stockInfo.setPtVolume(res.getPtVolume().getValue());
        stockInfo.setPtValue(res.getPtTradingValue().getValue());
        stockInfo.setPriorTradingVolume(res.getPriorVolume().getValue());
        stockInfo.setTurnoverRate((double) res.getTurnoverRate().getValue());
        stockInfo.setParValue((int) res.getParValue().getValue());
        stockInfo.setListedQuantity(res.getListedQuantity().getValue());
        stockInfo.setForeignerBuyVolume(res.getForeignerBuyVolume().getValue());
        stockInfo.setForeignerSellVolume(res.getForeignerSellVolume().getValue());
        stockInfo.setForeignerTotalRoom(res.getForeignerTotalRoom().getValue());
        stockInfo.setForeignerCurrentRoom(res.getForeignerCurrentRoom().getValue());
        stockInfo.setTotalOfferCount(res.getAccumulateOfferCount().getValue());
        stockInfo.setTotalBidCount(res.getAccumulateBidCount().getValue());
        stockInfo.setRights(res.getRights().getValue());
        stockInfo.setLowPrice52Week((double) res.getLowPrice52Week().getValue());
        stockInfo.setHighPrice52Week((double) res.getHighPrice52Week().getValue());
        stockInfo.setType(SymbolTypeEnum.STOCK);
        stockInfo.setExpectedPrice((double) res.getProjectOpen().getValue());
        if (res.getBidOfferData() != null) {
            res.getBidOfferData().forEach(item -> {
                SymbolInfo.BidOfferItem bidOfferItem = new SymbolInfo.BidOfferItem();
                bidOfferItem.setBidPrice((double) item.getBidPrice().getValue());
                bidOfferItem.setBidVolume(item.getBidVolume().getValue());
                bidOfferItem.setBidVolumeChange(item.getBidVolumeChange().getValue());
                bidOfferItem.setOfferPrice((double) item.getOfferPrice().getValue());
                bidOfferItem.setOfferVolume(item.getOfferVolume().getValue());
                bidOfferItem.setOfferVolumeChange(item.getOfferVolumeChange().getValue());
                stockInfo.getBidOfferList().add(bidOfferItem);
            });
        }
        if (res.getHighLowYearData() != null) {
            res.getHighLowYearData().forEach((item) -> {
                SymbolInfo.HighLowYearItem highLowYearItem = new SymbolInfo.HighLowYearItem();
                highLowYearItem.setHighPrice(item.getHighPrice().getValue());
                highLowYearItem.setDateOfHighPrice(item.getDateOfHighPrice().getValue());
                highLowYearItem.setLowPrice(item.getLowPrice().getValue());
                highLowYearItem.setDateOfLowPrice(item.getDateOfLowPrice().getValue());
                stockInfo.getHighLowYearData().add(highLowYearItem);
            });
        }
        return stockInfo;
    };

    Func1<MarketIndustryCurrentIndexRcv, SymbolInfo> QUERY_INDEX_INFO = res -> {
        SymbolInfo indexInfo = new SymbolInfo();
        indexInfo.setOpen((double) res.getOpenIndex().getValue());
        indexInfo.setLast((double) res.getLast().getValue());
        indexInfo.setHigh((double) res.getHighIndex().getValue());
        indexInfo.setLow((double) res.getLowIndex().getValue());
        indexInfo.setChange((double) res.getChange().getValue());
        indexInfo.setRate((double) res.getRate().getValue());
        indexInfo.setTradingVolume(res.getTradingVolume().getValue());
        indexInfo.setTradingValue(res.getTradingValue().getValue());
        indexInfo.setPriorTradingVolume(res.getPriorVolume().getValue());
        indexInfo.setUpCount(res.getUpCount().getValue());
        indexInfo.setCeilingCount(res.getCeilingCount().getValue());
        indexInfo.setDownCount(res.getDownCount().getValue());
        indexInfo.setFloorCount(res.getFloorCount().getValue());
        indexInfo.setUnchangedCount(res.getUnchangedCount().getValue());
        indexInfo.setType(SymbolTypeEnum.INDEX);
        return indexInfo;
    };

    Func1<MarketFuturesCurrentPriceRcv, SymbolInfo> QUERY_FUTURES_INFO = res -> {
        SymbolInfo futuresInfo = new SymbolInfo();
        futuresInfo.setCode(res.getStockCode().getValue());
        futuresInfo.setName(res.getStockName().getValue());
        futuresInfo.setLast((double) res.getLast().getValue());
        futuresInfo.setOpen((double) res.getOpen().getValue());
        futuresInfo.setHigh((double) res.getHigh().getValue());
        futuresInfo.setLow((double) res.getLow().getValue());
        futuresInfo.setChange((double) res.getChange().getValue());
        futuresInfo.setRate((double) res.getRate().getValue());
        futuresInfo.setTradingValue(res.getTradingValue().getValue());
        futuresInfo.setTradingVolume(res.getTradingVolume().getValue());
        if (res.getChange().getIndex() == 1) {
            futuresInfo.setCeilingFloorEqual(StockQuoteCeilingFloorEqEnum.CEILING.name());
        } else if (res.getChange().getIndex() == 4) {
            futuresInfo.setCeilingFloorEqual(StockQuoteCeilingFloorEqEnum.FLOOR.name());
        }
        futuresInfo.setTime(res.getTime().getValue());
        futuresInfo.setCeilingPrice((double) res.getCeilingPrice().getValue());
        futuresInfo.setFloorPrice((double) res.getFloorPrice().getValue());
        futuresInfo.setAveragePrice((double) res.getAveragePrice().getValue());
        futuresInfo.setReferencePrice((double) res.getReferencePrice().getValue());
        futuresInfo.setHighTime(res.getHighTime().getValue());
        futuresInfo.setLowTime(res.getLowTime().getValue());
        futuresInfo.setExpectedPrice((double) res.getProjectOpen().getValue());

        futuresInfo.setPtVolume(res.getPtVolume().getValue());
        futuresInfo.setTotalTradingVolume(res.getTotalTradingVolume().getValue());
        futuresInfo.setTotalTradingValue(res.getTotalTradingValue().getValue());
        futuresInfo.setPriorVolume(res.getPriorVolume().getValue());
        futuresInfo.setTurnoverRate((double) res.getTurnoverRate().getValue());
        futuresInfo.setPtTradingValue(res.getPtTradingValue().getValue());
        futuresInfo.setParValue((int) res.getParValue().getValue());
        futuresInfo.setListedQuantity(res.getListedQuantity().getValue());
        futuresInfo.setForeignerBuyVolume(res.getForeignerBuyVolume().getValue());
        futuresInfo.setForeignerSellVolume(res.getForeignerSellVolume().getValue());
        futuresInfo.setForeignerCurrentRoom(res.getForeignerCurrentRoom().getValue());
        futuresInfo.setForeignerTotalRoom(res.getForeignerTotalRoom().getValue());
        futuresInfo.setControlCode(res.getControlCode().getValue());
        futuresInfo.setHighPrice52Week((double) res.getHighPrice52Week().getValue());
        futuresInfo.setLowPrice52Week((double) res.getLowPrice52Week().getValue());

        if (res.getHighLowYearData() != null) {
            res.getHighLowYearData().forEach((item) -> {
                SymbolInfo.HighLowYearItem highLowYearItem = new SymbolInfo.HighLowYearItem();
                highLowYearItem.setHighPrice(item.getHighPrice().getValue());
                highLowYearItem.setDateOfHighPrice(item.getDateOfHighPrice().getValue());
                highLowYearItem.setLowPrice(item.getLowPrice().getValue());
                highLowYearItem.setDateOfLowPrice(item.getDateOfLowPrice().getValue());
                futuresInfo.getHighLowYearData().add(highLowYearItem);
            });
        }
        futuresInfo.setBidPrice((double) res.getBidPrice().getValue());
        futuresInfo.setOfferPrice((double) res.getOfferPrice().getValue());

        if (res.getBidOfferData() != null) {
            res.getBidOfferData().forEach(item -> {
                SymbolInfo.BidOfferItem bidOfferItem = new SymbolInfo.BidOfferItem();
                bidOfferItem.setBidPrice((double) item.getBidPrice().getValue());
                bidOfferItem.setBidVolume(item.getBidVolume().getValue());
                bidOfferItem.setBidVolumeChange(item.getBidVolumeChange().getValue());
                bidOfferItem.setOfferPrice((double) item.getOfferPrice().getValue());
                bidOfferItem.setOfferVolume(item.getOfferVolume().getValue());
                bidOfferItem.setOfferVolumeChange(item.getOfferVolumeChange().getValue());
                futuresInfo.getBidOfferList().add(bidOfferItem);
            });
        }

        futuresInfo.setTotalBidVolume((long) res.getTotalBidVolume().getValue());
        futuresInfo.setTotalOfferVolume((long) res.getTotalOfferVolume().getValue());
        futuresInfo.setChangeOfTotalBidVolume(res.getChangeOfTotalBidVolume().getValue());
        futuresInfo.setChangeOfTotalOfferVolume(res.getChangeOfTotalOfferVolume().getValue());
        futuresInfo.setDiffBidOffer(res.getDiffBidOffer().getValue());
        futuresInfo.setAccumulateBidCount(res.getAccumulateBidCount().getValue());
        futuresInfo.setAccumulateBidVolume(res.getAccumulateBidVolume().getValue());
        futuresInfo.setAccumulateOfferCount(res.getAccumulateOfferCount().getValue());
        futuresInfo.setAccumulateOfferVolume(res.getAccumulateOfferVolume().getValue());
        futuresInfo.setMarketName(res.getMarketName().getValue());
        futuresInfo.setIndustry(res.getIndustry().getValue());
        futuresInfo.setRights(res.getRights().getValue());
        futuresInfo.setBaseCode(res.getBaseCode().getValue());
        futuresInfo.setOpenInterest(res.getOpenInterest().getValue());
        futuresInfo.setOpenInterestChange(Math.max(res.getOpenInterestChange().getValue(), 0));
        futuresInfo.setNormalForeignerBuyValue(res.getNormalForeignerBuyValue().getValue());
        futuresInfo.setNormalForeignerBuyVolume(res.getNormalForeignerBuyVolume().getValue());
        futuresInfo.setNormalForeignerSellValue(res.getNormalForeignerSellValue().getValue());
        futuresInfo.setNormalForeignerSellVolume(res.getNormalForeignerSellVolume().getValue());
        futuresInfo.setPtForeignerTotalBuyValue(res.getPtForeignerTotalBuyValue().getValue());
        futuresInfo.setPtForeignerTotalBuyVolume(res.getPtForeignerTotalBuyVolume().getValue());
        futuresInfo.setPtForeignerTotalSellValue(res.getPtForeignerTotalSellValue().getValue());
        futuresInfo.setPtForeignerTotalSellVolume(res.getPtForeignerTotalSellVolume().getValue());
        futuresInfo.setRemainDate(res.getRemainDate().getValue());
        futuresInfo.setTheoryPrice((double) res.getTheoryPrice().getValue());
        futuresInfo.setBasis((double) res.getBasis().getValue());
        futuresInfo.setTheoryBasis((double) res.getTheoryBasis().getValue());
        futuresInfo.setMarketBasis((double) res.getMarketBasis().getValue());
        futuresInfo.setDisparate((double) res.getDisparate().getValue());
        futuresInfo.setDisparateRate((double) res.getDisparateRate().getValue());
        futuresInfo.setType(SymbolTypeEnum.FUTURES);
        try {
            futuresInfo.setFirstTradingDate(DefaultUtils.DATE_FORMAT().parse("" + res.getStartDate().getValue()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            futuresInfo.setMaturityDate(DefaultUtils.DATE_FORMAT().parse("" + res.getEndDate().getValue()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return futuresInfo;
    };

}
