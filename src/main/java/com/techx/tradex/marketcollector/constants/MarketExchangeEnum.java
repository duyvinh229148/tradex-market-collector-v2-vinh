package com.techx.tradex.marketcollector.constants;

public enum MarketExchangeEnum {

    HOSE("01"), UPCOM("31"), HNX("11"), ETF_HOSE("04"), FUND_HOSE("03");

    private String value;

    MarketExchangeEnum(String value) {
        this.value = value;
    }

    public static MarketExchangeEnum from(String value) {
        if (value.equalsIgnoreCase(HOSE.value)) {
            return HOSE;
        }
        if (value.equalsIgnoreCase(UPCOM.value)) {
            return UPCOM;
        }
        if (value.equalsIgnoreCase(HNX.value)) {
            return HNX;
        }
        if (value.equalsIgnoreCase(ETF_HOSE.value)) {
            return ETF_HOSE;
        }
        if (value.equalsIgnoreCase(FUND_HOSE.value)) {
            return FUND_HOSE;
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
