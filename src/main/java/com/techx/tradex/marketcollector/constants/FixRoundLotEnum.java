package com.techx.tradex.marketcollector.constants;

public enum FixRoundLotEnum {
    HOSE(10), UPCOM(100), HNX(100), FUTURES(1);

    private Integer roundLot;

    FixRoundLotEnum(Integer roundLot) {
        this.roundLot = roundLot;
    }

    public Integer getRoundLot() {
        return roundLot;
    }

    public void setRoundLot(Integer roundLot) {
        this.roundLot = roundLot;
    }

}
