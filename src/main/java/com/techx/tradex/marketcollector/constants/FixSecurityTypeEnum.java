package com.techx.tradex.marketcollector.constants;

public enum FixSecurityTypeEnum {
    CS, FUT, OPT, WAR, MLEG
}
