package com.techx.tradex.marketcollector.constants;

public enum SymbolTypeEnum {
    FUTURES, STOCK, INDEX, BOND, CW
}
