package com.techx.tradex.marketcollector;

import com.ea.async.Async;
import com.techx.tradex.htsconnection.socket.message.receive.ClassRegistration;
import com.techx.tradex.marketcollector.configurations.AppConf;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.UUID;

@SpringBootApplication
@EnableConfigurationProperties({AppConf.class})
@EnableScheduling
@EnableAsync
@EnableMongoRepositories
public class Application {
    public static final String instanceId = UUID.randomUUID().toString();

    public static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) {
        System.out.println("do registering classes");
        ClassRegistration.doRegisters();
        applicationContext = new SpringApplication(Application.class).run(args);
        Async.init();
    }
}
