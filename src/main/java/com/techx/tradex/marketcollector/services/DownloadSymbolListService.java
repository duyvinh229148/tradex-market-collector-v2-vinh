package com.techx.tradex.marketcollector.services;

import com.techx.tradex.common.utils.RxUtils;
import com.techx.tradex.htsconnection.socket.LoginData;
import com.techx.tradex.htsconnection.socket.nonblocking.ResultWaiter;
import com.techx.tradex.htsconnection.socket.nonblocking.SelectorHandler;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.model.db.Symbol;
import com.techx.tradex.marketcollector.socket.DownloadResourceConnectionHandler;
import com.techx.tradex.marketcollector.utils.FileTblUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
public class DownloadSymbolListService {
    private static final Logger log = LoggerFactory.getLogger(DownloadSymbolListService.class);
    private static final long STOCK_INDEX_MAP_LIFE_TIME = 14400000;

    private ConnectionPoolService connectionPoolService;
    private AppConf appConf;

    private Map<String, Symbol> stockIndexMap;
    private long stockIndexMapUpdatedAt = 0;

    @Autowired
    public DownloadSymbolListService(ConnectionPoolService connectionPoolService, AppConf appConf) {
        this.connectionPoolService = connectionPoolService;
        this.appConf = appConf;
    }

    public Observable<Map<String, Symbol>> download() {
        log.info("download resource");
        PublishSubject<Map<String, Symbol>> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        accountDownload = this.appConf.findAccount(accountDownload.getSystem(), accountDownload.getUsername());
        AppConf.SystemConf systemConf = appConf.getSysConf().get(accountDownload.getSystem());
        LoginData loginData = new LoginData(accountDownload.getUsername(), accountDownload.getPassword(), systemConf.getHost(), systemConf.getLoginPort(), 0, 0, systemConf.getSecCode());
        if (stockIndexMap != null &&
                stockIndexMapUpdatedAt > 0 &&
                System.currentTimeMillis() - this.stockIndexMapUpdatedAt < STOCK_INDEX_MAP_LIFE_TIME) {
            return Observable.from(Arrays.asList(stockIndexMap));
        }
        final Map<String, Symbol> stockIndexMap = new HashMap<>();
        final List<String> files = new ArrayList<>();
        log.info("download resource connect");
        connectionPoolService.connect(loginData, this::create).subscribe(
                fileName -> {
                    log.info("download file {}", fileName);
                    if (fileName.matches("master(_eng)?.tbl")) {
                        files.add(fileName);
                        boolean isEn = fileName.contains("_eng");
                        try {
                            log.info("reading file  {}", fileName);
                            File extractFile = FileTblUtils.decompressGzipFile(new File(fileName), FilenameUtils.getBaseName(fileName));
                            log.info(extractFile.getAbsolutePath());
                            FileTblUtils.parseMasterTblFile(stockIndexMap, extractFile, isEn);
                            log.info("total stock/index added  {}", stockIndexMap.size());
                            if (files.size() == 2) {
                                log.info("finish download stock and index  {}", stockIndexMap.size());
                                this.stockIndexMap = stockIndexMap;
                                this.stockIndexMapUpdatedAt = System.currentTimeMillis();
                                subject.onNext(stockIndexMap);
                                subject.onCompleted();
                            }
                        } catch (Exception e) {
                            log.error("Fail to parse or upload s3 master file", e);
                            subject.onError(e);
                        }
                    }
                },
                subject::onError
        );
        return subject;
    }

    public CompletableFuture<Map<String, Symbol>> downloadFuture() {
        log.info("download resource");
        AppConf.Account accountDownload = appConf.getAccountDownload();
        accountDownload = this.appConf.findAccount(accountDownload.getSystem(), accountDownload.getUsername());
        AppConf.SystemConf systemConf = appConf.getSysConf().get(accountDownload.getSystem());
        LoginData loginData = new LoginData(accountDownload.getUsername(), accountDownload.getPassword(), systemConf.getHost(), systemConf.getLoginPort(), 0, 0, systemConf.getSecCode());
        if (stockIndexMap != null &&
                stockIndexMapUpdatedAt > 0 &&
                System.currentTimeMillis() - this.stockIndexMapUpdatedAt < STOCK_INDEX_MAP_LIFE_TIME) {
            return CompletableFuture.completedFuture(stockIndexMap);
        }
        final Map<String, Symbol> stockIndexMap = new HashMap<>();
        final List<String> files = new ArrayList<>();
        log.info("download resource connect");
        CompletableFuture<Map<String, Symbol>> result = new CompletableFuture<>();
        connectionPoolService.connect(loginData, this::create).subscribe(
                fileName -> {
                    log.info("download file {}", fileName);
                    if (fileName.matches("master(_eng)?.tbl")) {
                        files.add(fileName);
                        boolean isEn = fileName.contains("_eng");
                        try {
                            log.info("reading file  {}", fileName);
                            File extractFile = FileTblUtils.decompressGzipFile(new File(fileName), FilenameUtils.getBaseName(fileName));
                            log.info(extractFile.getAbsolutePath());
                            FileTblUtils.parseMasterTblFile(stockIndexMap, extractFile, isEn);
                            log.info("total stock/index added  {}", stockIndexMap.size());
                            if (files.size() == 2) {
                                log.info("finish download stock and index  {}", stockIndexMap.size());
                                this.stockIndexMap = stockIndexMap;
                                this.stockIndexMapUpdatedAt = System.currentTimeMillis();
                                result.complete(stockIndexMap);
                            }
                        } catch (Exception e) {
                            log.error("Fail to parse or upload s3 master file", e);
                            result.completeExceptionally(e);
                        }
                    }
                },
                result::completeExceptionally
        );
        return result;
    }

    private DownloadResourceConnectionHandler create(String connectionId, SelectorHandler selectorHandler
            , LoginData loginData, ResultWaiter<String> subject) throws IOException {
        DownloadResourceConnectionHandler connectionHandler
                = new DownloadResourceConnectionHandler(connectionId, loginData, selectorHandler.getSelector());
        RxUtils.safeSubscribe(connectionHandler.start(),
                fileName -> {
                    log.info("file name");
                    subject.response(fileName);
                }, subject::response,
                () -> {
                    subject.getObserver().onCompleted();
                    connectionHandler.close();
                }
        );
        return connectionHandler;
    }
}
