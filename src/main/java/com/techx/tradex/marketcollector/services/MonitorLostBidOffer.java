package com.techx.tradex.marketcollector.services;

import com.techx.tradex.common.utils.Pair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MonitorLostBidOffer {
    private Map<String, Pair<Integer, Integer>> map;

    public MonitorLostBidOffer() {
        map = new HashMap<>();
    }

    public void receiveQuote(String code) {
        Pair<Integer, Integer> pair = this.map.get(code);
        if (pair == null) {
            pair = new Pair<>(0, 0);
            this.map.put(code, pair);
        }
        pair.setLeft(pair.getLeft() + 1);
    }

    public void receiveBidOffer(String code) {
        Pair<Integer, Integer> pair = this.map.get(code);
        if (pair == null) {
            pair = new Pair<>(0, 0);
            this.map.put(code, pair);
        }
        pair.setRight(pair.getRight() + 1);
    }

    public List<String> getLostCodes() {
        List<String> result = new ArrayList<>();
        map.forEach((key, value) -> {
            if (value.getLeft() > 0 && value.getRight() == 0) {
                result.add(key);
            }
        });
        return result;
    }
}
