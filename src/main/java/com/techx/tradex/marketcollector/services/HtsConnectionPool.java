package com.techx.tradex.marketcollector.services;

import com.techx.tradex.common.exceptions.GeneralException;
import com.techx.tradex.common.utils.StringUtils;
import com.techx.tradex.htsconnection.socket.LoginData;
import com.techx.tradex.htsconnection.socket.message.receive.LogInRcv;
import com.techx.tradex.htsconnection.socket.nonblocking.BaseHtsConnectionHandler;
import com.techx.tradex.htsconnection.socket.nonblocking.SelectorHandler;
import com.techx.tradex.marketcollector.configurations.AppConf;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

@Service
public class HtsConnectionPool {
    private static final Logger log = LoggerFactory.getLogger(HtsConnectionPool.class);
    public static final String NO_MATCHED_ACCOUNT_CODE = "NO_MATCHED_ACCOUNT";

    private ConnectionPoolService connectionPoolService;
    private AppConf appConf;

    private List<Connection> connectionHandlers = new ArrayList();
    private List<Integer> connectionCount = new ArrayList();

    @Autowired
    public HtsConnectionPool(
            ConnectionPoolService connectionPoolService,
            AppConf appConf
    ) {
        this.connectionPoolService = connectionPoolService;
        this.appConf = appConf;
    }

    public void acquire(AppConf.Account account, BiConsumer<Connection, Throwable> consumer) {
        if (account == null) {
            this.acquire(consumer, false);
        } else {
            this.acquire(account.getSystem(), account.getUsername(), consumer);
        }
    }

    public CompletableFuture<Connection> acquire(AppConf.Account account, boolean dedicated) {
        if (account == null) {
            return this.acquireFuture(dedicated);
        } else {
            return this.acquireFuture(account.getSystem(), account.getUsername(), SelectorHandler.htsConnectionandlerCreator, dedicated);
        }
    }

    public CompletableFuture<Connection> acquireFuture(
            AppConf.Account account,
            boolean dedicated
    ) {
        if (account == null) {
            return this.acquireFuture(dedicated);
        } else {
            return this.acquireFuture(account.getSystem(), account.getUsername(), SelectorHandler.htsConnectionandlerCreator, dedicated);
        }
    }

    public void acquire(String username, BiConsumer<Connection, Throwable> consumer) {
        this.acquire(null, username, SelectorHandler.htsConnectionandlerCreator, consumer);
    }

    public void acquire(BiConsumer<Connection, Throwable> consumer, boolean dedicated) {
        this.acquire(null, null, SelectorHandler.htsConnectionandlerCreator, consumer, dedicated);
    }

    public void acquire(AppConf.RealtimeAccountConf accountConf,
                        BiConsumer<Connection, Throwable> consumer,
                        boolean dedicated) {
        this.acquire(accountConf.getSystem(), accountConf.getUsername(),
                SelectorHandler.htsConnectionandlerCreator, consumer, dedicated);
    }

    public CompletableFuture<Connection> acquireFuture(boolean dedicated) {
        return this.acquireFuture(null, null, SelectorHandler.htsConnectionandlerCreator, dedicated);
    }

    public void acquire(String system, String username,
                        BiConsumer<Connection, Throwable> consumer) {
        this.acquire(system, username, SelectorHandler.htsConnectionandlerCreator, consumer);
    }

    public CompletableFuture<Connection> acquireFuture(String system, String username) {
        return this.acquireFuture(system, username, SelectorHandler.htsConnectionandlerCreator);
    }

    private boolean isMatch(Connection connection, String system, String username) {
        return (StringUtils.isEmpty(system) || system.equals(connection.getSystem()))
                && (StringUtils.isEmpty(username) || username.equals(connection.getConnection().getLoginData().getUsername()));
    }

    private void onConnectionClosed(Connection con) {
        log.warn("remove connection {} {}", con.system, con.connection.getLoginData().getUsername());
        connectionHandlers.remove(con);
        this.connectionCount.remove(con.account.getId());
    }

    public void acquire(String system, String username,
                        SelectorHandler.CreateConnectionHandler connectionCreator,
                        BiConsumer<Connection, Throwable> consumer) {
        this.acquire(system, username, connectionCreator, consumer, false);
    }

    public CompletableFuture<Connection> acquireFuture(String system, String username,
                                                       SelectorHandler.CreateConnectionHandler connectionCreator) {
        return this.acquireFuture(system, username, connectionCreator, false);
    }

    public void acquire(String system, String username,
                        SelectorHandler.CreateConnectionHandler connectionCreator,
                        BiConsumer<Connection, Throwable> consumer,
                        boolean dedicated
    ) {
        if (this.connectionCount.isEmpty()) {
            for (int i = 0; i < this.appConf.getAccounts().size(); i++) {
                this.connectionCount.add(0);
                this.appConf.getAccounts().get(i).setId(i);
            }
        }
        Connection connection = null;
        log.info("searching for connection system: {} and username: {}", system, username);
        if (!dedicated) {
            for (Connection con : this.connectionHandlers) {
                if (!con.isDedicated() &&
                        !con.getConnection().isDisconnected() &&
                        !con.getConnection().checkConnectionDead() &&
                        this.isMatch(con, system, username)) {
                    connection = con;
                    break;
                }
            }
        }
        if (connection == null) {
            log.warn("cannot find a current connection for system: {} and username: {}. will connect a new one",
                    system, username);
            AppConf.Account account = this.findAccount(system, username);
            if (account == null) {
                log.error("cannot find an account for system: {} and username: {}. Or all account has been taken", system, username);
                consumer.accept(null, new GeneralException(NO_MATCHED_ACCOUNT_CODE));
                return;
            }
            AppConf.SystemConf systemConf = appConf.getSysConf().get(account.getSystem());
            if (systemConf == null) {
                log.error("configuration problem about {}", account);
                consumer.accept(null, new GeneralException());
                return;
            }
            LoginData loginData = new LoginData(account.getUsername(), account.getPassword(),
                    systemConf.getHost(), systemConf.getLoginPort(), systemConf.getDataPort()
                    , systemConf.getMarketPort(), systemConf.getSecCode());
            log.warn("connect a new one {}", loginData.getUsername());
            this.connectionPoolService.connect(loginData, connectionCreator).subscribe(
                    loginRcv -> {
                        log.warn("success fully connect {}", loginData.getUsername());
                        Connection con = new Connection(account, this,
                                ((LogInRcv) loginRcv).getConnectionHandler(), system);
                        con.setDedicated(dedicated);
                        this.connectionHandlers.add(con);
                        this.connectionCount.add(this.connectionCount.get(account.getId()) + 1);
                        consumer.accept(con, null);
                    },
                    err -> consumer.accept(null, (Throwable) err)
            );
        } else {
            log.warn("found a connection for system: {} and username: {}. which is {}",
                    system, username, connection.connection.getLoginData());
            connection.acquire();
            consumer.accept(connection, null);
        }
    }

    public CompletableFuture<Connection> acquireFuture(
            String system,
            String username,
            SelectorHandler.CreateConnectionHandler connectionCreator,
            boolean dedicated
    ) {
        final CompletableFuture<Connection> result = new CompletableFuture<>();
        BiConsumer<Connection, Throwable> consumer = (con, ex) -> {
            if (ex != null) {
                result.completeExceptionally(ex);
            } else {
                result.complete(con);
            }
        };
        this.acquire(system, username, connectionCreator, consumer, dedicated);
        return result;
    }

    public interface ConnectionEvent {
        void onClose();
    }

    private AppConf.Account findAccount(String system, String username) {
        for (int i = 0; i < this.appConf.getAccounts().size(); i++) {
            AppConf.Account acc = this.appConf.getAccounts().get(i);
            if ((StringUtils.isEmpty(system) || system.equals(acc.getSystem()))
                    && (StringUtils.isEmpty(username) || username.equals(acc.getUsername()))
                    && (acc.getLimit() == 0 || this.connectionCount.get(i) < acc.getLimit())
            ) {
                return acc;
            }
        }
        return null;
    }

    @Data
    public static class Connection implements AutoCloseable {
        private AppConf.Account account;
        private HtsConnectionPool htsConnectionPool;
        private BaseHtsConnectionHandler connection;
        private boolean dedicated = false;
        private String system;
        private int numberOfUse;
        private List<ConnectionEvent> subscribers = new ArrayList<>();

        public Connection(
                AppConf.Account account,
                HtsConnectionPool htsConnectionPool,
                BaseHtsConnectionHandler connection,
                String system
        ) {
            this.account = account;
            this.htsConnectionPool = htsConnectionPool;
            this.connection = connection;
            this.system = system;
            this.numberOfUse = 1;
            this.connection.subscribeDisconnectEvent().subscribe(
                    (data) -> this.onConnectionClose(),
                    (error) -> {
                        log.error("got error while subscribing disconnect event on {}",
                                this.connection.getLoginData().getUsername(), error);
                        this.onConnectionClose();
                    }
            );
        }

        public void subscribe(ConnectionEvent event) {
            this.subscribers.add(event);
        }

        private void onConnectionClose() {
            log.warn("connection close {}", this.connection.getLoginData());
            this.htsConnectionPool.onConnectionClosed(this);
            this.subscribers.forEach(sub -> sub.onClose());
        }

        public void acquire() {
            this.numberOfUse++;
        }

        public void release() {
            this.numberOfUse--;
            if (this.getNumberOfUse() <= 0) {
                this.getConnection().close();
                this.htsConnectionPool.connectionHandlers.remove(this);
            }
        }

        public void close() {
            this.release();
        }
    }
}