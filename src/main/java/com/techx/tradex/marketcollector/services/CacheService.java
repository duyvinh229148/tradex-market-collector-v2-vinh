package com.techx.tradex.marketcollector.services;

import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.daos.RedisDao;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import com.techx.tradex.marketcollector.model.realtime.TransformData;
import com.techx.tradex.marketcollector.repositories.MarketRepository;
import com.techx.tradex.marketcollector.repositories.SymbolInfoRepository;
import com.techx.tradex.marketcollector.repositories.SymbolQuoteRepository;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Data
public class CacheService {
    private static final Logger log = LoggerFactory.getLogger(CacheService.class);

    private AppConf appConf;
    private SymbolQuoteRepository symbolQuoteRepository;
    private SymbolInfoRepository symbolInfoRepository;
    private MarketRepository marketRepository;
    private RedisDao redisDao;

    private ConcurrentHashMap<String, String> refIndexCodeMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, String> futuresCodeRefMap = new ConcurrentHashMap<>();
    private Map<String, Integer> mapSequence = new HashMap<>();
    private Map<String, Long> mapLastTradingVolume = new HashMap<>();
    private Calendar timeStartReceiveBidAsk = Calendar.getInstance(); // start time to store bidOfferList from getTbl
    private Calendar timeStopReceiveBidAsk = Calendar.getInstance(); // stop time to store bidOfferList from getTbl
    private double lastVn30 = 0;

    @Autowired
    public CacheService(AppConf appConf, SymbolQuoteRepository symbolQuoteRepository,
                        SymbolInfoRepository symbolInfoRepository, RedisDao redisDao,
                        MarketRepository marketRepository) {
        this.appConf = appConf;
        this.symbolQuoteRepository = symbolQuoteRepository;
        this.symbolInfoRepository = symbolInfoRepository;
        this.marketRepository = marketRepository;
        this.redisDao = redisDao;
    }


    public void reset() {
        log.info("start reset cacheService");
        timeStartReceiveBidAsk = Calendar.getInstance();
        String[] hourMinute = appConf.getTimeStartReceiveBidAsk().split(":");
        timeStartReceiveBidAsk.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourMinute[0]));
        timeStartReceiveBidAsk.set(Calendar.MINUTE, Integer.parseInt(hourMinute[1]));

        timeStopReceiveBidAsk = Calendar.getInstance();
        hourMinute = appConf.getTimeStopReceiveBidAsk().split(":");
        timeStopReceiveBidAsk.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourMinute[0]));
        timeStopReceiveBidAsk.set(Calendar.MINUTE, Integer.parseInt(hourMinute[1]));

        refIndexCodeMap.clear();
        futuresCodeRefMap.clear();
        mapSequence.clear();
        mapLastTradingVolume.clear();

        this.redisDao.getAllSymbolInfo().forEach(symbolInfo -> {
            mapSequence.put(symbolInfo.getCode(), symbolInfo.getSequence());
            mapLastTradingVolume.put(symbolInfo.getCode(), symbolInfo.getTradingVolume());
            if (symbolInfo.getType().equals(SymbolTypeEnum.FUTURES)) {
                this.futuresCodeRefMap.put(symbolInfo.getCode(), symbolInfo.getRefCode());
            } else if (symbolInfo.getType().equals(SymbolTypeEnum.INDEX)) {
                this.refIndexCodeMap.put(symbolInfo.getRefCode(), symbolInfo.getCode());
                if (symbolInfo.getCode().equals("VN30")) {
                    this.lastVn30 = symbolInfo.getLast();
                }
            }
        });
        log.info("finish reset cacheService!");
    }

    @PostConstruct
    public void init() {
        this.reset();
        TransformData.setAppConf(appConf);
    }

    public int getSequence(String code, boolean increment) {
        int seq = mapSequence.getOrDefault(code, 0);
        if (increment) {
            mapSequence.put(code, seq + 1);
        }
        return seq;
    }

    public void resetSequence() {
        log.info("start initSequence");
        long t1 = System.currentTimeMillis();
        this.mapSequence = new HashMap<>();
        List<SymbolInfo> symbolInfoList = this.symbolInfoRepository.findAll();
        symbolInfoList.forEach(symbolInfo -> {
            symbolInfo.setSequence(0);
            symbolInfo.setUpdatedAt(new Date());
        });
        this.marketRepository.updateInBulk(symbolInfoList, SymbolInfo.class);
        log.info("update sequence for database done!");
        this.redisDao.getAllSymbolInfo().forEach(symbolInfo -> {
            symbolInfo.setSequence(0);
            symbolInfo.setUpdatedAt(new Date());
            this.redisDao.setSymbolInfo(symbolInfo);
        });
        log.info("update sequence for redis done!");

        long t2 = System.currentTimeMillis();
        log.info("finish initSequence, take: {}", (t2 - t1));
    }
}
