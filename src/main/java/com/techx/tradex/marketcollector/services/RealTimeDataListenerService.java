package com.techx.tradex.marketcollector.services;

import com.techx.tradex.common.model.kafka.Body;
import com.techx.tradex.common.utils.Operator;
import com.techx.tradex.common.utils.StringUtils;
import com.techx.tradex.common.utils.Transform;
import com.techx.tradex.htsconnection.socket.message.AutoRcv;
import com.techx.tradex.htsconnection.socket.message.receive.*;
import com.techx.tradex.htsconnection.socket.nonblocking.BaseHtsConnectionHandler;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.Symbol;
import com.techx.tradex.marketcollector.model.realtime.*;
import com.techx.tradex.marketcollector.utils.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@Service
@ConditionalOnProperty(name = "test", matchIfMissing = true)
public class RealTimeDataListenerService implements ApplicationRunner, Runnable {
    private static final Logger log = LoggerFactory.getLogger(RealTimeDataListenerService.class);
    private static final long resourceCacheTime = 28800000;
    private HtsConnectionPool htsConnectionPool;
    private SocketClusterService socketClusterService;
    private CacheService cacheService;
    private DownloadSymbolListService downloadSymbolListService;
    private RequestSender requestSender;
    private AppConf appConf;
    private List<String> indexCodes = new ArrayList<>();
    private List<String> stockCodes = new ArrayList<>();
    private List<String> bondCodes = new ArrayList<>();
    private List<String> futuresCodes = new ArrayList<>();
    private List<String> cwCodes = new ArrayList<>();
    private List<Symbol> allCodes = new ArrayList<>();
    private Calendar from = Calendar.getInstance();
    private Calendar to = Calendar.getInstance();
    private volatile boolean isRunning = false;
    private List<RealTimeConnectionHandler> connections = new ArrayList<>();
    private long resourceTime = -1;
    private Map<String, Class> destClassMap = new HashMap<>();
    private MonitorLostBidOffer monitorLostBidOffer;
    private static final int QUEUE_TIMEOUT = 1000; // after 1000ms, queue has been publish.
    private Map<String, SortedSet<TransformData>> mapQueueQuoteUpdate = new HashMap<>();
    private Map<String, AppConf.SendOut> sendOutMap = new HashMap<>();

    @Autowired
    public RealTimeDataListenerService(
            HtsConnectionPool htsConnectionPool
            , DownloadSymbolListService downloadSymbolListService
            , RequestSender requestSender
            , SocketClusterService socketClusterService
            , AppConf appConf
            , CacheService cacheService
            , MonitorLostBidOffer monitorLostBidOffer
    ) {
        this.htsConnectionPool = htsConnectionPool;
        this.downloadSymbolListService = downloadSymbolListService;
        this.requestSender = requestSender;
        this.socketClusterService = socketClusterService;
        this.cacheService = cacheService;
        this.monitorLostBidOffer = monitorLostBidOffer;
        this.appConf = appConf;
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        try {
            from.setTime(sdf.parse(appConf.getRealtime().getWorkingTime().getFrom()));
            to.setTime(sdf.parse(appConf.getRealtime().getWorkingTime().getTo()));
        } catch (Exception e) {
            log.error("Exception on parsing realtime working time {}", appConf.getRealtime().getWorkingTime(), e);
            System.exit(1);
        }
    }


    @Override
    public void run(ApplicationArguments args) {
        this.run();
    }

    public void run() {
        if (this.isRunning) {
            log.info("already running");
            return;
        }
        this.isRunning = true;
        Calendar calendar = Calendar.getInstance();
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        if (appConf.getRealtime().getWorkingTime().getWeekDays().stream().noneMatch(day -> day == weekDay)) {
            log.info("it's not in the week day list. Not started");
            this.isRunning = false;
            return;
        }
        calendar.set(1970, Calendar.JANUARY, 1);
        log.info(from.getTimeInMillis() + " - " + to.getTimeInMillis() + " - " + calendar.getTimeInMillis());
        if (from.after(calendar) || to.before(calendar)) {
            log.info("out of working time. Not started");
            this.isRunning = false;
            return;
        }
        log.info("running realtime data listener. start by downloading resource--");
        downloadResource(this::start);
    }

    public void stop() {
        this.isRunning = false;
        this.clearConnection();
    }

    private void clearConnection() {
        for (RealTimeConnectionHandler con : this.connections) {
            con.stop();
        }
        this.connections.clear();
    }

    private void startAccount(AppConf.RealtimeAccountConf acc) {
        if (acc.getRepeat() != null && acc.getRepeat() > 1) {
            for (int i = 0; i < acc.getRepeat(); i++) {
                this.connections.add(new RealTimeConnectionHandler(this, acc, this.monitorLostBidOffer));
            }
        } else {
            this.connections.add(new RealTimeConnectionHandler(this, acc, this.monitorLostBidOffer));
        }
    }

    private int startAccountPartition(AppConf.RealtimeAccountConf account, int total, int index, Set<String> exceptCodes) {
        if (account.getRepeat() != null && account.getRepeat() > 1) {
            for (int j = 0; j < account.getRepeat(); j++) {
                this.connections.add(new RealTimeConnectionHandler(this, account, this.monitorLostBidOffer, total, index + j, exceptCodes));
            }
            return account.getRepeat();
        } else {
            this.connections.add(new RealTimeConnectionHandler(this, account, this.monitorLostBidOffer, total, index, exceptCodes));
            return 1;
        }
    }

    private void start() {
        this.clearConnection();
        this.sendOutMap = appConf.getRealtime().getTopics();
        log.info("using sendOutMap '{}'", this.sendOutMap);

        appConf.getRealtime().getAccounts().forEach(acc -> {
            if (CollectionUtils.isEmpty(acc.getPartitions())) {
                this.startAccount(acc);
            } else {
                Set<String> exceptCodes = new HashSet<>();
                List<AppConf.RealtimeAccountConf> accounts = new ArrayList<>();
                AtomicInteger total = new AtomicInteger(0);
                acc.getPartitions().forEach(it -> {
                    if (!CollectionUtils.isEmpty(it.getOnlyCodes())) {
                        exceptCodes.addAll(it.getOnlyCodes());
                        this.startAccount(it);
                    } else {
                        accounts.add(it);
                        total.set(total.get() + (it.getRepeat() == null ? 1 : it.getRepeat()));
                    }
                });
                int index = 0;
                for (int i = 0; i < accounts.size(); i++) {
                    AppConf.RealtimeAccountConf account = accounts.get(i);
                    index += this.startAccountPartition(account, total.get(), index, exceptCodes);
                }
            }
            if (appConf.getRealtime().isCount()) {
                new Timer().scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        connections.forEach(connection -> {
                            log.warn("==============status");
                            connection.itemCount.forEach((code, count) -> {
                                StringBuffer sb = new StringBuffer();
                                count.forEach((c, co) -> {
                                    sb.append("\n  - ").append(c.getSimpleName()).append(": ").append(co);
                                });
                                log.warn("- {} {}", code, sb);
                            });
                        });
                    }
                }, 20000L, 20000L);
            }
        });
    }

    private void downloadResource(Operator operator) {
        if (resourceTime != -1 && System.currentTimeMillis() - resourceTime > resourceCacheTime && !allCodes.isEmpty()) {
            log.warn("resource is still valid. not download");
            operator.operate();
            return;
        }
        log.info("download resource");
        indexCodes.clear();
        stockCodes.clear();
        futuresCodes.clear();
        allCodes.clear();
        downloadSymbolListService.download().subscribe(
                map -> {
                    try {
                        map.forEach((k, v) -> {
                            try {
                                if (v.getType().equals(SymbolTypeEnum.INDEX)) {
                                    indexCodes.add(v.getRefCode());
                                } else if (v.getType().equals(SymbolTypeEnum.CW)) {
                                    cwCodes.add(v.getValidCode());
                                } else if (v.getType().equals(SymbolTypeEnum.STOCK)) {
                                    stockCodes.add(v.getValidCode());
                                } else if (v.getType().equals(SymbolTypeEnum.BOND)) {
                                    bondCodes.add(v.getValidCode());
                                } else if (v.getType().equals(SymbolTypeEnum.FUTURES)) {
                                    futuresCodes.add(v.getValidCode());
                                }
                                allCodes.add(v);
                            } catch (Exception e) {
                                log.error("fail to map codes resource {0}", e);
                                throw e;
                            }
                        });
                        this.resourceTime = System.currentTimeMillis();
                        log.info("finish loading codes: {}, {}, {}, {}", indexCodes.size(), stockCodes.size(), futuresCodes.size(), cwCodes);
                        operator.operate();
                    } catch (Exception e) {
                        log.error("fail to get codes resource and do next. Retrying {0}", e);
                        this.downloadResource(operator);
                    }
                },
                err -> {
                    log.error("fail to download resource. retrying... {0}", err);
                    this.downloadResource(operator);
                }
        );
    }


    private class RealTimeConnectionHandler implements HtsConnectionPool.ConnectionEvent {
        private RealTimeDataListenerService realTimeDataListenerService;
        private AppConf.RealtimeAccountConf accountConf;
        private BaseHtsConnectionHandler connectionHandler;
        private MonitorLostBidOffer monitorLostBidOffer;
        private HtsConnectionPool.Connection connection;

        private String baseTransformPackageName;
        private AppConf.SendOut sendOut = null;
        private Class<?> destClass;
        private int totalPartition = 0;
        private int partitionIndex = 0;
        private List<String> items;
        private Map<String, Map<Class<?>, Integer>> itemCount = new HashMap<>();
        private Set<String> exceptCodes;
        private int retryIndex = 0;

        public RealTimeConnectionHandler(RealTimeDataListenerService realTimeDataListenerService,
                                         AppConf.RealtimeAccountConf accountConf,
                                         MonitorLostBidOffer monitorLostBidOffer
        ) {
            init(realTimeDataListenerService, accountConf, monitorLostBidOffer, 0, 0);
        }

        public RealTimeConnectionHandler(RealTimeDataListenerService realTimeDataListenerService,
                                         AppConf.RealtimeAccountConf accountConf,
                                         MonitorLostBidOffer monitorLostBidOffer,
                                         int totalPartition,
                                         int partitionIndex,
                                         Set<String> exceptCodes
        ) {
            init(realTimeDataListenerService, accountConf,
                    monitorLostBidOffer, totalPartition, partitionIndex);
            this.exceptCodes = exceptCodes;
        }

        private void init(RealTimeDataListenerService realTimeDataListenerService,
                          AppConf.RealtimeAccountConf accountConf,
                          MonitorLostBidOffer monitorLostBidOffer,
                          int totalPartition,
                          int partitionIndex
        ) {
            this.totalPartition = totalPartition;
            this.partitionIndex = partitionIndex;
            this.realTimeDataListenerService = realTimeDataListenerService;
            this.monitorLostBidOffer = monitorLostBidOffer;
            this.accountConf = accountConf;
            this.baseTransformPackageName = IndexUpdateData.class.getPackage().getName();
            this.start();
        }

        @Override
        public void onClose() {
            if (this.realTimeDataListenerService.isRunning) {
                log.error("{} disconnected. retrying...", this.accountConf);
                this.start();
            } else {
                log.warn("{} disconnected", this.accountConf);
            }
        }

        private void start() {
            if (this.retryIndex > appConf.getRealtime().getMaxRetry()) {
                log.error("retry exceeded {}. Stop now", this.accountConf);
                return;
            }
            this.retryIndex++;
            this.realTimeDataListenerService.htsConnectionPool.acquire(this.accountConf, (connection, err) -> {
                if (err != null) {
                    log.error("{} an error occur when trying to connect. Retrying..", this.accountConf);
                    log.error("{0}", err);
                    if (this.realTimeDataListenerService.isRunning) {
                        this.start();
                    }
                } else {
                    this.retryIndex = 0;
                    this.connection = connection;
                    this.connection.subscribe(this);
                    this.connectionHandler = connection.getConnection();
                    this.connectRealTime();
                }
            }, true);
        }

        private void stop() {
            this.connection.release();
        }

        private void connectRealTime() {
            log.warn("{} will connect realtime", this.accountConf);
            this.connectionHandler.connectRealTime(true).subscribe(
                    autoRcv -> this.receivePacket((AutoRcv) autoRcv)
                    , err -> {
                        log.error("fail to connect realtime {} - {}", this.accountConf, err);
                        try {
                            this.connectionHandler.close();
                        } catch (Exception e) {
                            /*swallow*/
                        }
                    }
            );
        }

        private void receivePacket(AutoRcv autoRcv) {
            if (autoRcv == null) {
                this.startSubscribeAuto();
            } else {
                if (autoRcv.getItems() == null || autoRcv.getItems().isEmpty()) {
                    return;
                }
                if (appConf.isLogRealtimePacket()) {
                    log.info("got an real time packet {}", autoRcv);
                } else {
                    log.trace("got an real time packet {}", autoRcv.getType().getValue());
                }
                this.destClass = null;
                this.sendOut = null;
                for (int i = 0; i < autoRcv.getItems().size(); i++) {
                    AutoRcv item = autoRcv.getItems().get(i);
                    Class<?> clazz = item.getClass();
                    if (clazz.getSimpleName().contains("A78") || clazz.getSimpleName().contains("Tick")) {
                        log.error("receive A78|Tick: " + item);
                    }
                    if (appConf.isEnableMonitorLostBidOffer()) {
                        if (clazz.equals(BidOfferAutoItem.class)) {
                            monitorLostBidOffer.receiveBidOffer(((BidOfferAutoItem) item).getStockCode().getValue());
                        } else if (clazz.equals(StockAutoItem.class)) {
                            monitorLostBidOffer.receiveQuote(((StockAutoItem) item).getStockCode().getValue());
                        }
                    }
                    String className = clazz.getSimpleName();
                    if (accountConf.getCodeType() != null && accountConf.getCodeType().equalsIgnoreCase(SymbolTypeEnum.CW.name())) {
                        className = accountConf.getTopicMapping().get(className);
                    }
                    sendOut = this.realTimeDataListenerService.sendOutMap.get(className);
                    if (sendOut == null) {
                        log.warn("cannot find a sending out config for class {}", clazz);
                        break;
                    }
                    if (destClassMap.containsKey(sendOut.getTransformTo())) {
                        destClass = destClassMap.get(sendOut.getTransformTo());
                        if (destClass == null) {
                            break;
                        }
                    } else {
                        try {
                            destClass = Class.forName(this.baseTransformPackageName + "." + sendOut.getTransformTo());
                            if (!Transform.class.isAssignableFrom(destClass)) {
                                log.error("transform class does not implement Transform {}", sendOut.getTransformTo());
                                destClassMap.put(sendOut.getTransformTo(), null);
                                break;
                            }
                            if (!Body.class.isAssignableFrom(destClass)) {
                                log.error("transform class does not implement Body {}", sendOut.getTransformTo());
                                destClassMap.put(sendOut.getTransformTo(), null);
                                break;
                            }
                        } catch (ClassNotFoundException e) {
                            log.error("cannot find transform class for realtime data {}", sendOut.getTransformTo());
                            destClassMap.put(sendOut.getTransformTo(), null);
                            break;
                        }
                    }

                    try {
                        AutoRcv it = autoRcv.getItems().get(i);
                        TransformData<AutoRcv, ? extends TransformData> destInstance = (TransformData) destClass.newInstance();
                        destInstance = destInstance.from(it);
                        if (appConf.getRealtime().isCount()) {
                            Map<Class<?>, Integer> count = this.itemCount.get(destInstance.getCode());
                            if (count != null) {
                                count.put(it.getClass(), count.get(it.getClass()) + 1);
                            }
                        }
                        try {
                            destInstance.validate();
                            destInstance.formatTime();
                            destInstance.formatRefCode(cacheService);
                            destInstance.parseStatus(appConf.getStatusMap());
                            destInstance.setSendOut(className);
                            if (destClass.equals(IndexUpdateData.class)) {
                                IndexUpdateData indexUpdateData = (IndexUpdateData) destInstance.toRealObject();
                                if (indexUpdateData.getCode().equals("VN30")) {
                                    cacheService.setLastVn30(indexUpdateData.getLast());
                                }
                                int sequence = cacheService.getSequence(destInstance.getCode(), true);
                                destInstance.setSequence(sequence);
                                log.info("index code: {} _ sequence: {}", destInstance.getCode(), sequence);
                            } else if (destClass.equals(FuturesUpdateData.class)) {
                                FuturesUpdateData futuresUpdateData = (FuturesUpdateData) destInstance.toRealObject();
                                long lastTradingVolume = cacheService.getMapLastTradingVolume().getOrDefault(destInstance.getCode(), 0L);
                                long tradingVolume = futuresUpdateData.getTradingVolume();
                                long matchingVolume = futuresUpdateData.getMatchingVolume();
                                if (cacheService.getLastVn30() > 0 && futuresUpdateData.getCode().startsWith("VN30")) {
                                    futuresUpdateData.setBasis(NumberUtils.round2Decimal(futuresUpdateData.getLast() - cacheService.getLastVn30()));
                                }
                                if (lastTradingVolume == 0 || (tradingVolume - matchingVolume == lastTradingVolume)) {
                                    int sequence = cacheService.getSequence(destInstance.getCode(), true);
                                    destInstance.setSequence(sequence);
                                    log.info("futures code: {} _ sequence: {}", destInstance.getCode(), sequence);
                                    cacheService.getMapLastTradingVolume().put(destInstance.getCode(), futuresUpdateData.getTradingVolume());
                                } else if (lastTradingVolume - tradingVolume == 0) {
                                    log.warn("Ignore futures quote by lastTradingVolume:  {} _ {}", lastTradingVolume, destInstance);
                                    break;
                                } else if ((tradingVolume - matchingVolume < lastTradingVolume)) {
                                    log.warn("Ignore futures quote by lastTradingVolume:  {} _ {}", lastTradingVolume, destInstance);
                                    break;
                                } else if ((tradingVolume - matchingVolume > lastTradingVolume)) {
                                    log.error("Wrong order, lastTradingVolume: {} _ futuresQuote: {} -> add To Queue", lastTradingVolume, destInstance);
                                    addUpdateDataToQueue(futuresUpdateData);
                                    break;
                                }

                            } else if (destClass.equals(StockUpdateData.class)) {
                                StockUpdateData stockUpdateData = (StockUpdateData) destInstance.toRealObject();
                                long lastTradingVolume = cacheService.getMapLastTradingVolume().getOrDefault(destInstance.getCode(), 0L);
                                long tradingVolume = stockUpdateData.getTradingVolume();
                                long matchingVolume = stockUpdateData.getMatchingVolume();

                                if (lastTradingVolume == 0 || (tradingVolume - matchingVolume == lastTradingVolume)) {
                                    int sequence = cacheService.getSequence(destInstance.getCode(), true);
                                    destInstance.setSequence(sequence);
                                    log.info("stock code: {} _ sequence: {}", destInstance.getCode(), sequence);
                                    cacheService.getMapLastTradingVolume().put(destInstance.getCode(), stockUpdateData.getTradingVolume());
                                } else if (lastTradingVolume - tradingVolume == 0) {
                                    log.warn("Ignore stock quote by lastTradingVolume:  {} _ {}", lastTradingVolume, destInstance);
                                    break;
                                } else if ((tradingVolume - matchingVolume < lastTradingVolume)) {
                                    log.warn("Ignore stock quote by lastTradingVolume:  {} _ {}", lastTradingVolume, destInstance);
                                    break;
                                } else if (tradingVolume - matchingVolume > lastTradingVolume) {
                                    log.error("Wrong order, lastTradingVolume: {} _ stockQuote: {} -> add To Queue", lastTradingVolume, destInstance);
                                    addUpdateDataToQueue(stockUpdateData);
                                    break;
                                }

                            } else if (destClass.equals(CWUpdateData.class)) {
                                CWUpdateData cwUpdateData = (CWUpdateData) destInstance.toRealObject();
                                long lastTradingVolume = cacheService.getMapLastTradingVolume().getOrDefault(destInstance.getCode(), 0L);
                                long tradingVolume = cwUpdateData.getTradingVolume();
                                long matchingVolume = cwUpdateData.getMatchingVolume();

                                if (lastTradingVolume == 0 || (tradingVolume - matchingVolume == lastTradingVolume)) {
                                    int sequence = cacheService.getSequence(destInstance.getCode(), true);
                                    destInstance.setSequence(sequence);
                                    log.info("cw code: {} _ sequence: {}", destInstance.getCode(), sequence);
                                    cacheService.getMapLastTradingVolume().put(destInstance.getCode(), cwUpdateData.getTradingVolume());
                                } else if (lastTradingVolume - tradingVolume == 0) {
                                    log.warn("Ignore cw quote by lastTradingVolume:  {} _ {}", lastTradingVolume, destInstance);
                                    break;
                                } else if ((tradingVolume - matchingVolume < lastTradingVolume)) {
                                    log.warn("Ignore cw quote by lastTradingVolume:  {} _ {}", lastTradingVolume, destInstance);
                                    break;
                                } else if (tradingVolume - matchingVolume > lastTradingVolume) {
                                    log.error("Wrong order, lastTradingVolume: {} _ cwQuote: {} -> add To Queue", lastTradingVolume, destInstance);
                                    addUpdateDataToQueue(cwUpdateData);
                                    break;
                                }
                            }
                        } catch (TransformData.IgnoreException e) {
                            log.warn("ignore realtime packet item {}", item, e);
                            continue;
                        }
                        this.realTimeDataListenerService.scPublish(this.sendOut, destInstance);
                        this.realTimeDataListenerService.kafkaPublish(sendOut, destInstance);
                    } catch (IllegalAccessException | InstantiationException e) {
                        log.error("fail to init item of class {}", destClass);
                        break;
                    }
                }
            }
        }

        private void startSubscribeAuto() {
            log.info("start register auto classes");
            accountConf.getTopics().forEach(topic -> {
                try {
                    Class clazz = Class.forName(ClassRegistration.class.getPackage().getName() + "." + topic);
                    if (AutoRcv.class.isAssignableFrom(clazz)) {
                        if (this.items == null) {
                            this.items = new ArrayList<>();
                            this.items.addAll(getCodes(clazz, this.accountConf));
                            if (!CollectionUtils.isEmpty(this.accountConf.getOnlyCodes())) {
                                items.clear();
                                items.addAll(this.accountConf.getOnlyCodes());
                            }
                            if (!CollectionUtils.isEmpty(this.exceptCodes)) {
                                this.items.removeAll(this.exceptCodes);
                            }
                            if (this.totalPartition > 0) {
                                int partitionSize = (int) Math.ceil((double) (this.items.size()) / (double) this.totalPartition);
                                int startIndex = partitionSize * this.partitionIndex;
                                int endIndex = (this.partitionIndex < this.totalPartition - 1) ? startIndex + partitionSize : this.items.size();
                                this.items = this.items.subList(startIndex, endIndex);
                            }
                        }
                        if (appConf.getRealtime().isCount()) {
                            this.items.forEach(item -> {
                                Map<Class<?>, Integer> count = new HashMap<>();
                                count.put(StockAutoItem.class, 0);
                                count.put(BidOfferAutoItem.class, 0);
                                itemCount.put(item, count);
                            });
                        }
                        log.info("register {} {} {}", clazz, this.items.size(), this.items);
                        this.connectionHandler.subscribeSpecificAuto(this.items, clazz, appConf.getRealtime().getNumberCodesToRegister());
                    } else {
                        log.error("Auto topic class {} of topic {} is not derived class from AutoRcv", clazz, topic);
                        System.exit(1);
                    }
                } catch (ClassNotFoundException e) {
                    log.error("cannot find topic auto class for name {}", topic);
                    System.exit(1);
                }
            });
        }

        private List<String> getCodes(Class clazz, AppConf.RealtimeAccountConf conf) {
            if (conf != null && StringUtils.isNotEmpty(conf.getCodeType())) {
                if (SymbolTypeEnum.CW.name().equals(conf.getCodeType())) {
                    return cwCodes;
                } else if (SymbolTypeEnum.BOND.name().equals(conf.getCodeType())) {
                    return new ArrayList<>();
                } else if (SymbolTypeEnum.FUTURES.name().equals(conf.getCodeType())) {
                    return futuresCodes;
                } else if (SymbolTypeEnum.STOCK.name().equals(conf.getCodeType())) {
                    return stockCodes;
                } else if (SymbolTypeEnum.INDEX.name().equals(conf.getCodeType())) {
                    return indexCodes;
                }
            }
            if (appConf.getRealtime().getSubscribeFilter() != null
                    && appConf.getRealtime().getSubscribeFilter().containsKey(clazz.getSimpleName())) {
                boolean isIndex = clazz.equals(IndexAutoItem.class);
                return allCodes.stream().filter(si -> {
                    if (isIndex ^ si.getType().equals(SymbolTypeEnum.INDEX)) {
                        return false;
                    }
                    AppConf.SubscribeFilter subscribeFilter = appConf.getRealtime().getSubscribeFilter()
                            .get(clazz.getSimpleName());
                    if (subscribeFilter.getIncludes() != null && subscribeFilter.getIncludes().getExchanges() != null) {
                        for (String ex : subscribeFilter.getIncludes().getExchanges()) {
                            if (ex.equals(si.getExchange())) {
                                return true;
                            }
                        }
                    }
                    return false;
                }).map(Symbol::getValidCode).collect(Collectors.toList());
            }

            if (clazz.equals(IndexAutoItem.class)) {
                return this.realTimeDataListenerService.indexCodes;
            } else if (clazz.equals(FuturesAutoItem.class)) {
                return this.realTimeDataListenerService.futuresCodes;
            } else if (clazz.equals(FuturesBidOfferItem.class)) {
                return this.realTimeDataListenerService.futuresCodes;
            } else {
                return this.realTimeDataListenerService.stockCodes;
            }
        }
    }

    private void scPublish(AppConf.SendOut sendOut, TransformData destInstance) {
        if (StringUtils.isNotEmpty(sendOut.getScPublishTo())) {
            if (sendOut.isScPublishToCommonChannel()) {
                socketClusterService.publish(sendOut.getScPublishTo(), QuoteData.fromQuoteDataUpdate(destInstance));
            }
            socketClusterService.publish(sendOut.getScPublishTo() + "." + destInstance.getCode(), QuoteData.fromQuoteDataUpdate(destInstance));
        }
    }

    private void kafkaPublish(AppConf.SendOut sendOut, Body data) {
        if (sendOut != null) {
            this.requestSender.sendMiniMessageSafeNoResponse(sendOut.getTopic(), sendOut.getUri(), data);
        }
    }

    private void addUpdateDataToQueue(TransformData quoteData) {
        quoteData.setTimeInsertToQueue(new Date().getTime());
        if (this.mapQueueQuoteUpdate.containsKey(quoteData.getCode())) {
            SortedSet<TransformData> queue = this.mapQueueQuoteUpdate.getOrDefault(quoteData.getCode(), new TreeSet<>((p1, p2) ->
                    (int) (p1.getTradingVolume() - p2.getTradingVolume())
            ));
            queue.add(quoteData);
        } else {
            SortedSet<TransformData> queue = new TreeSet<>((p1, p2) ->
                    (int) (p1.getTradingVolume() - p2.getTradingVolume())
            );
            queue.add(quoteData);
            this.mapQueueQuoteUpdate.put(quoteData.getCode(), queue);
        }

    }

    public void checkQueueQuoteTimeOut() {
        long currentTime = new Date().getTime();
        this.mapQueueQuoteUpdate.forEach((code, queue) -> {
            if (!queue.isEmpty() && currentTime - queue.last().getTimeInsertToQueue() >= QUEUE_TIMEOUT) {
                List<TransformData> autoDataList = new ArrayList<>(queue);
                queue.clear();
                AppConf.SendOut sendOut = this.sendOutMap.get(autoDataList.get(0).getSendOut());
                autoDataList.forEach(autoData -> {
                    int sequence = cacheService.getSequence(autoData.getCode(), true);
                    autoData.setSequence(sequence);
                    scPublish(sendOut, autoData);
                    kafkaPublish(sendOut, autoData);
                    cacheService.getMapLastTradingVolume().put(autoData.getCode(), autoData.getTradingVolume());
                });
            }
        });
    }

}
