package com.techx.tradex.marketcollector.services;

import com.ea.async.Async;
import com.techx.tradex.common.utils.DefaultUtils;
import com.techx.tradex.htsconnection.socket.message.receive.*;
import com.techx.tradex.htsconnection.socket.message.send.MarketCWQuoteListSnd;
import com.techx.tradex.htsconnection.socket.message.send.MarketFuturesQuoteListSnd;
import com.techx.tradex.htsconnection.socket.message.send.MarketIndexQuoteListSnd;
import com.techx.tradex.htsconnection.socket.message.send.MarketStockQuoteListSnd;
import com.techx.tradex.htsconnection.socket.nonblocking.HtsConnectionHandler;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.constants.MappingHTS;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.*;
import com.techx.tradex.marketcollector.model.request.DownloadRequest;
import com.techx.tradex.marketcollector.repositories.MarketRepository;
import com.techx.tradex.marketcollector.repositories.SymbolInfoRepository;
import com.techx.tradex.marketcollector.repositories.SymbolQuoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class DownloadAutoDataService {
    private static final Logger log = LoggerFactory.getLogger(DownloadAutoDataService.class);

    private AppConf appConf;
    private HtsConnectionPool htsConnectionPool;
    private MarketRepository marketRepository;
    private SymbolInfoRepository symbolInfoRepository;
    private SymbolQuoteRepository symbolQuoteRepository;
    private RedisService redisService;


    @Autowired
    public DownloadAutoDataService(
            AppConf appConf,
            HtsConnectionPool htsConnectionPool,
            MarketRepository marketRepository,
            SymbolInfoRepository symbolInfoRepository,
            SymbolQuoteRepository symbolQuoteRepository,
            RedisService redisService) {
        this.appConf = appConf;
        this.htsConnectionPool = htsConnectionPool;
        this.marketRepository = marketRepository;
        this.symbolInfoRepository = symbolInfoRepository;
        this.symbolQuoteRepository = symbolQuoteRepository;
        this.redisService = redisService;
    }


    public Observable<Boolean> downloadStockQuote(DownloadRequest downloadRequest) {
        log.info("download StockQuote: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.STOCK.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.STOCK.name());
        }
        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlStockQuote(htsConnectionHandler, symbolInfoList, downloadRequest.isOverride()).thenAccept(aVoid -> {
                    log.info("Finished download symbolQuote");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlStockQuote(HtsConnectionHandler htsConnectionHandler, List<SymbolInfo> symbolInfoList, boolean isOverride) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketStockQuoteListSnd snd = new MarketStockQuoteListSnd();
                snd.getStockCode().setValue(symbolInfo.getCode());
                snd.getIReadCount().setValue(50);
                List<SymbolQuote> result = new ArrayList<>();
                Async.await(queryStockQuote(htsConnectionHandler, symbolInfo, snd, result));
                int size = result.size();
                for (int j = 0; j < size; j++) {
                    result.get(j).setSequence(size - j - 1);
                }
                saveSymbolQuote(result, symbolInfo, isOverride);
                log.info("{}  Finish Crawl StockQuote code: {} - size: {}", i, symbolInfo.getCode(), result.size());

            }
            log.info("Finish Crawl SymbolQuote");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryStockQuote(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo, MarketStockQuoteListSnd snd, List<SymbolQuote> result) {
        log.info("querySymbolQuote: {}", symbolInfo.getCode());
        MarketStockQuoteListRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketStockQuoteListRcv.class));
        log.info("{} _ receive data {}", snd.getStockCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketStockQuoteItem item = data.getItems().get(i);
                SymbolQuote symbolQuote = new SymbolQuote();
                String quoteDate = DefaultUtils.DATE_FORMAT().format(new Date());
                symbolQuote.setId(symbolInfo.getCode() + "_" + quoteDate + "_" + item.getTradingVolume().getValue());
                symbolQuote.setCode(symbolInfo.getCode());
                symbolQuote.setType(symbolInfo.getType());
                MappingHTS.MAP_STOCK_MATCH_LIST.accept(item, symbolQuote);
                result.add(symbolQuote);
            }
            MarketStockQuoteItem lastItem = data.getItems().get(data.getItems().size() - 1);
            MarketStockQuoteStatus status = new MarketStockQuoteStatus();
            String time = lastItem.getTime().getValue();
            time = time.replace(":", "");
            status.getBaseTime().setValue(Integer.parseInt(time));
            snd.setStatus(status);
            snd.getExt().setValue(lastItem.getMatchVolume().getValue());
            snd.getFunctionID().setValue(1);
            Async.await(queryStockQuote(htsConnectionHandler, symbolInfo, snd, result));

        } else {
            log.info("Finish Crawl quote {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }


    public Observable<Boolean> downloadIndexQuote(DownloadRequest downloadRequest) {
        log.info("download IndexQuote: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.INDEX.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.INDEX.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlIndexQuote(htsConnectionHandler, symbolInfoList, downloadRequest.isOverride()).thenAccept(aVoid -> {
                    log.info("Finished downloadIndexQuote");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlIndexQuote(HtsConnectionHandler htsConnectionHandler, List<SymbolInfo> symbolInfoList, boolean isOverride) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketIndexQuoteListSnd snd = new MarketIndexQuoteListSnd();
                snd.getIndexCode().setValue(symbolInfo.getRefCode());
                snd.getIReadCount().setValue(50);
                List<SymbolQuote> result = new ArrayList<>();
                Async.await(queryIndexQuote(htsConnectionHandler, symbolInfo, snd, result));
                int size = result.size();
                for (int j = 0; j < size; j++) {
                    result.get(j).setSequence(size - j - 1);
                }
                saveSymbolQuote(result, symbolInfo, isOverride);
                log.info("{}  Finish Crawl IndexQuote code: {} - size: {}", i, symbolInfo.getCode(), result.size());
            }
            log.info("Finish handleCrawlIndexQuote");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryIndexQuote(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo, MarketIndexQuoteListSnd snd, List<SymbolQuote> result) {
        log.info("queryIndexQuote: {}", symbolInfo.getCode());
        MarketIndexQuoteListRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketIndexQuoteListRcv.class));
        log.info("{} _ receive data {}", snd.getIndexCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketIndexQuoteItem item = data.getItems().get(i);
                SymbolQuote symbolQuote = new SymbolQuote();
                String quoteDate = DefaultUtils.DATE_FORMAT().format(new Date());
                symbolQuote.setId(symbolInfo.getCode() + "_" + quoteDate + "_" + item.getTradingVolume().getValue());
                symbolQuote.setCode(symbolInfo.getCode());
                symbolQuote.setType(symbolInfo.getType());
                MappingHTS.MAP_INDEX_MATCH_LIST.accept(item, symbolQuote);
                result.add(symbolQuote);
            }
            MarketIndexQuoteItem lastItem = data.getItems().get(data.getItems().size() - 1);
            MarketIndexQuoteStatus status = new MarketIndexQuoteStatus();
            String time = lastItem.getTime().getValue();
            time = time.replace(":", "");
            status.getBaseTime().setValue(Integer.parseInt(time));
            snd.setStatus(status);
            snd.getFunctionID().setValue(1);
            Async.await(queryIndexQuote(htsConnectionHandler, symbolInfo, snd, result));

        } else {
            log.info("Finish queryIndexQuote {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }

    public Observable<Boolean> downloadCWQuote(DownloadRequest downloadRequest) {
        log.info("download CWQuote: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.CW.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.CW.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlCWQuote(htsConnectionHandler, symbolInfoList, downloadRequest.isOverride()).thenAccept(aVoid -> {
                    log.info("Finished download symbolQuote");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlCWQuote(HtsConnectionHandler htsConnectionHandler, List<SymbolInfo> symbolInfoList, boolean isOverride) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketCWQuoteListSnd snd = new MarketCWQuoteListSnd();
                snd.getCwCode().setValue(symbolInfo.getCode());
                snd.getIReadCount().setValue(50);
                List<SymbolQuote> result = new ArrayList<>();
                Async.await(queryCWQuote(htsConnectionHandler, symbolInfo, snd, result));
                int size = result.size();
                for (int j = 0; j < size; j++) {
                    result.get(j).setSequence(size - j - 1);
                }
                saveSymbolQuote(result, symbolInfo, isOverride);
                log.info("{}  Finish Crawl SymbolQuote code: {} - size: {}", i, symbolInfo.getCode(), result.size());
            }
            log.info("Finish Crawl SymbolQuote");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryCWQuote(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo, MarketCWQuoteListSnd snd, List<SymbolQuote> result) {
        log.info("querySymbolQuote: {}", symbolInfo.getCode());
        MarketCWQuoteListRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketCWQuoteListRcv.class));
        log.info("{} _ receive data {}", snd.getCwCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketCWQuoteItem item = data.getItems().get(i);
                SymbolQuote symbolQuote = new SymbolQuote();
                String quoteDate = DefaultUtils.DATE_FORMAT().format(new Date());
                symbolQuote.setId(symbolInfo.getCode() + "_" + quoteDate + "_" + item.getTradingVolume().getValue());
                symbolQuote.setCode(symbolInfo.getCode());
                symbolQuote.setType(symbolInfo.getType());
                MappingHTS.MAP_CW_MATCH_LIST.accept(item, symbolQuote);
                result.add(symbolQuote);
            }
            MarketCWQuoteItem lastItem = data.getItems().get(data.getItems().size() - 1);
            MarketCWQuoteStatus status = new MarketCWQuoteStatus();
            String time = lastItem.getTime().getValue();
            time = time.replace(":", "");
            status.getBaseTime().setValue(Integer.parseInt(time));
            snd.setStatus(status);
            snd.getExt().setValue(lastItem.getMatchVolume().getValue());
            snd.getFunctionID().setValue(1);
            Async.await(queryCWQuote(htsConnectionHandler, symbolInfo, snd, result));

        } else {
            log.info("Finish Crawl quote {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }

    public Observable<Boolean> downloadFuturesQuote(DownloadRequest downloadRequest) {
        log.info("download FuturesQuote: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.FUTURES.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.FUTURES.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlFuturesQuote(htsConnectionHandler, symbolInfoList, downloadRequest.isOverride()).thenAccept(aVoid -> {
                    log.info("Finished download symbolQuote");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlFuturesQuote(HtsConnectionHandler htsConnectionHandler, List<SymbolInfo> symbolInfoList, boolean isOverride) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketFuturesQuoteListSnd snd = new MarketFuturesQuoteListSnd();
                snd.getFuturesCode().setValue(symbolInfo.getCode());
                snd.getIReadCount().setValue(10);
                List<SymbolQuote> result = new ArrayList<>();
                Async.await(queryFuturesQuote(htsConnectionHandler, symbolInfo, snd, result));
                int size = result.size();
                for (int j = 0; j < size; j++) {
                    result.get(j).setSequence(size - j - 1);
                }
                saveSymbolQuote(result, symbolInfo, isOverride);
                log.info("{}  Finish Crawl Futures code: {} - size: {}", i, symbolInfo.getCode(), result.size());
            }
            log.info("Finish Crawl SymbolQuote");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryFuturesQuote(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo, MarketFuturesQuoteListSnd snd, List<SymbolQuote> result) {
        log.info("querySymbolQuote: {}", symbolInfo.getCode());
        MarketFuturesQuoteListRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketFuturesQuoteListRcv.class));
        log.info("{} _ receive data {}", snd.getFuturesCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketFuturesQuoteItem item = data.getItems().get(i);
                SymbolQuote symbolQuote = new SymbolQuote();
                String quoteDate = DefaultUtils.DATE_FORMAT().format(new Date());
                symbolQuote.setId(symbolInfo.getCode() + "_" + quoteDate + "_" + item.getTradingVolume().getValue());
                symbolQuote.setCode(symbolInfo.getCode());
                symbolQuote.setType(symbolInfo.getType());
                symbolQuote.setRefCode(symbolInfo.getRefCode());
                MappingHTS.MAP_FUTURES_MATCH_LIST.accept(item, symbolQuote);
                result.add(symbolQuote);
            }
            if (data.getStatus() != null) {
                MarketFuturesQuoteItem lastItem = data.getItems().get(data.getItems().size() - 1);
                MarketFuturesQuoteStatus status = new MarketFuturesQuoteStatus();
                String time = lastItem.getTime().getValue();
                time = time.replace(":", "");
                status.getBaseTime().setValue(Integer.parseInt(time));
                snd.setStatus(status);
                snd.getExt().setValue(lastItem.getMatchVolume().getValue());
                snd.getFunctionID().setValue(1);
                Async.await(queryFuturesQuote(htsConnectionHandler, symbolInfo, snd, result));
            } else {
                log.info("Finish Crawl quote {} because status = null", symbolInfo.getCode());
            }
        } else {
            log.info("Finish Crawl quote {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }

    public void backupAutoData() {
        DownloadRequest request = new DownloadRequest();
        request.setOverride(false);
        this.downloadFuturesQuote(request)
                .subscribe(aBoolean -> this.downloadIndexQuote(request)
                        .subscribe(aBoolean1 -> this.downloadCWQuote(request)
                                .subscribe(aBoolean2 -> this.downloadStockQuote(request))));
    }

    private void saveSymbolQuote(List<SymbolQuote> symbolQuoteList, SymbolInfo symbolInfo, boolean isOverride) {
        List<SymbolQuoteMinute> symbolQuoteMinuteList = marketRepository.groupSymbolQuoteMinute(symbolInfo.getCode());

        // create backup
        marketRepository.updateInBulkNonCheckClazz(symbolQuoteList, SymbolQuoteBackup.class);
        marketRepository.updateInBulkNonCheckClazz(symbolQuoteMinuteList, SymbolQuoteMinuteBackup.class);

        if (isOverride) {
            // update current quote
            marketRepository.updateInBulk(symbolQuoteList, SymbolQuote.class);
            marketRepository.updateInBulk(symbolQuoteMinuteList, SymbolQuoteMinute.class);
            // reload redis cache
            redisService.reloadSymbolQuote(symbolInfo.getCode(), symbolQuoteList);
            redisService.reloadSymbolQuoteMinute(symbolInfo.getCode(), symbolQuoteMinuteList);
        }
    }
}
