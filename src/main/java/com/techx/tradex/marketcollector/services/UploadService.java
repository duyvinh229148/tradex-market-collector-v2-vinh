package com.techx.tradex.marketcollector.services;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.techx.tradex.marketcollector.configurations.AppConf;
import io.minio.MinioClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;

@Service
public class UploadService implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(UploadService.class);
    private MinioClient minioClient;
    private AmazonS3 s3Client;

    @Autowired
    private AppConf appConf;
    private String s3BucketName;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (this.appConf.getMinio() != null && this.appConf.isEnableMinio()) {
            try {
                this.minioClient = new MinioClient(appConf.getMinio().getBaseUrl(),
                        appConf.getMinio().getAccessKey(), appConf.getMinio().getPrivateKey());
                log.info("checking minio bucket exist");
                boolean isExist = minioClient.bucketExists(appConf.getMinio().getBucketName());
                if (isExist) {
                    log.info("Minio bucket already exists.");
                } else {
                    log.warn("Creating bucket");
                    minioClient.makeBucket(appConf.getMinio().getBucketName());
                    String policy = appConf.getMinio().getBucketPolicy();
                    log.warn("Set bucket policy '{}'", policy);
                    minioClient.setBucketPolicy(appConf.getMinio().getBucketName(), policy);
                }
                if (!appConf.getMinio().getUrlRewriteTo().endsWith("/")) {
                    appConf.getMinio().setUrlRewriteTo(appConf.getMinio().getUrlRewriteTo() + "/");
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        if (this.appConf.getS3() != null && this.appConf.isEnableS3()) {
            AWSCredentials credentials = new BasicAWSCredentials(this.appConf.getS3().getAccessKey(), this.appConf.getS3().getPrivateKey());
            Regions regions = Regions.fromName(this.appConf.getS3().getRegion());
            this.s3Client = AmazonS3ClientBuilder.standard().
                    withCredentials(new AWSStaticCredentialsProvider(credentials)).
                    withRegion(regions).
                    build();
            this.s3BucketName = this.appConf.getS3().getBucketName();
        }
    }

    public String uploadFile(File file, String path, boolean privateAccess) {
        log.info("______start uploadFile");
        String result = null;
        if (this.s3Client != null) {
            log.info("upload to s3");
            try {
                this.s3Client.putObject(new PutObjectRequest(this.s3BucketName, path, file)
                        .withCannedAcl(privateAccess ? CannedAccessControlList.Private : CannedAccessControlList.PublicRead));
                result = s3Client.getUrl(this.s3BucketName, path).toString();
            } catch (Exception e) {
                log.error("fail to upload to S3: {},{}", e, path);
            }
        }
        if (this.minioClient != null) {
            log.info("upload to minio");
            try {
                this.minioClient.putObject(appConf.getMinio().getBucketName(), file.getName(), file.getAbsolutePath());
                result = appConf.getMinio().getUrlRewriteTo() + appConf.getMinio().getBucketName() + "/" + file.getName();
            } catch (Exception e) {
                log.error("cannot upload to Minio: {}", path);
            }
        }
        log.info("______finished uploadFile");
        return result;
    }

    public String uploadGzip(byte[] bytes, String path, boolean privateAccess) {
        log.info("start uploadGzip");
        if (this.s3Client != null) {
            log.info("upload to s3");
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(bytes.length);
            objectMetadata.setContentEncoding("gzip");
            objectMetadata.setContentType("application/json");
            this.s3Client.putObject(new PutObjectRequest(this.s3BucketName, path, new ByteArrayInputStream(bytes), objectMetadata)
                    .withCannedAcl(privateAccess ? CannedAccessControlList.Private : CannedAccessControlList.PublicRead));
            return s3Client.getUrl(this.s3BucketName, path).toString();
        }
        log.info("finished uploadGzip");
        return null;
    }
}
