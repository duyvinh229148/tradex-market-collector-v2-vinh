package com.techx.tradex.marketcollector.services;

import com.ea.async.Async;
import com.techx.tradex.common.constants.MarketTypeEnum;
import com.techx.tradex.common.utils.BeanUtils;
import com.techx.tradex.common.utils.DefaultUtils;
import com.techx.tradex.common.utils.FutureUtils;
import com.techx.tradex.htsconnection.socket.message.receive.*;
import com.techx.tradex.htsconnection.socket.message.send.*;
import com.techx.tradex.htsconnection.socket.nonblocking.HtsConnectionHandler;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.constants.*;
import com.techx.tradex.marketcollector.model.db.ForeignerDaily;
import com.techx.tradex.marketcollector.model.db.Symbol;
import com.techx.tradex.marketcollector.model.db.SymbolDaily;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import com.techx.tradex.marketcollector.model.response.EstimatedCeilFloorResponse;
import com.techx.tradex.marketcollector.repositories.MarketRepository;
import com.techx.tradex.marketcollector.repositories.SymbolInfoRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class DownloadInfoService {
    private static final Logger log = LoggerFactory.getLogger(DownloadInfoService.class);

    private AppConf appConf;
    private CacheService cacheService;
    private MarketRepository marketRepository;
    private SymbolInfoRepository symbolInfoRepository;

    @Autowired
    public DownloadInfoService(
            AppConf appConf,
            CacheService cacheService,
            MarketRepository marketRepository,
            SymbolInfoRepository symbolInfoRepository) {
        this.appConf = appConf;
        this.cacheService = cacheService;
        this.marketRepository = marketRepository;
        this.symbolInfoRepository = symbolInfoRepository;
    }

    public CompletableFuture<Void> downloadSymbolInfo(List<Symbol> indexList, List<Symbol> stockList, List<Symbol> cwList,
                                                      List<Symbol> futuresList, HtsConnectionPool.Connection connection) {
        HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();

        if (!indexList.isEmpty()) {
            Async.await(downloadIndexInfo(htsConnectionHandler, indexList));
        }
        if (!stockList.isEmpty()) {
            Async.await(downloadStockInfo(htsConnectionHandler, stockList));
        }
        if (!cwList.isEmpty()) {
            Async.await(downloadCWInfo(htsConnectionHandler, cwList));
        }
        if (!futuresList.isEmpty()) {
            Set<String> setIndex = indexList.stream().map(Symbol::getCode).collect(Collectors.toSet());
            Async.await(downloadFuturesInfo(htsConnectionHandler, futuresList, setIndex));
        }
        return CompletableFuture.completedFuture(null);
    }


    public CompletableFuture<Void> downloadSymbolDaily(List<Symbol> indexList, List<Symbol> stockList, List<Symbol> cwList,
                                                       List<Symbol> futuresList, Date date, HtsConnectionPool.Connection connection) {
        HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
        if (!indexList.isEmpty()) {
            Async.await(downloadDailyIndex(htsConnectionHandler, indexList, date));
        }
        if (!stockList.isEmpty()) {
            Async.await(downloadDailyStock(htsConnectionHandler, stockList, date));
            Async.await(downloadDailyStockForeigner(htsConnectionHandler, stockList, date));
        }
        if (!cwList.isEmpty()) {
            Async.await(downloadDailyCW(htsConnectionHandler, cwList, date));
        }
        if (!futuresList.isEmpty()) {
            Async.await(downloadDailyFutures(htsConnectionHandler, futuresList, date));
        }
        return CompletableFuture.completedFuture(null);
    }


    private CompletableFuture<Void> downloadStockInfo(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolInfo> results = Async.await(FutureUtils.allOf(symbolList.stream().map(symbol ->
                downloadStockInfo(symbol, htsConnectionHandler))));
        List<SymbolInfo> stockInfoList = results.getSuccess();
        Calendar current = Calendar.getInstance();
        if (current.before(cacheService.getTimeStartReceiveBidAsk()) || current.after(cacheService.getTimeStopReceiveBidAsk())) {
            log.info("ignore bidOfferList because of before TimeStartReceiveBidAsk");
            stockInfoList.forEach(symbolInfo -> symbolInfo.setBidOfferList(null));
        }
        Async.await(downloadEstimatedCeilingFloor(htsConnectionHandler, stockInfoList));
        symbolInfoRepository.removeByType(SymbolTypeEnum.STOCK.name());
        symbolInfoRepository.saveAll(stockInfoList);
        long t2 = System.currentTimeMillis();
        log.info("downloadStockInfo: {}/{} records _ takes: {}", stockInfoList.size(), symbolList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<SymbolInfo> downloadStockInfo(Symbol symbol, HtsConnectionHandler htsConnectionHandler) {
        MarketStockCurrentPriceSnd snd = new MarketStockCurrentPriceSnd();
        snd.getStockCode().setValue(symbol.getValidCode());
        MarketStockCurrentPriceRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketStockCurrentPriceRcv.class));
        SymbolInfo stockInfo = MappingHTS.QUERY_STOCK_INFO.call(rcv);

        stockInfo.setName(symbol.getName());
        stockInfo.setNameEn(symbol.getNameEn());
        stockInfo.setSecuritiesType(symbol.getSecuritiesType());
        stockInfo.setCreatedAt(new Date());
        stockInfo.setType(SymbolTypeEnum.STOCK);
        if (stockInfo.getMarketType().equals(MarketTypeEnum.HNX.name())) {
            stockInfo.setRoundLot(FixRoundLotEnum.HNX.getRoundLot());
            stockInfo.setSecurityExchange(FixSecurityExchangeEnum.HNX.getSecurityExchange());
        } else if (stockInfo.getMarketType().equals(MarketTypeEnum.UPCOM.name())) {
            stockInfo.setRoundLot(FixRoundLotEnum.UPCOM.getRoundLot());
            stockInfo.setSecurityExchange(FixSecurityExchangeEnum.UPCOM.getSecurityExchange());
        } else {
            stockInfo.setRoundLot(FixRoundLotEnum.HOSE.getRoundLot());
            stockInfo.setSecurityExchange(FixSecurityExchangeEnum.HOSE.getSecurityExchange());
        }
        if (stockInfo.getSecuritiesType().equals(FixCfiCodeEnum.STOCK.name())) {
            stockInfo.setCfiCode(FixCfiCodeEnum.STOCK.getCfiCode());
        } else if (stockInfo.getSecuritiesType().equals(FixCfiCodeEnum.ETF.name())) {
            stockInfo.setCfiCode(FixCfiCodeEnum.ETF.getCfiCode());
        } else if (stockInfo.getSecuritiesType().equals(FixCfiCodeEnum.FUND.name())) {
            stockInfo.setCfiCode(FixCfiCodeEnum.FUND.getCfiCode());
        }
        stockInfo.setFixSecurityType(FixSecurityTypeEnum.CS.name());
        stockInfo.setMinTradeVolume(Long.valueOf(stockInfo.getRoundLot()));
        stockInfo.setCurrency(Constants.FIX_DEFAULT_CURRENCY);
        stockInfo.setContractMultiplier(FixContractMultiplierEnum.STOCK.getContractMultiplier());
        if (appConf.getSecurityInfo().isEnable()) {
            SymbolInfo.SymbolInfoOverride overridingValue = appConf.getSecurityInfo().getOverrideValues().getSymbolMap().get(symbol.getCode());
            if (overridingValue != null) {
                BeanUtils.copyProperties(overridingValue, stockInfo, true);
            }
        }
        return CompletableFuture.completedFuture(stockInfo);
    }

    private CompletableFuture<Void> downloadIndexInfo(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolInfo> results = Async.await(FutureUtils.allOf(symbolList.stream().map(symbol -> downloadIndexInfo(symbol, htsConnectionHandler))));
        List<SymbolInfo> indexInfoList = results.getSuccess();

        symbolInfoRepository.removeByType(SymbolTypeEnum.INDEX.name());
        symbolInfoRepository.saveAll(indexInfoList);
        long t2 = System.currentTimeMillis();
        log.info("downloadIndexInfo: {}/{} records _ takes: {}", indexInfoList.size(), symbolList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<SymbolInfo> downloadIndexInfo(Symbol symbol, HtsConnectionHandler htsConnectionHandler) {
        MarketIndustryCurrentIndexSnd snd = new MarketIndustryCurrentIndexSnd();
        snd.getIndexCode().setValue(symbol.getValidCode());

        MarketIndustryCurrentIndexRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketIndustryCurrentIndexRcv.class));
        SymbolInfo indexInfo = MappingHTS.QUERY_INDEX_INFO.call(rcv);
        indexInfo.setName(symbol.getName());
        indexInfo.setNameEn(symbol.getNameEn());
        indexInfo.setMarketType(symbol.getMarketType());
        indexInfo.setCode(symbol.getCode());
        indexInfo.setRefCode(symbol.getValidCode());
        indexInfo.setCreatedAt(new Date());
        indexInfo.setType(SymbolTypeEnum.INDEX);
        if (indexInfo.getCode().equals("VN30")) {
            cacheService.setLastVn30(indexInfo.getLast());
        }
        indexInfo.setIsHighlight(appConf.getHighlightMap().getOrDefault(symbol.getCode(), Constants.DEFAULT_HIGHLIGHT_NUMBER));
        if (appConf.getSecurityInfo().isEnable()) {
            SymbolInfo.SymbolInfoOverride overridingValue = appConf.getSecurityInfo().getOverrideValues().getSymbolMap().get(symbol.getCode());
            if (overridingValue != null) {
                BeanUtils.copyProperties(overridingValue, indexInfo, true);
            }
        }
        return CompletableFuture.completedFuture(indexInfo);
    }

    private CompletableFuture<Void> downloadFuturesInfo(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList,
                                                        Set<String> setIndex) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolInfo> results = Async.await(FutureUtils.allOf(symbolList.stream().map(symbol -> downloadFuturesInfo(symbol, htsConnectionHandler, setIndex))));
        List<SymbolInfo> futuresInfoList = results.getSuccess();
        Calendar current = Calendar.getInstance();
        if (current.before(cacheService.getTimeStartReceiveBidAsk()) || current.after(cacheService.getTimeStopReceiveBidAsk())) {
            log.info("ignore bidOfferList because of before TimeStartReceiveBidAsk");
            futuresInfoList.forEach(symbolInfo -> symbolInfo.setBidOfferList(null));
        }
        List<SymbolInfo> vn30Futures = new ArrayList<>();
        List<SymbolInfo> gb05Futures = new ArrayList<>();
        futuresInfoList.forEach(futuresInfo -> {
            if (futuresInfo.getCode().startsWith("VN30")) {
                vn30Futures.add(futuresInfo);
            } else if (futuresInfo.getCode().startsWith("GB05")) {
                gb05Futures.add(futuresInfo);
            }
        });
        updateVN30Futures(vn30Futures);
        updateGB05Futures(gb05Futures);
        symbolInfoRepository.removeByType(SymbolTypeEnum.FUTURES.name());
        symbolInfoRepository.saveAll(futuresInfoList);

        long t2 = System.currentTimeMillis();
        log.info("downloadFuturesInfo: {}/{} records _ takes: {}", futuresInfoList.size(), symbolList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<SymbolInfo> downloadFuturesInfo(Symbol symbol, HtsConnectionHandler htsConnectionHandler, Set<String> setIndex) {
        MarketFuturesCurrentPriceSnd snd = new MarketFuturesCurrentPriceSnd();
        snd.getStockCode().setValue(symbol.getValidCode());
        MarketFuturesCurrentPriceRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketFuturesCurrentPriceRcv.class));
        SymbolInfo futuresInfo = MappingHTS.QUERY_FUTURES_INFO.call(rcv);

        futuresInfo.setCode(symbol.getValidCode());
        futuresInfo.setName(symbol.getName());
        futuresInfo.setNameEn(symbol.getNameEn());
        futuresInfo.setExchange(symbol.getExchange());
        futuresInfo.setCreatedAt(new Date());
        futuresInfo.setType(SymbolTypeEnum.FUTURES);
        if (futuresInfo.getExchange().equals("15")) {
            futuresInfo.setMarketType(MarketTypeEnum.HNX.name());
        }

        if (futuresInfo.getMarketType().equals(MarketTypeEnum.HNX.name())) {
            futuresInfo.setSecurityExchange(FixSecurityExchangeEnum.HNX.getSecurityExchange());
        } else if (futuresInfo.getMarketType().equals(MarketTypeEnum.UPCOM.name())) {
            futuresInfo.setSecurityExchange(FixSecurityExchangeEnum.UPCOM.getSecurityExchange());
        } else {
            futuresInfo.setSecurityExchange(FixSecurityExchangeEnum.HOSE.getSecurityExchange());
        }
        futuresInfo.setRoundLot(FixRoundLotEnum.FUTURES.getRoundLot());
        futuresInfo.setUnderlyingSymbol(StringUtils.isBlank(futuresInfo.getBaseCode()) ? "VN30" : futuresInfo.getBaseCode());
        futuresInfo.setMinTradeVolume(Long.valueOf(futuresInfo.getRoundLot()));
        futuresInfo.setCurrency(Constants.FIX_DEFAULT_CURRENCY);
        futuresInfo.setFixSecurityType(FixSecurityTypeEnum.FUT.name());
        if (setIndex.contains(futuresInfo.getBaseCode())) {
            futuresInfo.setContractMultiplier(FixContractMultiplierEnum.VN30_FUTURES.getContractMultiplier());
            futuresInfo.setBaseCodeSecuritiesType(SecurityTypeEnum.INDEX.name());
            futuresInfo.setCfiCode(FixCfiCodeEnum.INDEX_FUTURES.getCfiCode());
        } else {
            futuresInfo.setBaseCodeSecuritiesType(SecurityTypeEnum.BOND.name());
            futuresInfo.setContractMultiplier(FixContractMultiplierEnum.BOND_FUTURES.getContractMultiplier());
            futuresInfo.setCfiCode(FixCfiCodeEnum.BOND_FUTURES.getCfiCode());
        }
        if (cacheService.getLastVn30() > 0 && futuresInfo.getCode().startsWith("VN30")) {
            futuresInfo.setBasis(futuresInfo.getLast() - cacheService.getLastVn30());
        }
        if (appConf.getSecurityInfo().isEnable()) {
            SymbolInfo.SymbolInfoOverride overridingValue = appConf.getSecurityInfo().getOverrideValues().getSymbolMap().get(symbol.getCode());
            if (overridingValue != null) {
                BeanUtils.copyProperties(overridingValue, futuresInfo, true);
            }
        }
        return CompletableFuture.completedFuture(futuresInfo);
    }

    private CompletableFuture<Void> downloadCWInfo(HtsConnectionHandler htsConnectionHandler, List<Symbol> cwList) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolInfo> results = Async.await(FutureUtils.allOf(cwList.stream().map(symbol -> downloadCWInfo(symbol, htsConnectionHandler))));
        List<SymbolInfo> cwInfoList = results.getSuccess();
        Calendar current = Calendar.getInstance();
        if (current.before(cacheService.getTimeStartReceiveBidAsk()) || current.after(cacheService.getTimeStopReceiveBidAsk())) {
            log.info("ignore bidOfferList because of before TimeStartReceiveBidAsk");
            cwInfoList.forEach(symbolInfo -> symbolInfo.setBidOfferList(null));
        }
        Async.await(downloadEstimatedCeilingFloor(htsConnectionHandler, cwInfoList));
        symbolInfoRepository.removeByType(SymbolTypeEnum.CW.name());
        symbolInfoRepository.saveAll(cwInfoList);
        long t2 = System.currentTimeMillis();
        log.info("downloadCWInfo: {}/{} records _ takes: {}", cwInfoList.size(), cwList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<SymbolInfo> downloadCWInfo(Symbol symbol, HtsConnectionHandler htsConnectionHandler) {
        MarketCWCurrentPriceSnd snd = new MarketCWCurrentPriceSnd();
        snd.getCwCode().setValue(symbol.getValidCode());
        MarketCWCurrentPriceRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketCWCurrentPriceRcv.class));
        SymbolInfo cwInfo = MappingHTS.QUERY_CW_INFO.call(rcv);
        cwInfo.setName(symbol.getName());
        cwInfo.setNameEn(symbol.getNameEn());
        cwInfo.setCode(symbol.getValidCode());
        cwInfo.setCreatedAt(new Date());
        cwInfo.setType(SymbolTypeEnum.CW);
        if (cwInfo.getMarketType().equals(MarketTypeEnum.HNX.name())) {
            cwInfo.setRoundLot(FixRoundLotEnum.HNX.getRoundLot());
            cwInfo.setSecurityExchange(FixSecurityExchangeEnum.HNX.getSecurityExchange());
        } else if (cwInfo.getMarketType().equals(MarketTypeEnum.UPCOM.name())) {
            cwInfo.setRoundLot(FixRoundLotEnum.UPCOM.getRoundLot());
            cwInfo.setSecurityExchange(FixSecurityExchangeEnum.UPCOM.getSecurityExchange());
        } else {
            cwInfo.setRoundLot(FixRoundLotEnum.HOSE.getRoundLot());
            cwInfo.setSecurityExchange(FixSecurityExchangeEnum.HOSE.getSecurityExchange());
        }
        if (appConf.getDomainBos().equals("vcsc")) {
            cwInfo.setExercisePrice(cwInfo.getExercisePrice() / 10);
        }
        cwInfo.setFixSecurityType(FixSecurityTypeEnum.WAR.name());
        cwInfo.setCfiCode(FixCfiCodeEnum.CW.getCfiCode());
        cwInfo.setMinTradeVolume(Long.valueOf(cwInfo.getRoundLot()));
        cwInfo.setCurrency(Constants.FIX_DEFAULT_CURRENCY);
        cwInfo.setContractMultiplier(FixContractMultiplierEnum.CW.getContractMultiplier());
        if (StringUtils.isBlank(cwInfo.getUnderlyingSymbol())) {
            cwInfo.setUnderlyingSymbol(cwInfo.getCode().substring(1, 4));
        }
        if (appConf.getSecurityInfo().isEnable()) {
            SymbolInfo.SymbolInfoOverride overridingValue = appConf.getSecurityInfo().getOverrideValues().getSymbolMap().get(symbol.getCode());
            if (overridingValue != null) {
                BeanUtils.copyProperties(overridingValue, cwInfo, true);
            }
        }
        return CompletableFuture.completedFuture(cwInfo);
    }

    private CompletableFuture<Void> downloadEstimatedCeilingFloor(HtsConnectionHandler htsConnectionHandler, List<SymbolInfo> symbolInfoList) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<Void> results = Async.await(FutureUtils.allOf(symbolInfoList.stream().map(stockInfo -> this.downloadStockEstimatedCeilingFloor(htsConnectionHandler, stockInfo))));
        long t2 = System.currentTimeMillis();
        log.info("downloadEstimatedCeilingFloor: {}/{} records _ takes: {}", results.getSuccess().size(), symbolInfoList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> downloadStockEstimatedCeilingFloor(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo) {
        EstimatedCeilFloorSnd snd = new EstimatedCeilFloorSnd();
        snd.getStockCode().setValue(symbolInfo.getCode());
        EstimatedCeilFloorRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, EstimatedCeilFloorRcv.class));
        EstimatedCeilFloorResponse estimatedCeilFloorResponse = MappingHTS.QUERY_ESTIMATED_CEIL_FLOOR.call(rcv);
        symbolInfo.setEstimatedData(estimatedCeilFloorResponse.toEstimatedData());
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> downloadDailyCW(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList, Date date) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolDaily> results = Async.await(FutureUtils.allOf(
                symbolList.stream().map(symbol -> this.downloadDailyCW(htsConnectionHandler, symbol, date))));
        List<SymbolDaily> dailyCWList = results.getSuccess();
        marketRepository.updateInBulk(dailyCWList, SymbolDaily.class);

        long t2 = System.currentTimeMillis();
        log.info("downloadDailyCW : {}/{} records _ takes {} ms", dailyCWList.size(), symbolList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<SymbolDaily> downloadDailyCW(HtsConnectionHandler htsConnectionHandler, Symbol symbol, Date date) {
        MarketCWDailySnd snd = new MarketCWDailySnd();
        snd.getCwCode().setValue(symbol.getValidCode());
        snd.getBaseDate().setValue(getSdf().format(date));
        snd.getIReadCount().setValue(1);
        MarketCWDailyRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketCWDailyRcv.class));
        SymbolDaily dailyCW = MappingHTS.QUERY_DAILY_CW.call(rcv);
        dailyCW.setId(symbol.getCode() + "_" + DefaultUtils.DATE_FORMAT().format(date));
        dailyCW.setCode(symbol.getCode());
        dailyCW.setDate(date);
        dailyCW.setMarketType(symbol.getMarketType());
        dailyCW.setCreatedAt(new Date());
        dailyCW.setUpdatedAt(new Date());
        return CompletableFuture.completedFuture(dailyCW);
    }

    private CompletableFuture<Void> downloadDailyIndex(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList, Date date) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolDaily> results = Async.await(FutureUtils.allOf(
                symbolList.stream().map(symbol -> this.downloadDailyIndex(symbol, htsConnectionHandler, date))));
        List<SymbolDaily> dailyIndexList = results.getSuccess();
        marketRepository.updateInBulk(dailyIndexList, SymbolDaily.class);

        long t2 = System.currentTimeMillis();
        log.info("downloadDailyIndex : {}/{} records _ takes {} ms", dailyIndexList.size(), symbolList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<SymbolDaily> downloadDailyIndex(Symbol symbol, HtsConnectionHandler htsConnectionHandler, Date date) {
        MarketIndexDailySnd snd = new MarketIndexDailySnd();
        snd.getIndexCode().setValue(symbol.getValidCode());
        snd.getBaseDate().setValue(getSdf().format(date));
        snd.getIReadCount().setValue(1);
        MarketIndexDailyRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketIndexDailyRcv.class));
        SymbolDaily dailyIndex = MappingHTS.QUERY_DAILY_INDEX.call(rcv);
        dailyIndex.setId(symbol.getCode() + "_" + getSdf().format(dailyIndex.getDate()));
        dailyIndex.setCode(symbol.getCode());
        dailyIndex.setRefCode(symbol.getValidCode());
        dailyIndex.setDate(date);
        dailyIndex.setMarketType(symbol.getMarketType());
        dailyIndex.setCreatedAt(new Date());
        dailyIndex.setUpdatedAt(new Date());
        return CompletableFuture.completedFuture(dailyIndex);
    }

    private CompletableFuture<Void> downloadDailyStock(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList,
                                                       Date date) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolDaily> results = Async.await(FutureUtils.allOf(
                symbolList.stream().map(symbol -> this.downloadDailyStock(htsConnectionHandler, symbol, date))));
        List<SymbolDaily> dailyStockList = results.getSuccess();
        marketRepository.updateInBulk(dailyStockList, SymbolDaily.class);
        long t2 = System.currentTimeMillis();
        log.info("downloadDailyStock : {}/{} records _ takes {} ms", dailyStockList.size(), symbolList.size(), (t2 - t1));

        return CompletableFuture.completedFuture(null);

    }

    private CompletableFuture<SymbolDaily> downloadDailyStock(HtsConnectionHandler htsConnectionHandler, Symbol symbol, Date date) {
        MarketStockDailySnd snd = new MarketStockDailySnd();
        snd.getStockCode().setValue(symbol.getValidCode());
        snd.getBaseDate().setValue(getSdf().format(date));
        snd.getPriceType().setValue(1);
        snd.getIReadCount().setValue(1);
        MarketStockDailyRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketStockDailyRcv.class));
        SymbolDaily dailyStock = MappingHTS.QUERY_STOCK_DAILY.call(rcv);
        dailyStock.setId(symbol.getValidCode() + "_" + getSdf().format(dailyStock.getDate()));
        dailyStock.setCode(symbol.getValidCode());
        dailyStock.setDate(date);
        dailyStock.setMarketType(symbol.getMarketType());
        dailyStock.setCreatedAt(new Date());
        dailyStock.setUpdatedAt(new Date());
        dailyStock.setSecuritiesType(symbol.getSecuritiesType());
        if (symbol.getMarketType().equalsIgnoreCase(MarketTypeEnum.HOSE.name())
                || symbol.getMarketType().equalsIgnoreCase(MarketTypeEnum.HNX.name())) {
            double tempPrice = dailyStock.getPrice();
            dailyStock.setPrice(dailyStock.getLast());
            dailyStock.setLast(tempPrice);
        }
        return CompletableFuture.completedFuture(dailyStock);
    }

    private CompletableFuture<Void> downloadDailyStockForeigner(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList, Date date) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<ForeignerDaily> results = Async.await(FutureUtils.allOf(
                symbolList.stream().map(symbol -> this.downloadDailyStockForeigner(htsConnectionHandler, symbol, date))));
        List<ForeignerDaily> foreignerDailyList = results.getSuccess();
        marketRepository.updateInBulk(foreignerDailyList, ForeignerDaily.class);
        long t2 = System.currentTimeMillis();
        log.info("downloadDailyStockForeigner : {}/{} records _ takes {} ms", foreignerDailyList.size(), symbolList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<ForeignerDaily> downloadDailyStockForeigner(HtsConnectionHandler htsConnectionHandler, Symbol symbol, Date date) {
        MarketStockForeignerSnd snd = new MarketStockForeignerSnd();
        snd.getStockCode().setValue(symbol.getValidCode());
        snd.getBaseDate().setValue(Integer.parseInt(getSdf().format(date)));
        snd.getIReadCount().setValue(1);
        MarketStockForeignerRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketStockForeignerRcv.class));
        ForeignerDaily dailyStock = MappingHTS.QUERY_STOCK_FOREIGNER_DAILY.call(rcv);
        dailyStock.setId(symbol.getValidCode() + "_" + getSdf().format(dailyStock.getDate()));
        dailyStock.setCode(symbol.getValidCode());
        dailyStock.setDate(date);
        dailyStock.setCreatedAt(new Date());
        dailyStock.setUpdatedAt(new Date());
        return CompletableFuture.completedFuture(dailyStock);
    }


    private CompletableFuture<Void> downloadDailyFutures(HtsConnectionHandler htsConnectionHandler, List<Symbol> symbolList, Date date) {
        long t1 = System.currentTimeMillis();
        FutureUtils.Results<SymbolDaily> results = Async.await(FutureUtils.allOf(
                symbolList.stream().map(symbol -> this.downloadDailyFutures(htsConnectionHandler, symbol, date))));
        List<SymbolDaily> dailyFuturesList = results.getSuccess();
        marketRepository.updateInBulk(dailyFuturesList, SymbolDaily.class);

        long t2 = System.currentTimeMillis();
        log.info("downloadDailyFutures : {}/{} records _ takes {} ms", dailyFuturesList.size(), symbolList.size(), (t2 - t1));
        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<SymbolDaily> downloadDailyFutures(HtsConnectionHandler htsConnectionHandler, Symbol symbol, Date date) {
        MarketFuturesDailySnd snd = new MarketFuturesDailySnd();
        snd.getStkCode().setValue(symbol.getValidCode());
        snd.getBaseDate().setValue(getSdf().format(date));
        snd.getIReadCount().setValue(1);
        MarketFuturesDailyRcv rcv = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketFuturesDailyRcv.class));
        SymbolDaily dailyFutures = MappingHTS.QUERY_DAILY_FUTURES.call(rcv);
        dailyFutures.setId(symbol.getValidCode() + "_" + DefaultUtils.DATE_FORMAT().format(dailyFutures.getDate()));
        dailyFutures.setCode(symbol.getValidCode());
        dailyFutures.setDate(date);
        dailyFutures.setMarketType(symbol.getMarketType());
        dailyFutures.setCreatedAt(new Date());
        dailyFutures.setUpdatedAt(new Date());
        dailyFutures.setRefCode(this.cacheService.getFuturesCodeRefMap().get(dailyFutures.getCode()));
        return CompletableFuture.completedFuture(dailyFutures);
    }


    private void updateGB05Futures(List<SymbolInfo> gb05Futures) {
        if (gb05Futures.size() == 3) {
            gb05Futures.sort((f1, f2) -> f1.getMaturityDate().compareTo(f2.getMaturityDate()));
            gb05Futures.get(0).setRefCode("GB051Q");
            gb05Futures.get(1).setRefCode("GB052Q");
            gb05Futures.get(2).setRefCode("GB053Q");
            gb05Futures.forEach(futuresInfo ->
                    this.cacheService.getFuturesCodeRefMap().put(futuresInfo.getCode(), futuresInfo.getRefCode())
            );
        } else {
            log.error("Update refCode for VN30F, missing or redundant futuresInfo");
        }
    }

    private void updateVN30Futures(List<SymbolInfo> vn30Futures) {
        if (vn30Futures.size() == 4) {
            vn30Futures.sort((f1, f2) -> f1.getMaturityDate().compareTo(f2.getMaturityDate()));
            vn30Futures.get(0).setRefCode("VN30F1M");
            vn30Futures.get(1).setRefCode("VN30F2M");
            vn30Futures.get(2).setRefCode("VN30F1Q");
            vn30Futures.get(3).setRefCode("VN30F2Q");
            vn30Futures.forEach(futuresInfo ->
                    this.cacheService.getFuturesCodeRefMap().put(futuresInfo.getCode(), futuresInfo.getRefCode())
            );
        } else {
            log.error("Update refCode for GB05, missing or redundant futuresInfo");
        }
    }

    private SimpleDateFormat getSdf() {
        return new SimpleDateFormat("yyyyMMdd");
    }

}
