package com.techx.tradex.marketcollector.services;

import com.fasterxml.jackson.databind.node.NullNode;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.techx.tradex.common.utils.StringUtils;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.utils.SocketMinBinCodec;
import io.github.sac.Ack;
import io.github.sac.BasicListener;
import io.github.sac.ReconnectStrategy;
import io.github.sac.Socket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

@Service
@ConditionalOnProperty(name = "serviceTest", matchIfMissing = true)
public class SocketClusterService {
    private static final Logger log = LoggerFactory.getLogger(SocketClusterService.class);

    private AppConf appConf;
    private Socket socket;
    private Ack publishAck = (String name, Object error, Object data) -> {
        if (error != null && !(error instanceof NullNode)) {
            if (error instanceof Throwable) {
                log.error("error on publishing to {} _ {}", name, error);
            } else {
                log.error("error on publishing to {} with error {} and type {}", name, error, error.getClass());
            }
        }
    };

    @Autowired
    public SocketClusterService(AppConf appConf) {
        this.appConf = appConf;
    }

    @PostConstruct
    public void init() {
        if (this.appConf.getSocketCluster().isEnable()) {
            this.socket = new Socket("ws://" + appConf.getSocketCluster().getHostname() + ":" +
                    appConf.getSocketCluster().getPort() + "/" + appConf.getSocketCluster().getPath());
            if (appConf.getSocketCluster().isAutoReconnection()) {
                this.socket.setReconnection(new ReconnectStrategy().setDelay(600));
            }
            if (!appConf.getSocketCluster().isLogMessage()) {
                this.socket.disableLogging();
            }
            if (StringUtils.isEmpty(appConf.getSocketCluster().getCodec())
                    || appConf.getSocketCluster().getCodec().equals(AppConf.SocketClusterConf.CODEC_MIN_BIN)) {
                this.socket.setCodec(new SocketMinBinCodec());
            }
            this.socket.setListener(new BasicListener() {
                private long disconnectedTime = 0;
                private final long maxWarningDisconnectedTime = 15000;
                private Timer timer = null;

                @Override
                public void onConnected(Socket socket, Map<String, List<String>> headers) {
                    long time = System.currentTimeMillis();
                    if (disconnectedTime > 0 && time - disconnectedTime >= maxWarningDisconnectedTime) {
                        log.info("SC Connected");
                    }
                    timer.cancel();
                    timer = null;
                    disconnectedTime = 0;
                }

                @Override
                public void onDisconnected(Socket socket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) {
                    if (timer != null) return;
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            log.warn("SC Disconnected for long time {}", disconnectedTime);
                        }
                    }, maxWarningDisconnectedTime);
                    disconnectedTime = System.currentTimeMillis();
                }

                @Override
                public void onConnectError(Socket socket, WebSocketException exception) {
                    log.warn("SC connect error", exception);
                }

                @Override
                public void onAuthentication(Socket socket, Boolean status) {
                    log.warn("SC authentication state changed {}", status);
                }

                @Override
                public void onSetAuthToken(String token, Socket socket) {
                    log.warn("SC authentication data changed {}", token);
                }
            });
            this.socket.connect();
        }
    }

    public void publish(String channel, Object data) {
        if (this.appConf.getSocketCluster().isEnable()) {
            if (StringUtils.isEmpty(channel)) {
                log.error("channel is empty");
            }
            this.socket.publish(channel, data, publishAck);
        }
    }
}
