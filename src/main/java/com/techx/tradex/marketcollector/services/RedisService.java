package com.techx.tradex.marketcollector.services;

import com.techx.tradex.marketcollector.daos.RedisDao;
import com.techx.tradex.marketcollector.model.db.*;
import com.techx.tradex.marketcollector.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RedisService {

    private static final Logger log = LoggerFactory.getLogger(RequestSender.class);

    private RedisDao redisDao;
    private SymbolInfoRepository symbolInfoRepository;
    private SymbolDailyRepository symbolDailyRepository;
    private ForeignerDailyRepository foreignerDailyRepository;
    private CacheService cacheService;
    private SymbolQuoteRepository symbolQuoteRepository;
    private MarketRepository marketRepository;

    @Autowired
    public RedisService(RedisDao redisDao, SymbolInfoRepository symbolInfoRepository,
                        SymbolDailyRepository symbolDailyRepository, ForeignerDailyRepository foreignerDailyRepository,
                        CacheService cacheService, MarketRepository marketRepository,
                        SymbolQuoteRepository symbolQuoteRepository) {
        this.redisDao = redisDao;
        this.symbolInfoRepository = symbolInfoRepository;
        this.symbolDailyRepository = symbolDailyRepository;
        this.foreignerDailyRepository = foreignerDailyRepository;
        this.cacheService = cacheService;
        this.marketRepository = marketRepository;
        this.symbolQuoteRepository = symbolQuoteRepository;
    }


    public void reloadSymbolInfo() {
        log.info("start reset reloadSymbolInfo");
        long t1 = System.currentTimeMillis();
        List<SymbolInfo> symbolInfoList = this.symbolInfoRepository.findAll();
        log.info("load symbolInfoList from database: {}", symbolInfoList.size());
        log.info("finish clearSymbolInfo: {}", redisDao.clearSymbolInfo());

        for (int i = 0; i < symbolInfoList.size(); i++) {
            SymbolInfo symbolInfo = symbolInfoList.get(i);
            symbolInfo.setSequence(cacheService.getMapSequence().getOrDefault(symbolInfo.getCode(), 0));
            this.redisDao.setSymbolInfo(symbolInfo);
        }
        log.info("finish update symbolInfo");
        long t2 = System.currentTimeMillis();
        log.info("finish reloadSymbolInfo take: {}", (t2 - t1));
        cacheService.reset();
    }

    public void reloadSymbolDaily() {
        log.info("start reset reloadSymbolDaily");
        long t1 = System.currentTimeMillis();
        Date today = new Date();
        List<SymbolDaily> symbolDailyList = this.symbolDailyRepository.findByDate(today);
        log.info("load symbolDailyList from database: {}", symbolDailyList.size());
        List<ForeignerDaily> foreignerDailyList = this.foreignerDailyRepository.findByDate(today);
        log.info("load foreignerDailyList from database: {}", foreignerDailyList.size());

        log.info("finish clearSymbolDaily: {}", redisDao.clearSymbolDaily());
        log.info("finish clearForeignerDaily: {}", redisDao.clearForeignerDaily());

        for (int i = 0; i < symbolDailyList.size(); i++) {
            SymbolDaily symbolDaily = symbolDailyList.get(i);
            this.redisDao.setSymbolDaily(symbolDaily);
        }
        log.info("finish update symbolDaily");
        for (int i = 0; i < foreignerDailyList.size(); i++) {
            ForeignerDaily foreignerDaily = foreignerDailyList.get(i);
            this.redisDao.setForeignerDaily(foreignerDaily);
        }
        long t2 = System.currentTimeMillis();
        log.info("finish reloadSymbolDaily take: {}", (t2 - t1));
        cacheService.reset();
    }

    public void reloadSymbolQuote(String code, List<SymbolQuote> symbolQuoteList) {
        log.info("start reloadSymbolQuote {}", code);
        long t1 = System.currentTimeMillis();
        redisDao.clearSymbolQuote(code);
        for (SymbolQuote symbolQuote : symbolQuoteList) {
            redisDao.addSymbolQuote(symbolQuote);
        }
        long t2 = System.currentTimeMillis();
        log.info("finish reloadSymbolQuote {} take: {}", code, (t2 - t1));
    }

    public void reloadSymbolQuoteMinute(String code, List<SymbolQuoteMinute> symbolQuoteMinuteList) {
        log.info("start reloadSymbolQuoteMinute {}", code);
        long t1 = System.currentTimeMillis();
        redisDao.clearSymbolQuoteMinute(code);
        for (SymbolQuoteMinute symbolQuoteMinute : symbolQuoteMinuteList) {
            redisDao.addSymbolQuoteMinute(symbolQuoteMinute);
        }
        long t2 = System.currentTimeMillis();
        log.info("finish reloadSymbolQuoteMinute {} take: {}", code, (t2 - t1));
    }

}
