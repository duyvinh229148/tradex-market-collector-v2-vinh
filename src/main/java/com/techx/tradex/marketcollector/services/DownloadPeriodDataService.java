package com.techx.tradex.marketcollector.services;

import com.ea.async.Async;
import com.techx.tradex.common.constants.MarketTypeEnum;
import com.techx.tradex.htsconnection.socket.message.receive.*;
import com.techx.tradex.htsconnection.socket.message.send.*;
import com.techx.tradex.htsconnection.socket.nonblocking.HtsConnectionHandler;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.constants.MappingHTS;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.ForeignerDaily;
import com.techx.tradex.marketcollector.model.db.SymbolDaily;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import com.techx.tradex.marketcollector.model.request.DownloadRequest;
import com.techx.tradex.marketcollector.repositories.MarketRepository;
import com.techx.tradex.marketcollector.repositories.SymbolInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class DownloadPeriodDataService {
    private static final Logger log = LoggerFactory.getLogger(DownloadPeriodDataService.class);

    private AppConf appConf;
    private MongoTemplate mongoTemplate;
    private HtsConnectionPool htsConnectionPool;
    private CacheService cacheService;
    private MarketRepository marketRepository;
    private SymbolInfoRepository symbolInfoRepository;

    @Autowired
    public DownloadPeriodDataService(
            AppConf appConf,
            MongoTemplate mongoTemplate,
            HtsConnectionPool htsConnectionPool,
            CacheService cacheService,
            MarketRepository marketRepository,
            SymbolInfoRepository symbolInfoRepository) {
        this.appConf = appConf;
        this.mongoTemplate = mongoTemplate;
        this.htsConnectionPool = htsConnectionPool;
        this.cacheService = cacheService;
        this.marketRepository = marketRepository;
        this.symbolInfoRepository = symbolInfoRepository;
    }


    public Observable<Boolean> downloadStockDaily(DownloadRequest downloadRequest) {
        log.info("download StockDaily: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.STOCK.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.STOCK.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list {0}", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlStockDaily(htsConnectionHandler, symbolInfoList, downloadRequest.getFromDate(), downloadRequest.getToDate()).thenAccept(aVoid -> {
                    log.info("Finished downloadStockDaily");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlStockDaily(HtsConnectionHandler htsConnectionHandler,
                                                          List<SymbolInfo> symbolInfoList, String fromDate, String toDate) {
        try {
            int symbolInfoListSize = symbolInfoList.size();
            for (int i = 0; i < symbolInfoListSize; i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketStockDailySnd snd = new MarketStockDailySnd();
                snd.getStockCode().setValue(symbolInfo.getCode());
                snd.getIReadCount().setValue(20);
                snd.getBaseDate().setValue(toDate);
                snd.getPriceType().setValue(1);
                List<SymbolDaily> result = new ArrayList<>();
                Async.await(queryStockDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                marketRepository.updateInBulk(result, SymbolDaily.class);
                log.info("{}/{}  Finish Crawl StockDaily code: {} - size: {}", i, symbolInfoListSize, symbolInfo.getCode(), result.size());

            }
            log.info("Finish Crawl StockDaily");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryStockDaily(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo,
                                                    MarketStockDailySnd snd, List<SymbolDaily> result, String fromDate, String toDate) {
        log.info("queryStockDaily: {}", symbolInfo.getCode());
        MarketStockDailyRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketStockDailyRcv.class));
        log.info("{} _ receive data {}", snd.getStockCode().getValue(), data.getItems().size());

        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketStockDailyItem item = data.getItems().get(i);

                SymbolDaily stockDaily = new SymbolDaily();
                stockDaily.setId(symbolInfo.getCode() + "_" + item.getDate().getValue());
                stockDaily.setCode(symbolInfo.getCode());
                stockDaily.setMarketType(symbolInfo.getMarketType());
                stockDaily.setSecuritiesType(symbolInfo.getSecuritiesType());
                MappingHTS.MAP_STOCK_DAILY.accept(item, stockDaily);
                stockDaily.setCreatedAt(new Date());
                stockDaily.setUpdatedAt(new Date());
                if (stockDaily.getMarketType().equalsIgnoreCase(MarketTypeEnum.HOSE.name())
                        || stockDaily.getMarketType().equalsIgnoreCase(MarketTypeEnum.HNX.name())) {
                    stockDaily.setLast((double) item.getPrice().getValue());
                }
                result.add(stockDaily);
            }
            if (data.getStatus() == null) {
                log.info("Finish Crawl Daily {} because status null", symbolInfo.getCode());
            } else {
                MarketStockDailyItem lastItem = data.getItems().get(data.getItems().size() - 1);
                if (lastItem.getDate().getValue().compareTo(fromDate) >= 0) {
                    snd.getBaseDate().setValue(lastItem.getDate().getValue());
                    Async.await(queryStockDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                } else {
                    log.info("Finish Crawl stockDaily {} because < fromDate", symbolInfo.getCode());
                }
            }

        } else {
            log.info("Finish Crawl Daily {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }

    public Observable<Boolean> downloadIndexDaily(DownloadRequest downloadRequest) {
        log.info("download IndexDaily: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.INDEX.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.INDEX.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list {0}", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlIndexDaily(htsConnectionHandler, symbolInfoList, downloadRequest.getFromDate(), downloadRequest.getToDate()).thenAccept(aVoid -> {
                    log.info("Finished downloadIndexDaily");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlIndexDaily(HtsConnectionHandler htsConnectionHandler,
                                                          List<SymbolInfo> symbolInfoList, String fromDate, String toDate) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketIndexDailySnd snd = new MarketIndexDailySnd();
                snd.getIndexCode().setValue(symbolInfo.getRefCode());
                snd.getIReadCount().setValue(100);
                snd.getBaseDate().setValue(toDate);
                List<SymbolDaily> result = new ArrayList<>();
                Async.await(queryIndexDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                marketRepository.updateInBulk(result, SymbolDaily.class);
                log.info("{}  Finish Crawl IndexDaily code: {} - size: {}", i, symbolInfo.getCode(), result.size());

            }
            log.info("Finish Crawl IndexDaily");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryIndexDaily(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo,
                                                    MarketIndexDailySnd snd, List<SymbolDaily> result, String fromDate, String toDate) {
        log.info("snd: {}", snd);
        log.info("queryIndexDaily: {}", symbolInfo.getCode());
        MarketIndexDailyRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketIndexDailyRcv.class));
        log.info("{} _ receive data {}", snd.getIndexCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketIndexDailyItem item = data.getItems().get(i);

                SymbolDaily indexDaily = new SymbolDaily();
                indexDaily.setId(symbolInfo.getCode() + "_" + item.getDate().getValue());
                indexDaily.setCode(symbolInfo.getCode());
                indexDaily.setMarketType(symbolInfo.getMarketType());
                indexDaily.setSecuritiesType(symbolInfo.getSecuritiesType());
                MappingHTS.MAP_DAILY_INDEX.accept(item, indexDaily);
                indexDaily.setCreatedAt(new Date());
                indexDaily.setUpdatedAt(new Date());
                result.add(indexDaily);
            }
            MarketIndexDailyItem lastItem = data.getItems().get(data.getItems().size() - 1);
            if (lastItem.getDate().getValue().compareTo(fromDate) >= 0) {
                snd.getBaseDate().setValue(lastItem.getDate().getValue());
                Async.await(queryIndexDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
            } else {
                log.info("Finish Crawl indexDaily {} because < fromDate", symbolInfo.getCode());
            }

        } else {
            log.info("Finish Crawl Daily {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }


    public Observable<Boolean> downloadFuturesDaily(DownloadRequest downloadRequest) {
        log.info("download FuturesDaily: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.FUTURES.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.FUTURES.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list {0}", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlFuturesDaily(htsConnectionHandler, symbolInfoList, downloadRequest.getFromDate(), downloadRequest.getToDate()).thenAccept(aVoid -> {
                    log.info("Finished downloadFuturesDaily");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlFuturesDaily(HtsConnectionHandler htsConnectionHandler,
                                                            List<SymbolInfo> symbolInfoList, String fromDate, String toDate) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketFuturesDailySnd snd = new MarketFuturesDailySnd();
                snd.getStkCode().setValue(symbolInfo.getCode());
                snd.getIReadCount().setValue(100);
                snd.getBaseDate().setValue(toDate);
                List<SymbolDaily> result = new ArrayList<>();
                Async.await(queryFuturesDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                marketRepository.updateInBulk(result, SymbolDaily.class);
                log.info("{}  Finish Crawl FuturesDaily code: {} - size: {}", i, symbolInfo.getCode(), result.size());

            }
            log.info("Finish Crawl FuturesDaily");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryFuturesDaily(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo,
                                                      MarketFuturesDailySnd snd, List<SymbolDaily> result, String fromDate, String toDate) {
        log.info("snd: {}", snd);
        log.info("queryFuturesDaily: {}", symbolInfo.getCode());
        MarketFuturesDailyRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketFuturesDailyRcv.class));
        log.info("{} _ receive data {}", snd.getStkCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketFuturesDailyItem item = data.getItems().get(i);

                SymbolDaily futuresDaily = new SymbolDaily();
                futuresDaily.setId(symbolInfo.getCode() + "_" + item.getDate().getValue());
                futuresDaily.setCode(symbolInfo.getCode());
                futuresDaily.setMarketType(symbolInfo.getMarketType());
                futuresDaily.setSecuritiesType(symbolInfo.getSecuritiesType());
                MappingHTS.MAP_DAILY_FUTURES.accept(item, futuresDaily);
                futuresDaily.setCreatedAt(new Date());
                futuresDaily.setUpdatedAt(new Date());
                futuresDaily.setRefCode(symbolInfo.getRefCode());
                result.add(futuresDaily);
            }
            MarketFuturesDailyItem lastItem = data.getItems().get(data.getItems().size() - 1);
            if (lastItem.getDate().getValue().compareTo(fromDate) >= 0) {
                snd.getBaseDate().setValue(lastItem.getDate().getValue());
                Async.await(queryFuturesDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
            } else {
                log.info("Finish Crawl futuresDaily {} because < fromDate", symbolInfo.getCode());
            }

        } else {
            log.info("Finish Crawl Daily {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }


    public Observable<Boolean> downloadCWDaily(DownloadRequest downloadRequest) {
        log.info("download CWDaily: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.CW.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.CW.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list {0}", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlCWDaily(htsConnectionHandler, symbolInfoList, downloadRequest.getFromDate(), downloadRequest.getToDate()).thenAccept(aVoid -> {
                    log.info("Finished downloadCWDaily");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlCWDaily(HtsConnectionHandler htsConnectionHandler,
                                                       List<SymbolInfo> symbolInfoList, String fromDate, String toDate) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketCWDailySnd snd = new MarketCWDailySnd();
                snd.getCwCode().setValue(symbolInfo.getCode());
                snd.getIReadCount().setValue(100);
                snd.getBaseDate().setValue(toDate);
                List<SymbolDaily> result = new ArrayList<>();
                Async.await(queryCWDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                marketRepository.updateInBulk(result, SymbolDaily.class);
                log.info("{}  Finish Crawl CWDaily code: {} - size: {}", i, symbolInfo.getCode(), result.size());

            }
            log.info("Finish Crawl CWDaily");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryCWDaily(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo,
                                                 MarketCWDailySnd snd, List<SymbolDaily> result, String fromDate, String toDate) {
        log.info("snd: {}", snd);
        log.info("queryCWDaily: {}", symbolInfo.getCode());
        MarketCWDailyRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketCWDailyRcv.class));
        log.info("{} _ receive data {}", snd.getCwCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketCWDailyItem item = data.getItems().get(i);

                SymbolDaily cwDaily = new SymbolDaily();
                cwDaily.setId(symbolInfo.getCode() + "_" + item.getDate().getValue());
                cwDaily.setCode(symbolInfo.getCode());
                cwDaily.setMarketType(symbolInfo.getMarketType());
                cwDaily.setSecuritiesType(symbolInfo.getSecuritiesType());
                MappingHTS.MAP_DAILY_CW.accept(item, cwDaily);
                cwDaily.setCreatedAt(new Date());
                cwDaily.setUpdatedAt(new Date());
                result.add(cwDaily);
            }
            if (data.getStatus() != null) {
                MarketCWDailyItem lastItem = data.getItems().get(data.getItems().size() - 1);
                if (lastItem.getDate().getValue().compareTo(fromDate) >= 0) {
                    snd.getBaseDate().setValue(lastItem.getDate().getValue());
                    Async.await(queryCWDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                } else {
                    log.info("Finish Crawl cwDaily {} because < fromDate", symbolInfo.getCode());
                }
            } else {
                log.info("Finish Crawl Daily {} because status null", symbolInfo.getCode());
            }

        } else {
            log.info("Finish Crawl Daily {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }


    public Observable<Boolean> downloadStockForeignerDaily(DownloadRequest downloadRequest) {
        log.info("download ForeignerDaily: {}", downloadRequest);
        final List<SymbolInfo> symbolInfoList;
        List<String> symbolList = downloadRequest.getSymbolList();
        if (symbolList != null && symbolList.size() > 0) {
            symbolInfoList = symbolInfoRepository.findByTypeAndCodeIn(SymbolTypeEnum.STOCK.name(), symbolList);
        } else {
            symbolInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.STOCK.name());
        }

        PublishSubject<Boolean> subject = PublishSubject.create();
        AppConf.Account accountDownload = appConf.getAccountDownload();
        htsConnectionPool.acquire(accountDownload, (connection, err) -> {
            log.info("____________ acquire done!");
            if (err != null) {
                log.error("fail to login on get info list {0}", err);
            } else {
                log.info("____________ login success!");
                HtsConnectionHandler htsConnectionHandler = (HtsConnectionHandler) connection.getConnection();
                handleCrawlStockForeignerDaily(htsConnectionHandler, symbolInfoList, downloadRequest.getFromDate(), downloadRequest.getToDate()).thenAccept(aVoid -> {
                    log.info("Finished downloadStockForeignerDaily");
                    connection.release();
                    subject.onNext(true);
                });
            }
        });
        return subject;
    }

    private CompletableFuture<Void> handleCrawlStockForeignerDaily(HtsConnectionHandler htsConnectionHandler,
                                                                   List<SymbolInfo> symbolInfoList, String fromDate, String toDate) {
        try {
            for (int i = 0; i < symbolInfoList.size(); i++) {
                SymbolInfo symbolInfo = symbolInfoList.get(i);
                MarketStockForeignerSnd snd = new MarketStockForeignerSnd();
                snd.getStockCode().setValue(symbolInfo.getCode());
                snd.getIReadCount().setValue(100);
                snd.getBaseDate().setValue(Integer.parseInt(toDate));
                List<ForeignerDaily> result = new ArrayList<>();
                Async.await(queryStockForeignerDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                marketRepository.updateInBulk(result, ForeignerDaily.class);
                log.info("{}  Finish Crawl StockForeignerDaily code: {} - size: {}", i, symbolInfo.getCode(), result.size());

            }
            log.info("Finish Crawl StockForeignerDaily");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return CompletableFuture.completedFuture(null);
    }

    private CompletableFuture<Void> queryStockForeignerDaily(HtsConnectionHandler htsConnectionHandler, SymbolInfo symbolInfo,
                                                             MarketStockForeignerSnd snd, List<ForeignerDaily> result, String fromDate, String toDate) {
        log.info("snd: {}", snd);
        log.info("queryStockForeignerDaily: {}", symbolInfo.getCode());
        MarketStockForeignerRcv data = Async.await(htsConnectionHandler.sendMessageFuture(snd, MarketStockForeignerRcv.class));
        log.info("{} _ receive data {}", snd.getStockCode().getValue(), data.getItems().size());
        if (!data.getItems().isEmpty()) {
            for (int i = 0; i < data.getItems().size(); i++) {
                MarketStockForeignerItem item = data.getItems().get(i);

                ForeignerDaily foreignerDaily = new ForeignerDaily();
                foreignerDaily.setId(symbolInfo.getCode() + "_" + item.getDate().getValue());
                foreignerDaily.setCode(symbolInfo.getCode());
                MappingHTS.MAP_STOCK_FOREIGNER_DAILY.accept(item, foreignerDaily);
                foreignerDaily.setCreatedAt(new Date());
                foreignerDaily.setUpdatedAt(new Date());
                result.add(foreignerDaily);
            }
            if (data.getStatus() != null) {
                MarketStockForeignerItem lastItem = data.getItems().get(data.getItems().size() - 1);
                if (lastItem.getDate().getValue().compareTo(fromDate) >= 0) {
                    snd.getBaseDate().setValue(Integer.parseInt(lastItem.getDate().getValue()));
                    Async.await(queryStockForeignerDaily(htsConnectionHandler, symbolInfo, snd, result, fromDate, toDate));
                } else {
                    log.info("Finish Crawl foreignerDaily {} because < fromDate", symbolInfo.getCode());
                }
            } else {
                log.info("Finish Crawl Daily {} because status null", symbolInfo.getCode());
            }

        } else {
            log.info("Finish Crawl Daily {} because empty", symbolInfo.getCode());
        }
        return CompletableFuture.completedFuture(null);
    }

}
