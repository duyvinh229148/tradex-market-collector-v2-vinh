package com.techx.tradex.marketcollector.services;

import com.techx.tradex.marketcollector.configurations.AppConf;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Data
public class JobService {
    private static final Logger log = LoggerFactory.getLogger(JobService.class);
    private AppConf appConf;
    private CacheService cacheService;
    private RealTimeDataListenerService realTimeDataListenerService;
    private DownloadAutoDataService downloadAutoDataService;

    @Autowired
    public JobService(CacheService cacheService, RealTimeDataListenerService realTimeDataListenerService,
                      DownloadAutoDataService downloadAutoDataService, AppConf appConf) {
        this.appConf = appConf;
        this.cacheService = cacheService;
        this.realTimeDataListenerService = realTimeDataListenerService;
        this.downloadAutoDataService = downloadAutoDataService;
    }

    // Internal Job for Project
    @Scheduled(cron = "${app.schedulers.resetCacheMapLastTradingVolume}")
    public void resetCacheMapLastTradingVolume() {
        cacheService.getMapLastTradingVolume().clear();
    }

    @Scheduled(cron = "${app.schedulers.resetCacheMapSequence}")
    public void resetCacheMapSequence() {
        cacheService.resetSequence();
    }

    @Scheduled(cron = "${app.schedulers.checkQueueQuoteTimeOut}")
    public void checkQueueQuoteTimeOut() {
        realTimeDataListenerService.checkQueueQuoteTimeOut();
    }

    @Scheduled(cron = "${app.schedulers.startRealtime1st}")
    public void startRealtime1st() {
        realTimeDataListenerService.run();
    }

    @Scheduled(cron = "${app.schedulers.stopRealtime1st}")
    public void stopRealtime1st() {
        realTimeDataListenerService.stop();
    }

    @Scheduled(cron = "${app.schedulers.startRealtime2nd}")
    public void startRealtime2nd() {
        realTimeDataListenerService.run();
    }

    @Scheduled(cron = "${app.schedulers.stopRealtime2nd}")
    public void stopRealtime2nd() {
        realTimeDataListenerService.stop();
    }

    @Scheduled(cron = "${app.schedulers.backupAutoData}")
    public void backupAutoData() {
        if (appConf.isEnableBackup()) {
            downloadAutoDataService.backupAutoData();
        }
    }
}
