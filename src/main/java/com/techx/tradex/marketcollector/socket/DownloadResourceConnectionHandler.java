package com.techx.tradex.marketcollector.socket;

import com.techx.tradex.common.exceptions.GeneralException;
import com.techx.tradex.htsconnection.socket.LoginData;
import com.techx.tradex.htsconnection.socket.message.CNV;
import com.techx.tradex.htsconnection.socket.message.Constant;
import com.techx.tradex.htsconnection.socket.message.Packet;
import com.techx.tradex.htsconnection.socket.message.receive.BlockDataRcv;
import com.techx.tradex.htsconnection.socket.message.receive.DefinedTypeItem;
import com.techx.tradex.htsconnection.socket.message.receive.DefinedTypeRcv;
import com.techx.tradex.htsconnection.socket.message.receive.DownloadDataRcv;
import com.techx.tradex.htsconnection.socket.message.send.DefinedTypeSnd;
import com.techx.tradex.htsconnection.socket.message.send.DownloadDataSnd;
import com.techx.tradex.htsconnection.socket.nonblocking.BaseHtsConnectionHandler;
import com.techx.tradex.htsconnection.socket.nonblocking.ResultWaiter;
import com.techx.tradex.htsconnection.socket.nonblocking.SocketClient;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.List;

public class DownloadResourceConnectionHandler extends BaseHtsConnectionHandler<String> {
    private static final Logger log = LoggerFactory.getLogger(DownloadResourceConnectionHandler.class);
    private static final int INDEX_REQUEST_KEY = -2;

    private List<DefinedTypeItem> items = new ArrayList<>();
    private String currentFileName;
    private File currentFile;
    private FileOutputStream fos;
    private PublishSubject<String> publishSubject;
    private boolean isClosed = false;

    public DownloadResourceConnectionHandler(String connectionId, LoginData loginData, Selector selector) {
        super(connectionId, loginData, selector);
    }

    public Observable<String> start() throws IOException {
        publishSubject = PublishSubject.create();
        requestKeyMap.put(INDEX_REQUEST_KEY, new ResultWaiter<>(publishSubject));
        log.info("create connection");
        this.data = new SocketClient(this.selector, this.loginData.getHost(), this.loginData.getLoginPort(), this);
        this.data.start();
        return publishSubject;
    }

    @Override
    public void connected(SocketClient socketClient, boolean success) {
        log.info("connected {}", success);
        if (success) {
            initBeforeLogin();
        }
    }

    private void initBeforeLogin() {
        log.info("initBeforeLogin");
        DefinedTypeSnd snd = new DefinedTypeSnd();
        snd.getSecCode().setValue(this.loginData.getCode());
        snd.getMediaType().setValue("08");
        snd.getLanguage().setValue(Constant.Language);
        this.sendMessage(snd, DefinedTypeRcv.class).subscribe(
                data -> {/*already handle in process message*/},
                err -> {
                    if (!isClosed) {
                        throwResponse(new GeneralException().source(err), INDEX_REQUEST_KEY);
                    }
                }
        );
    }


    private void downloadResource(DefinedTypeItem item) {
        DownloadDataSnd snd = new DownloadDataSnd();
        snd.getSeqNo().setValue(item.getSeqNo().getValue());
        this.sendMessage(snd, DownloadDataRcv.class).subscribe(
                data -> {/*already handle in process message*/},
                err -> {
                    if (!isClosed) {
                        throwResponse(new GeneralException().source(err), INDEX_REQUEST_KEY);
                    }
                }
        );
    }

    private void nextFile() {
        if (!this.items.isEmpty()) {
            DefinedTypeItem currentItem = this.items.get(this.items.size() - 1);
            this.downloadResource(currentItem);
            this.currentFileName = FilenameUtils.getName(currentItem.getFilePath().getValue());
            this.items.remove(this.items.size() - 1);
        } else {
            this.publishSubject.onCompleted();
        }
    }

    @Override
    public void close() {
        this.isClosed = true;
        super.close();
    }

    @Override
    protected void processMessage(Packet packet) {
        log.trace("processMessage: {}", packet);
        packet.setConnectionHandler(this);

        if (packet instanceof DefinedTypeRcv) {
            if (packet.isError()) {
                this.throwResponse(new GeneralException(), INDEX_REQUEST_KEY);
                return;
            }
            if (((DefinedTypeRcv) packet).getCont().getValue() == 11) {
                this.nextFile();
            } else {
                this.items.addAll(((DefinedTypeRcv) packet).getFiles());
            }
        } else if (packet instanceof DownloadDataRcv) {
            if (packet.isError()) {
                this.throwResponse(new GeneralException(), INDEX_REQUEST_KEY);
            }
            this.currentFile = new File(this.currentFileName);
            try {
                this.fos = new FileOutputStream(this.currentFile);
                System.out.println("create fos");
            } catch (IOException e) {
                throw new GeneralException().source(e);
            }
        } else if (packet instanceof BlockDataRcv) {
            if (packet.isError()) {
                this.throwResponse(new GeneralException(), INDEX_REQUEST_KEY);
            }
            try {
                System.out.println((fos == null) + "-" + (((BlockDataRcv) packet).getData() == null));
                fos.write(((BlockDataRcv) packet).getData());
            } catch (IOException e) {
                throw new GeneralException().source(e);
            }
        } else if (packet instanceof CNV) {
            if (((CNV) packet).getErrorCode().getValue() > 0) {
                throwResponse(new GeneralException(), packet.getRequestKey().getValue());
                return;
            }
            if (((CNV) packet).getTypeID().getValue() == 121) {
                try {
                    fos.close();
                } catch (IOException e) {
                    throw new GeneralException().source(e);
                }
                this.publishSubject.onNext(this.currentFileName);
                this.nextFile();
            }
        } else {
            if (packet.getRequestKey().getValue() == 0) {
                return;
            }
            this.callbackResponse(packet, packet.getRequestKey().getValue());
        }
    }
}