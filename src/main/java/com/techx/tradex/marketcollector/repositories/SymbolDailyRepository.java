package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.common.utils.TimeUtils;
import com.techx.tradex.marketcollector.model.db.SymbolDaily;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SymbolDailyRepository extends MongoRepository<SymbolDaily, String> {
    @Query("{'date': {$gte: ?0 , $lte: ?1}}")
    List<SymbolDaily> findByDateRange(Date startOfDate, Date endOfDate);

    default List<SymbolDaily> findByDate(Date date) {
        return findByDateRange(TimeUtils.getStartOfDate(date), TimeUtils.getEndOfDate(date));
    }
}
