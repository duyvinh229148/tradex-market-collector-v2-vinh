package com.techx.tradex.marketcollector.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.marketcollector.model.db.MappingMongo;
import com.techx.tradex.marketcollector.model.db.SymbolQuote;
import com.techx.tradex.marketcollector.model.db.SymbolQuoteMinute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class MarketRepository {
    private MongoTemplate mongoTemplate;
    private ObjectMapper objectMapper;
    private final int BATCH_SIZE = 1000;

    @Autowired
    public MarketRepository(MongoTemplate mongoTemplate, ObjectMapper objectMapper) {
        this.mongoTemplate = mongoTemplate;
        this.objectMapper = objectMapper;
    }


    public <E extends MappingMongo> void updateInBulk(List<E> dataList, Class<E> clazz) {
        updateInBulkNonCheckClazz(dataList, clazz);
    }

    public <E extends MappingMongo> void updateInBulkNonCheckClazz(List<E> dataList, Class clazz) {
        if (dataList == null || dataList.isEmpty()) {
            return;
        }
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, clazz);
        int count = 0;

        for (int i = 0; i < dataList.size(); i++) {
            MappingMongo mappingMongo = dataList.get(i);
            Map<String, Object> map = mappingMongo.toMapUpdate();
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(mappingMongo.getId());
            query.addCriteria(criteria);
            Update update = new Update();
            map.forEach((key, value) -> update.set(key, value));
            bulkOps.upsert(query, update);
            count++;
            if (count == BATCH_SIZE) {
                bulkOps.execute();
                count = 0;
            }
        }
        if (count > 0) {
            bulkOps.execute();
        }
    }

    public List<SymbolQuoteMinute> groupSymbolQuoteMinute(String code) {
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE, Sort.Direction.ASC, "tradingVolume");
        MatchOperation matchCode = Aggregation.match(Criteria.where("code").is(code));

        ProjectionOperation project1 = Aggregation.project("code", "refCode", "open", "high", "low", "last", "tradingValue", "tradingVolume", "matchingVolume",
                "date").and(DateOperators.dateOf("date").hour()).as("hour").and(DateOperators.dateOf("date").minute()).as("minute")
                .and(DateOperators.dateOf("date").toString("%Y%m%d%H%M")).as("datetime");
        ProjectionOperation project2 = Aggregation.project("code", "refCode", "open", "high", "low", "last", "tradingValue", "tradingVolume", "matchingVolume",
                "date").and(StringOperators.Concat.valueOf("code").concat("_").concatValueOf("datetime")).as("id");
        SortOperation sortByTradingVolume = Aggregation.sort(pageable.getSort());
        GroupOperation groupCode = Aggregation.group("id")
                .last("code").as("code")
                .last("refCode").as("refCode")
                .last("date").as("date")
                .first("last").as("open")
                .max("last").as("high")
                .min("last").as("low")
                .last("last").as("last")
                .last("tradingValue").as("tradingValue")
                .last("tradingVolume").as("tradingVolume")
                .sum("matchingVolume").as("periodTradingVolume");

        SkipOperation skipOperation = Aggregation.skip(pageable.getOffset());
        LimitOperation limitOperation = Aggregation.limit(pageable.getPageSize());
        Aggregation aggregation = Aggregation.newAggregation(
                matchCode, project1, project2, sortByTradingVolume, groupCode, sortByTradingVolume, skipOperation, limitOperation
        );

        AggregationResults<SymbolQuoteMinute> groupResults
                = mongoTemplate.aggregate(aggregation, SymbolQuote.class, SymbolQuoteMinute.class);
        return groupResults.getMappedResults();
    }
}
