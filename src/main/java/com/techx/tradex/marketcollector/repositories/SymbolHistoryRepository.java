package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.marketcollector.model.db.SymbolHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SymbolHistoryRepository extends MongoRepository<SymbolHistory, String> {

}
