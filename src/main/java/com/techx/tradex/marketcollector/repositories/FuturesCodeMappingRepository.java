package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.marketcollector.model.db.FuturesCodeMapping;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FuturesCodeMappingRepository extends MongoRepository<FuturesCodeMapping, String> {

}
