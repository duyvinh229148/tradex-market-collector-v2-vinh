package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SymbolInfoRepository extends MongoRepository<SymbolInfo, String> {
    void removeByType(String type);

    List<SymbolInfo> findByTypeOrderByCode(String type);

    List<SymbolInfo> findByTypeAndCodeIn(String type, List<String> codes);
}
