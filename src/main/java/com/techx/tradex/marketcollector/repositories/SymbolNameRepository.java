package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.marketcollector.model.db.SymbolName;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SymbolNameRepository extends MongoRepository<SymbolName, String> {

}
