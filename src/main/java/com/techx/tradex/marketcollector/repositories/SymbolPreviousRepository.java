package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.marketcollector.model.db.SymbolPrevious;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SymbolPreviousRepository extends MongoRepository<SymbolPrevious, String> {
}
