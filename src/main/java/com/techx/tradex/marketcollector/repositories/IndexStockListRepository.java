package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.marketcollector.model.db.IndexStockList;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface IndexStockListRepository extends MongoRepository<IndexStockList, String> {
    List<IndexStockList> findByIndexCodeIn(List<String> codes);
}
