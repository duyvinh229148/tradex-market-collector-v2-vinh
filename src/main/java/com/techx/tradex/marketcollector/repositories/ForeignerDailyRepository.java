package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.common.utils.TimeUtils;
import com.techx.tradex.marketcollector.model.db.ForeignerDaily;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

public interface ForeignerDailyRepository extends MongoRepository<ForeignerDaily, String> {
    @Query("{'date': {$gte: ?0 , $lte: ?1}}")
    List<ForeignerDaily> findByDateRange(Date startOfDate, Date endOfDate);

    default List<ForeignerDaily> findByDate(Date date) {
        return findByDateRange(TimeUtils.getStartOfDate(date), TimeUtils.getEndOfDate(date));
    }
}
