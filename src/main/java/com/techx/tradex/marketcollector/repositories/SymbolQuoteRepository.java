package com.techx.tradex.marketcollector.repositories;

import com.techx.tradex.marketcollector.model.db.SymbolQuote;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SymbolQuoteRepository extends MongoRepository<SymbolQuote, String> {
    void removeByCode(String code);

}
