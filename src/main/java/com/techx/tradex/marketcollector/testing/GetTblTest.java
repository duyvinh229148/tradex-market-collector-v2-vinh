package com.techx.tradex.marketcollector.testing;

import com.techx.tradex.marketcollector.job.GetTblFilesJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "getTblTest")
public class GetTblTest implements ApplicationRunner {
    private GetTblFilesJob dailyGetTblFilesJob;

    @Autowired
    public GetTblTest(
            GetTblFilesJob dailyGetTblFilesJob
    ) {
        this.dailyGetTblFilesJob = dailyGetTblFilesJob;
    }

    @Override
    public void run(ApplicationArguments args) {
        this.dailyGetTblFilesJob.getTblFiles(true, true);
    }
}
