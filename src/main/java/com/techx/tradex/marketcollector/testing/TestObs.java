package com.techx.tradex.marketcollector.testing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class TestObs {
    public static void main(String[] args) {
        // PublishSubject<String> p = PublishSubject.create();
        // p.subscribe(
        //     s -> s.substring(0),
        //     err -> err.printStackTrace(),
        //     () -> System.out.println("complete")
        // );
        // p.onNext(null);
        // p.onCompleted();
        // p.onError(new Exception("sdf"));
        // p.onNext("b");

        // List<String> list = new ArrayList();
        // list.addAll(Arrays.asList("a", "b","c","d"));
        // for (String i : list) {
        //     if (i.equals("c")) {
        //         list.remove(i);
        //     }
        //     System.out.println(i);
        // }
        // System.out.println(list);

        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        try {
            System.out.println(sdf.parse("08:55"));
            from.setTime(sdf.parse("08:55"));
            to.setTime(sdf.parse("15:30"));
        } catch (Exception e) {
            System.exit(1);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(1970, 0, 1);
        // int hour = calendar.get(Calendar.HOUR_OF_DAY);
        // int minute = calendar.get(Calendar.MINUTE);
        // int timeInMs = (hour * 60 + minute) * 60000 - calendar.getTimeZone().getRawOffset();
        // calendar.setTimeInMillis(timeInMs);
        System.out.println(from);
        System.out.println(to);
        System.out.println(calendar);
        System.out.println(from.getTimeInMillis());
        System.out.println(to.getTimeInMillis());
        // System.out.println(hour);
        // System.out.println(minute);
        // System.out.println(timeInMs);
        System.out.println(calendar.getTimeInMillis());
    }
}