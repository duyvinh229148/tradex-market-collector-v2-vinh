package com.techx.tradex.marketcollector.testing;

import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.services.RequestSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(name = "testSendKafkaOrdered")
public class TestSendKafkaOrdered implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(TestSendKafkaOrdered.class);
    private RequestSender requestSender;
    private AppConf appConf;

    @Autowired
    public TestSendKafkaOrdered(
            RequestSender requestSender
            , AppConf appConf) {
        this.requestSender = requestSender;
        this.appConf = appConf;
    }

    @Override
    public void run(ApplicationArguments args) {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            //swallow
        }

//        Properties props = new Properties();
//        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, appConf.getKafkaUrl());
//        props.put(ProducerConfig.CLIENT_ID_CONFIG, appConf.getClusterId());
//        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName());
//        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

//        KafkaProducer<String, String> producer = new KafkaProducer<>(props);

        Long time = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            this.requestSender.sendMessageSafe("testSendKafkaOrdered", "no", i);
//            ProducerRecord<String, String> record = new ProducerRecord("testSendKafkaOrdered", i + "");
//            producer.send(record);
        }
        System.out.println("finished in " + (System.currentTimeMillis() - time));
    }
}
