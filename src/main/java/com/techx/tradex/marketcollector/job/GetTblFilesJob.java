package com.techx.tradex.marketcollector.job;

import com.ea.async.Async;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.constants.MarketTypeEnum;
import com.techx.tradex.common.model.kafka.BodyWrapper;
import com.techx.tradex.common.utils.DefaultUtils;
import com.techx.tradex.common.utils.TimeUtils;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.constants.Constants;
import com.techx.tradex.marketcollector.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.*;
import com.techx.tradex.marketcollector.model.response.MarketDataResponse;
import com.techx.tradex.marketcollector.model.response.RefreshNotify;
import com.techx.tradex.marketcollector.repositories.*;
import com.techx.tradex.marketcollector.services.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

@Component
public class GetTblFilesJob {
    private static final Logger log = LoggerFactory.getLogger(GetTblFilesJob.class);

    private AppConf appConf;
    private RequestSender requestSender;
    private HtsConnectionPool htsConnectionPool;
    private DownloadSymbolListService downloadSymbolListService;
    private DownloadInfoService downloadInfoService;
    private CacheService cacheService;
    private SymbolRepository symbolRepository;
    private SymbolPreviousRepository symbolPreviousRepository;
    private SymbolDailyRepository symbolDailyRepository;
    private SymbolHistoryRepository symbolHistoryRepository;
    private MarketRepository marketRepository;
    private SymbolInfoRepository symbolInfoRepository;
    private FuturesCodeMappingRepository futuresCodeMappingRepository;
    private SymbolNameRepository symbolNameRepository;
    private RedisService redisService;
    private SocketClusterService socketClusterService;
    private UploadService uploadService;

    @Autowired
    public GetTblFilesJob(AppConf appConf,
                          RequestSender requestSender,
                          HtsConnectionPool htsConnectionPool,
                          DownloadSymbolListService downloadSymbolListService,
                          DownloadInfoService downloadInfoService,
                          SymbolRepository stockRepository,
                          SymbolPreviousRepository symbolPreviousRepository,
                          SymbolDailyRepository symbolDailyRepository,
                          SymbolHistoryRepository stockHistoryRepository,
                          MarketRepository marketRepository,
                          CacheService cacheService,
                          SymbolInfoRepository securitiesInfoRepository,
                          FuturesCodeMappingRepository futuresCodeMappingRepository,
                          SymbolNameRepository symbolNameRepository,
                          RedisService redisService,
                          SocketClusterService socketClusterService,
                          UploadService uploadService) {
        this.appConf = appConf;
        this.requestSender = requestSender;
        this.htsConnectionPool = htsConnectionPool;
        this.downloadSymbolListService = downloadSymbolListService;
        this.downloadInfoService = downloadInfoService;
        this.cacheService = cacheService;
        this.symbolRepository = stockRepository;
        this.symbolPreviousRepository = symbolPreviousRepository;
        this.symbolDailyRepository = symbolDailyRepository;
        this.symbolHistoryRepository = stockHistoryRepository;
        this.marketRepository = marketRepository;
        this.symbolInfoRepository = securitiesInfoRepository;
        this.futuresCodeMappingRepository = futuresCodeMappingRepository;
        this.symbolNameRepository = symbolNameRepository;
        this.redisService = redisService;
        this.socketClusterService = socketClusterService;
        this.uploadService = uploadService;
    }

    public void getTblFiles() {
        this.getTblFiles(true, true);
    }

    public void getTblFiles(boolean isDownloadInfo, boolean isDownloadDaily) {
        requestSender.sendRequestNoResponseSafe(appConf.getTopics().getRealtime(), "/disableAutoData", new BodyWrapper(""));
        this.getTblFiles(0, isDownloadInfo, isDownloadDaily);
    }

    private CompletableFuture<Void> getTblFiles(int index, boolean isDownloadInfo, boolean isDownloadDaily) {
        if (index >= Constants.MAX_RETRY) {
            log.error("retry getTblFiles exceeded {} times Stop", Constants.MAX_RETRY);
            throw new RuntimeException("retry getTblFiles exceeded {} times Stop");
        }
        log.info("========================= START getTblFiles {}============================", index);
        Map<String, Symbol> mapSymbol = Async.await(downloadSymbolListService.downloadFuture());

        List<Symbol> symbolList = new ArrayList<>();
        mapSymbol.forEach((code, symbol) -> {
            if (!symbol.isValid()) {
                log.warn("symbol data is missing {}", symbol);
            }
            symbolList.add(symbol);
        });

        this.updateName(symbolList);
        log.info("updateName done!");
        this.updateIsHighlight(symbolList, appConf.getHighlightMap());
        log.info("updateIsHighlight done!");
        this.updateIndexStockList(symbolList);
        log.info("updateIndexStockList done!");
        this.updateSymbolListToDb(symbolList);
        log.info("updateSymbolListToDb done!");
        List<Symbol> indexList = symbolList.stream().filter(symbol -> symbol.getType() == SymbolTypeEnum.INDEX).collect(Collectors.toList());
        List<Symbol> stockList = symbolList.stream().filter(symbol -> symbol.getType() == SymbolTypeEnum.STOCK).collect(Collectors.toList());
        List<Symbol> cwList = symbolList.stream().filter(symbol -> symbol.getType() == SymbolTypeEnum.CW).collect(Collectors.toList());
        List<Symbol> futuresList = symbolList.stream().filter(symbol -> symbol.getType() == SymbolTypeEnum.FUTURES).collect(Collectors.toList());

        AppConf.Account account = this.appConf.getAccountDownload();
        HtsConnectionPool.Connection connection = Async.await(this.htsConnectionPool.acquireFuture(account, false));

        if (isDownloadDaily) {
            log.info("_______________start downloadDaily");
            Async.await(downloadInfoService.downloadSymbolDaily(indexList, stockList, cwList, futuresList, new Date(), connection));
            updateSymbolPrevious();
            redisService.reloadSymbolDaily();
            log.info("_______________finish downloadDaily");
        }
        if (isDownloadInfo) {
            log.info("_______________start downloadInfo");
            Async.await(downloadInfoService.downloadSymbolInfo(indexList, stockList, cwList, futuresList, connection));
            log.info("_______________finish downloadInfo");
            onFinishInitSymbolInfo(mapSymbol);
            log.info("_______________finish onFinishInitSymbolInfo");
        }

        requestSender.sendRequestNoResponseSafe(appConf.getTopics().getRealtime(), "/enableAutoData", new BodyWrapper(""));
        return CompletableFuture.completedFuture(null);
    }

    public void updateSymbolPrevious() {
        log.info("start updateSymbolPrevious");
        long t1 = System.currentTimeMillis();
//        Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.DATE, -1);
        Date date = new Date();
        List<SymbolDaily> symbolDailyList = symbolDailyRepository.findByDate(date);
        if (symbolDailyList.isEmpty()) {
            return;
        }
        List<SymbolPrevious> symbolPreviousList = symbolPreviousRepository.findAll();
        Map<String, SymbolPrevious> symbolPreviousMap = new HashMap<>();
        symbolPreviousList.forEach(symbolPrevious -> symbolPreviousMap.put(symbolPrevious.getCode(), symbolPrevious));

        List<SymbolPrevious> newSymbolPreviousList = new ArrayList<>();
        for (int i = 0; i < symbolDailyList.size(); i++) {
            SymbolDaily symbolDaily = symbolDailyList.get(i);
            SymbolPrevious symbolPrevious = symbolPreviousMap.get(symbolDaily.getCode());
            if (symbolPrevious == null) {
                symbolPrevious = new SymbolPrevious();
                symbolPrevious.setCode(symbolDaily.getCode());
                symbolPrevious.setClose(symbolDaily.getLast());
                symbolPrevious.setLastTradingDate(symbolDaily.getDate());
                symbolPrevious.setMarketType(symbolDaily.getMarketType());
                symbolPrevious.setType(symbolDaily.getType());
                symbolPrevious.setRefCode(symbolDaily.getRefCode());
                symbolPrevious.setNote("create new");
                symbolPrevious.setCreatedAt(new Date());
                symbolPrevious.setUpdatedAt(new Date());
            } else {
                if (TimeUtils.compareDateOnly(symbolPrevious.getLastTradingDate(), symbolDaily.getDate()) == 0) {
                    symbolPrevious.setClose(symbolDaily.getLast());
                    symbolPrevious.setUpdatedAt(new Date());
                    symbolPrevious.setNote("same date -> just update close");
                } else {
                    symbolPrevious.setPreviousClose(symbolPrevious.getClose());
                    symbolPrevious.setPreviousTradingDate(symbolPrevious.getLastTradingDate());
                    symbolPrevious.setClose(symbolDaily.getLast());
                    symbolPrevious.setLastTradingDate(symbolDaily.getDate());
                    symbolPrevious.setUpdatedAt(new Date());
                    symbolPrevious.setNote("different date -> update close/previousClose");
                }
            }
            newSymbolPreviousList.add(symbolPrevious);
        }
        marketRepository.updateInBulk(newSymbolPreviousList, SymbolPrevious.class);
        long t2 = System.currentTimeMillis();
        log.info("end updateSymbolPrevious, take: {} ms", (t2 - t1));
    }

    public void getTblFileOnly() {
        this.getTblFiles(0, false, false);
    }

    public void getInfoDataOnly() {
        requestSender.sendRequestNoResponseSafe(appConf.getTopics().getRealtime(), "/disableAutoData", new BodyWrapper(""));
        this.getTblFiles(0, true, false);
    }

    public void getDailyDataOnly() {
        requestSender.sendRequestNoResponseSafe(appConf.getTopics().getRealtime(), "/disableAutoData", new BodyWrapper(""));
        this.getTblFiles(0, false, true);
    }

    public void onFinishInitSymbolInfo(Map<String, Symbol> mapStockIndex) {
        updateFuturesCodeMapping();
        redisService.reloadSymbolInfo();
        if (appConf.isTblUploadEnable()) {
            try {
                uploadTbl(mapStockIndex);
            } catch (Exception e) {
                log.error("uploadToS3 failed!", e);
            }
        }
        if (appConf.isMarketDataUploadEnable()) {
            try {
                uploadMarketData();
                socketClusterService.publish(appConf.getChannels().getRefreshData(), RefreshNotify.getNotifyMarketData());
            } catch (Exception e) {
                log.error("uploadMarketData failed! ", e);
            }
        }
        try {
            requestSender.sendRequestNoResponseSafe(appConf.getTopics().getFix(), "/api/v1/fix/reloadSecurityList", new BodyWrapper(""));
            requestSender.sendMessageNoResponse(appConf.getTopics().getTblUpdate(), "/update", new BodyWrapper(""));
        } catch (Exception e) {
            log.error("notification onFinishInitSymbolInfo failed! {0}", e);
        }
    }

    private void uploadTbl(Map<String, Symbol> stockIndexMap) throws IOException {
        log.info("______start uploadTbl");
        ObjectMapper objectMapper = new ObjectMapper();
        String fileName = "master-" + DefaultUtils.DATETIME_FORMAT().format(new Date()) + "." + System.currentTimeMillis() + ".tbl";
        File file = new File(fileName);
        FileUtils.writeStringToFile(file, objectMapper.writeValueAsString(stockIndexMap.values()), StandardCharsets.UTF_8);
        log.info("fileName: {}", fileName);
        String url = uploadService.uploadFile(file, "tbl/" + fileName, false);
        log.info("URL tbl-file: {}", url);
        log.info("______finished uploadTbl");
    }

    private void updateSymbolListToDb(List<Symbol> symbolList) {
        log.info("___Start updateSymbolListToDb");
        Set<String> setSymbolCodeList = new HashSet<>();
        symbolList.forEach(symbol -> setSymbolCodeList.add(symbol.getCode()));

        // check closed symbol
        List<Symbol> oldSymbolList = this.symbolRepository.findAll();
        List<SymbolHistory> closedSymbolList = new ArrayList<>();
        oldSymbolList.forEach(symbol -> {
            if (!setSymbolCodeList.contains(symbol.getCode())) {
                closedSymbolList.add(SymbolHistory.fromSymbol(symbol));
                log.info("closedSymbolList: " + symbol.getCode());
            }
        });
        this.symbolRepository.deleteAll();
        log.info("deleteAll Symbols");
        this.symbolRepository.saveAll(symbolList);
        log.info("saveAll new Symbols");
        this.symbolHistoryRepository.saveAll(closedSymbolList);
        log.info("___Finish updateSymbolListToDb");
    }

    private void updateFuturesCodeMapping() {
        log.info("Start updateFuturesCodeMapping");
        boolean isChanged = false;
        List<FuturesCodeMapping> futuresCodeMappingList = futuresCodeMappingRepository.findAll();
        Map<String, FuturesCodeMapping> mapCode = new HashMap<>();
        futuresCodeMappingList.forEach((futuresCodeMapping ->
                mapCode.put(futuresCodeMapping.getRefCode(), futuresCodeMapping)
        ));

        Map<String, String> mapChange = new HashMap<>();
        List<SymbolInfo> futuresInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.FUTURES.name());
        for (int i = 0; i < futuresInfoList.size(); i++) {
            SymbolInfo futuresInfo = futuresInfoList.get(i);
            FuturesCodeMapping futuresCodeMapping = mapCode.get(futuresInfo.getRefCode());
            String newFuturesCode = futuresInfo.getCode();
            if (futuresCodeMapping == null) {
                futuresCodeMapping = new FuturesCodeMapping();
                futuresCodeMapping.setRefCode(futuresInfo.getRefCode());
                futuresCodeMapping.setCode(futuresInfo.getCode());
                futuresCodeMapping.setOldCode(futuresInfo.getCode());
                futuresCodeMapping.setUpdatedDate(new Date());
                futuresCodeMappingList.add(futuresCodeMapping);
                mapChange.put(futuresInfo.getCode(), newFuturesCode);
                isChanged = true;

            } else if (!newFuturesCode.equalsIgnoreCase(futuresCodeMapping.getCode())) {
                mapChange.put(StringUtils.defaultString(futuresCodeMapping.getCode(), futuresInfo.getCode()), newFuturesCode);
                futuresCodeMapping.setOldCode(futuresCodeMapping.getCode());
                futuresCodeMapping.setCode(newFuturesCode);
                futuresCodeMapping.setUpdatedDate(new Date());
                isChanged = true;
            }
        }

        if (isChanged) {
            try {
                // notify to user-service to update alarm, favoriteList
                requestSender.sendMessageNoResponse(appConf.getTopics().getUserUtility(), "/updateFuturesCodeMapping", mapChange);
            } catch (Exception ex) {
                log.error("error while notify updateFuturesCodeMapping! {0}", ex);
            }
            // update futuresCodeMapping and save to database.
            futuresCodeMappingRepository.deleteAll();
            futuresCodeMappingRepository.saveAll(futuresCodeMappingList);
        }
        log.info("Finished updateFuturesCodeMapping");
    }

    private void uploadMarketData() throws IOException {
        log.info("______start uploadMarketData");
        ObjectMapper objectMapper = new ObjectMapper();
        List<MarketDataResponse> marketDataResponseList = new ArrayList<>();
        List<SymbolPrevious> symbolPreviousList = symbolPreviousRepository.findAll();
        Map<String, SymbolPrevious> symbolPreviousMap = new HashMap<>();
        symbolPreviousList.forEach(symbolPrevious -> symbolPreviousMap.put(symbolPrevious.getCode(), symbolPrevious));

        List<SymbolInfo> stockInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.STOCK.name());
        List<SymbolInfo> futuresInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.FUTURES.name());
        futuresInfoList.sort((p1, p2) -> {
            if (p1.getCode().startsWith("VN") && p2.getCode().startsWith("GB")) {
                return -1;
            }
            return 0;
        });
        List<SymbolInfo> cwInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.CW.name());
        List<SymbolInfo> indexInfoList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.INDEX.name());
        indexInfoList.removeIf(symbolInfo -> !appConf.getIndexListActive().contains(symbolInfo.getRefCode())); // only keep active index
        indexInfoList.sort(Comparator.comparingInt(SymbolInfo::getIsHighlight));

        stockInfoList.forEach(symbolInfo -> marketDataResponseList.add(MarketDataResponse.fromSymbolInfo(symbolInfo, symbolPreviousMap.get(symbolInfo.getCode()))));
        futuresInfoList.forEach(symbolInfo -> marketDataResponseList.add(MarketDataResponse.fromSymbolInfo(symbolInfo, symbolPreviousMap.get(symbolInfo.getCode()))));
        cwInfoList.forEach(symbolInfo -> marketDataResponseList.add(MarketDataResponse.fromSymbolInfo(symbolInfo, symbolPreviousMap.get(symbolInfo.getCode()))));
        indexInfoList.forEach(symbolInfo -> marketDataResponseList.add(MarketDataResponse.fromSymbolInfo(symbolInfo, symbolPreviousMap.get(symbolInfo.getCode()))));
        log.info("finish preparing data to upload");
        // upload without gzip
        String fileName = appConf.getFileUploads().getSymbolStaticFile();
        File file = new File(fileName);
        FileUtils.writeStringToFile(file, objectMapper.writeValueAsString(marketDataResponseList), StandardCharsets.UTF_8);
        String url1 = uploadService.uploadFile(file, "market_data/" + fileName, false);
        log.info("URL market-data non-gzip: {}", url1);

        // upload with gzip
        fileName = appConf.getFileUploads().getSymbolStaticGzipFile();
        String url = uploadService.uploadGzip(gzipFile(file), "market_data/" + fileName, false);
        log.info("URL market-data gzip: {}", url);
        log.info("______finish uploadMarketData");
    }

    private void updateName(List<Symbol> symbolList) {
        try {
            log.info("start updateName ...");
            List<SymbolName> listSymbolName = symbolNameRepository.findAll();
            log.info("listSymbolName: {}", listSymbolName.size());
            Map<String, SymbolName> mapSymbolName = new HashMap<>();
            listSymbolName.forEach(symbolName ->
                    mapSymbolName.put(symbolName.getCode(), symbolName)
            );
            symbolList.forEach(symbol -> {
                SymbolName symbolName = mapSymbolName.getOrDefault(symbol.getCode(), null);
                if (symbolName != null) {
                    if (StringUtils.isNotBlank(symbolName.getName())) {
                        symbol.setName(symbolName.getName());
                    }
                    if (StringUtils.isNotBlank(symbolName.getNameEn())) {
                        symbol.setNameEn(symbolName.getNameEn());
                    }
                }
                if (StringUtils.isBlank(symbol.getName())) {
                    symbol.setName(symbol.getCode());
                }
                if (StringUtils.isBlank(symbol.getNameEn())) {
                    symbol.setName(symbol.getCode());
                }
            });
            log.info("finish updateName!");
        } catch (Exception ex) {
            log.error("Error while updateName {0}", ex);
        }
    }

    private void updateIndexStockList(List<Symbol> symbolList) {
        try {
            log.info("start updateIndexStockList ...");
            List<String> hnxStockList = new ArrayList<>();
            List<String> hoseStockList = new ArrayList<>();
            List<String> upcomStockList = new ArrayList<>();
            IndexStockList hnxIndexStockList = new IndexStockList("HNX", hnxStockList, new Date());
            IndexStockList vnIndexStockList = new IndexStockList("VN", hoseStockList, new Date());
            IndexStockList upcomIndexStockList = new IndexStockList("UPCOM", upcomStockList, new Date());
            symbolList.forEach(stock -> {
                if (stock.getMarketType().equals(MarketTypeEnum.HNX.name())) {
                    hnxStockList.add(stock.getValidCode());
                } else if (stock.getMarketType().equals(MarketTypeEnum.HOSE.name())) {
                    hoseStockList.add(stock.getValidCode());
                } else if (stock.getMarketType().equals(MarketTypeEnum.UPCOM.name())) {
                    upcomStockList.add(stock.getValidCode());
                }
            });
            marketRepository.updateInBulk(Arrays.asList(hnxIndexStockList, upcomIndexStockList, vnIndexStockList), IndexStockList.class);
            log.info("finish updateIndexStockList!");
        } catch (Exception ex) {
            log.error("Error while updateIndexStockList {0}", ex);
        }
    }

    private void updateIsHighlight(List<Symbol> symbolList, Map<String, Integer> mapHighlight) {
        try {
            log.info("start updateIsHighlight ...");
            symbolList.forEach(symbol -> {
                if (symbol.getType().equals(SymbolTypeEnum.INDEX)) {
                    symbol.setIsHighlight(mapHighlight.getOrDefault(symbol.getCode(), Constants.DEFAULT_HIGHLIGHT_NUMBER));
                }
            });
            log.info("finish updateIsHighlight!");
        } catch (Exception ex) {
            log.error("Error while updateIsHighlight {0}", ex);
        }
    }

    private static byte[] gzipFile(File file) throws IOException {
        try {
            byte[] bytes = Files.readAllBytes(file.toPath());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            GZIPOutputStream out = new GZIPOutputStream(baos);
            out.write(bytes, 0, bytes.length);
            // Complete the GZIP file
            out.finish();
            out.close();

            return baos.toByteArray();
        } catch (IOException e) {
            throw e;
        }
    }


    @org.springframework.scheduling.annotation.Async
    public void shutdown() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
