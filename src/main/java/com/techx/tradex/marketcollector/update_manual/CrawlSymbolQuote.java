package com.techx.tradex.marketcollector.update_manual;

import com.ea.async.Async;
import com.techx.tradex.common.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import com.techx.tradex.marketcollector.model.request.DownloadRequest;
import com.techx.tradex.marketcollector.repositories.SymbolInfoRepository;
import com.techx.tradex.marketcollector.services.DownloadAutoDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(name = "crawSymbolQuote")
public class CrawlSymbolQuote implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(CrawlSymbolQuote.class);

    @Autowired
    private SymbolInfoRepository symbolInfoRepository;
    @Autowired
    private DownloadAutoDataService downloadAutoDataService;

    public void run(ApplicationArguments args) {
        Async.init();
        crawlSymbolQuote();
    }

    private void crawlSymbolQuote() {
        DownloadRequest downloadRequest = new DownloadRequest();
//        List<SymbolInfo> stockList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.STOCK.name());
//        stockList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("ROS"));
//        downloadAutoDataService.downloadStockQuote(stockList).subscribe(aBoolean -> {
//            System.out.println("crawlStockQuote done!");
//        });

//        List<SymbolInfo> indexList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.INDEX.name());
//        indexList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("HNX"));
//        downloadAutoDataService.downloadIndexQuote(indexList).subscribe(aBoolean -> {
//            System.out.println("crawlIndexQuote done!");
//        });


//        List<SymbolInfo> cwList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.CW.name());
//        cwList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("CPNJ1902"));
//        downloadAutoDataService.downloadCWQuote(cwList).subscribe(aBoolean -> {
//            System.out.println("crawlCWQuote done!");
//        });

        List<SymbolInfo> futuresList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.FUTURES.name());
        futuresList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("VN30F2001"));
        downloadRequest.setSymbolList(futuresList.stream().map(symbolInfo -> symbolInfo.getCode()).collect(Collectors.toList()));
        downloadAutoDataService.downloadFuturesQuote(downloadRequest).subscribe(aBoolean -> {
            System.out.println("crawlFuturesQuote done!");
        });
    }
}
