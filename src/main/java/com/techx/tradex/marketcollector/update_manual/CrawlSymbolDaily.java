package com.techx.tradex.marketcollector.update_manual;

import com.ea.async.Async;
import com.techx.tradex.common.constants.SymbolTypeEnum;
import com.techx.tradex.marketcollector.model.db.SymbolInfo;
import com.techx.tradex.marketcollector.model.request.DownloadRequest;
import com.techx.tradex.marketcollector.repositories.SymbolInfoRepository;
import com.techx.tradex.marketcollector.services.DownloadPeriodDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(name = "crawSymbolDaily")
public class CrawlSymbolDaily implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(CrawlSymbolDaily.class);

    @Autowired
    private SymbolInfoRepository symbolInfoRepository;
    @Autowired
    private DownloadPeriodDataService downloadPeriodDataService;

    public void run(ApplicationArguments args) {
        Async.init();
        crawlSymbolDaily();
    }

    private void crawlSymbolDaily() {
        DownloadRequest downloadRequest = new DownloadRequest();
        downloadRequest.setFromDate("20191201");
        downloadRequest.setToDate("20200103");
//        List<SymbolInfo> stockList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.STOCK.name());
//        stockList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("AAA"));
//        downloadPeriodDataService.downloadStockDaily(stockList, fromDate, toDate).subscribe(aBoolean -> {
//            System.out.println("downloadStockDaily done!");
//        });

//        List<SymbolInfo> indexList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.INDEX.name());
//        indexList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("HNX"));
//        downloadPeriodDataService.downloadIndexDaily(indexList, fromDate, toDate).subscribe(aBoolean -> {
//            System.out.println("downloadIndexDaily done!");
//        });

//        List<SymbolInfo> futuresList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.FUTURES.name());
//        futuresList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("VN30F2001"));
//        downloadPeriodDataService.downloadFuturesDaily(futuresList, fromDate, toDate).subscribe(aBoolean -> {
//            System.out.println("downloadFuturesDaily done!");
//        });

//        List<SymbolInfo> cwList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.CW.name());
//        cwList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("CPNJ1902"));
//        downloadPeriodDataService.downloadCWDaily(cwList, fromDate, toDate).subscribe(aBoolean -> {
//            System.out.println("downloadCWDaily done!");
//        });

        List<SymbolInfo> foreignerList = symbolInfoRepository.findByTypeOrderByCode(SymbolTypeEnum.STOCK.name());
        foreignerList.removeIf(symbolInfo -> !symbolInfo.getCode().equals("AAA"));
        downloadRequest.setSymbolList(foreignerList.stream().map(symbolInfo -> symbolInfo.getCode()).collect(Collectors.toList()));
        downloadPeriodDataService.downloadStockForeignerDaily(downloadRequest).subscribe(aBoolean -> {
            System.out.println("downloadStockForeignerDaily done!");
        });
    }
}
