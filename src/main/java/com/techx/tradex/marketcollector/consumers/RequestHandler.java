package com.techx.tradex.marketcollector.consumers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.kafka.KafkaRequestHandler;
import com.techx.tradex.common.model.kafka.Message;
import com.techx.tradex.common.model.kafka.MessageTypeEnum;
import com.techx.tradex.marketcollector.configurations.AppConf;
import com.techx.tradex.marketcollector.job.GetTblFilesJob;
import com.techx.tradex.marketcollector.model.request.DownloadRequest;
import com.techx.tradex.marketcollector.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.Observable;

@Service
public class RequestHandler extends KafkaRequestHandler {
    public static final Logger log = LoggerFactory.getLogger(RequestHandler.class);

    private GetTblFilesJob dailyGetTblFilesJob;
    private AppConf appConf;
    private RealTimeDataListenerService realTimeDataListenerService;
    private MonitorLostBidOffer monitorLostBidOffer;
    private ObjectMapper objectMapper;
    private CacheService cacheService;
    private DownloadAutoDataService downloadAutoDataService;
    private DownloadPeriodDataService downloadPeriodDataService;

    @Autowired
    public RequestHandler(
            ObjectMapper objectMapper,
            GetTblFilesJob dailyGetTblFilesJob,
            AppConf appConf,
            RealTimeDataListenerService realTimeDataListenerService,
            MonitorLostBidOffer monitorLostBidOffer,
            CacheService cacheService,
            DownloadAutoDataService downloadAutoDataService,
            DownloadPeriodDataService downloadPeriodDataService
    ) {
        super(objectMapper, appConf.getKafkaBootstraps(), appConf.getClusterId(), 5);
        this.dailyGetTblFilesJob = dailyGetTblFilesJob;
        this.appConf = appConf;
        this.realTimeDataListenerService = realTimeDataListenerService;
        this.monitorLostBidOffer = monitorLostBidOffer;
        this.objectMapper = objectMapper;
        this.cacheService = cacheService;
        this.downloadAutoDataService = downloadAutoDataService;
        this.downloadPeriodDataService = downloadPeriodDataService;
    }

    @Override
    protected Object handle(Message message) {
        try {
            if (message.getMessageType() == MessageTypeEnum.REQUEST) {
                if (message.getUri().equalsIgnoreCase("/job/getTblFile")) {
                    dailyGetTblFilesJob.getTblFiles();
                } else if (message.getUri().equalsIgnoreCase("/job/getTblFileOnly")) {
                    dailyGetTblFilesJob.getTblFileOnly();
                } else if (message.getUri().equalsIgnoreCase("/job/getInfoDataOnly")) {
                    dailyGetTblFilesJob.getInfoDataOnly();
                } else if (message.getUri().equalsIgnoreCase("/job/getDailyDataOnly")) {
                    dailyGetTblFilesJob.getDailyDataOnly();
                } else if (message.getUri().equalsIgnoreCase("/job/exit")) {
                    log.warn("Shutdown service");
                    dailyGetTblFilesJob.shutdown();
                } else if (message.getUri().equalsIgnoreCase("/job/realtime/start")) {
                    log.warn("start realtime");
                    realTimeDataListenerService.run();
                } else if (message.getUri().equalsIgnoreCase("/job/realtime/stop")) {
                    log.warn("stop realtime");
                    realTimeDataListenerService.stop();
                } else if (message.getUri().equalsIgnoreCase("/job/printLostBidOfferCodes")) {
                    log.warn("/job/printLostBidOfferCodes");
                    monitorLostBidOffer.getLostCodes().forEach(log::warn);
                } else if (message.getUri().equalsIgnoreCase("/job/enableMonitorLostBidOfferCodes")) {
                    appConf.setEnableMonitorLostBidOffer(true);
                } else if (message.getUri().equalsIgnoreCase("/job/disableMonitorLostBidOfferCodes")) {
                    appConf.setEnableMonitorLostBidOffer(false);
                } else if (message.getUri().equalsIgnoreCase("/job/resetSequence")) {
                    cacheService.resetSequence();
                } else if (message.getUri().equalsIgnoreCase("/job/resetLastTradingVolume")) {
                    cacheService.getMapLastTradingVolume().clear();
                } else if (message.getUri().equalsIgnoreCase("/job/downloadStockDaily")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadPeriodDataService.downloadStockDaily(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadIndexDaily")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadPeriodDataService.downloadIndexDaily(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadFuturesDaily")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadPeriodDataService.downloadFuturesDaily(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadCWDaily")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadPeriodDataService.downloadCWDaily(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadForeignerDaily")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadPeriodDataService.downloadStockForeignerDaily(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadStockQuote")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadAutoDataService.downloadStockQuote(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadIndexQuote")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadAutoDataService.downloadIndexQuote(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadFuturesQuote")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadAutoDataService.downloadFuturesQuote(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/downloadCwQuote")) {
                    DownloadRequest downloadRequest = Message.getData(objectMapper, message, DownloadRequest.class);
                    downloadAutoDataService.downloadCWQuote(downloadRequest);
                } else if (message.getUri().equalsIgnoreCase("/job/backupAutoData")) {
                    downloadAutoDataService.backupAutoData();
                }
            }
        } catch (Exception e) {
            return Observable.error(e);
        }
        return false;
    }
}
