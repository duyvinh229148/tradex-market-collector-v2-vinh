package com.techx.tradex.marketcollector.daos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.techx.tradex.common.constants.RedisDataTypeEnum;
import com.techx.tradex.marketcollector.constants.Constants;
import com.techx.tradex.marketcollector.model.db.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class RedisDao {
    private static final Logger log = LoggerFactory.getLogger(RedisDao.class);

    private RedisTemplate<String, String> redisTemplate;
    private ObjectMapper objectMapper;

    public RedisDao(RedisTemplate<String, String> redisTemplate, ObjectMapper objectMapper) {
        this.redisTemplate = redisTemplate;
        this.objectMapper = objectMapper;
    }

    public void setSymbolInfo(SymbolInfo symbolInfo) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        try {
            hashOperations.put(Constants.REDIS_KEY_SYMBOL_INFO, symbolInfo.getCode(), objectToString(symbolInfo));
        } catch (JsonProcessingException e) {
            log.error("error while setSymbolInfo: {0}", e);
        }
    }

    public void setSymbolDaily(SymbolDaily symbolDaily) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        try {
            hashOperations.put(Constants.REDIS_KEY_SYMBOL_DAILY, symbolDaily.getCode(), objectToString(symbolDaily));
        } catch (JsonProcessingException e) {
            log.error("error while setSymbolDaily: {0}", e);
        }
    }

    public void setForeignerDaily(ForeignerDaily foreignerDaily) {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        try {
            hashOperations.put(Constants.REDIS_KEY_FOREIGNER_DAILY, foreignerDaily.getCode(), objectToString(foreignerDaily));
        } catch (JsonProcessingException e) {
            log.error("error while setForeignerDaily: {0}", e);
        }
    }

    public Long clearSymbolInfo() {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        Set<String> setSymbolInfoKeys = hashOperations.keys(Constants.REDIS_KEY_SYMBOL_INFO);
        if (setSymbolInfoKeys.size() == 0) {
            return 0L;
        }
        String[] arrSymbolInfoKeys = new String[setSymbolInfoKeys.size()];
        setSymbolInfoKeys.toArray(arrSymbolInfoKeys);
        return hashOperations.delete(Constants.REDIS_KEY_SYMBOL_INFO, arrSymbolInfoKeys);
    }

    public Long clearSymbolDaily() {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        Set<String> setSymbolDailyKeys = hashOperations.keys(Constants.REDIS_KEY_SYMBOL_DAILY);
        if (setSymbolDailyKeys.size() == 0) {
            return 0L;
        }
        String[] arrSymbolDailyKeys = new String[setSymbolDailyKeys.size()];
        setSymbolDailyKeys.toArray(arrSymbolDailyKeys);
        return hashOperations.delete(Constants.REDIS_KEY_SYMBOL_DAILY, arrSymbolDailyKeys);
    }

    public Long clearForeignerDaily() {
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        Set<String> setForeignerDailyKeys = hashOperations.keys(Constants.REDIS_KEY_FOREIGNER_DAILY);
        if (setForeignerDailyKeys.size() == 0) {
            return 0L;
        }
        String[] arrForeignerDailyKeys = new String[setForeignerDailyKeys.size()];
        setForeignerDailyKeys.toArray(arrForeignerDailyKeys);
        return hashOperations.delete(Constants.REDIS_KEY_FOREIGNER_DAILY, arrForeignerDailyKeys);
    }

    public List<SymbolInfo> getAllSymbolInfo() {
        List<SymbolInfo> symbolInfoList = new ArrayList<>();
        HashOperations<String, String, String> hashOperations = redisTemplate.opsForHash();
        try {
            List<String> listSymbolInfoStr = hashOperations.values(Constants.REDIS_KEY_SYMBOL_INFO);
            for (String data : listSymbolInfoStr) {
                SymbolInfo symbolInfo = stringToObject(data, SymbolInfo.class);
                symbolInfoList.add(symbolInfo);
            }
        } catch (IOException e) {
            log.error("error while getAllSymbolInfo: {0}", e);
        }
        return symbolInfoList;
    }

    private <E> String objectToString(E data) throws JsonProcessingException {
        if (data == null) {
            return RedisDataTypeEnum.NULL.getType();
        } else if (data instanceof Boolean) {
            return RedisDataTypeEnum.BOOLEAN.getType() + data;
        } else if (data instanceof String) {
            return RedisDataTypeEnum.STRING.getType() + data;
        } else if (data instanceof Number) {
            return RedisDataTypeEnum.NUMBER.getType() + data;
        } else {
            return RedisDataTypeEnum.OBJECT.getType() + objectMapper.writeValueAsString(data);
        }
    }

    private <E> E stringToObject(String data, Class<E> clazz) throws IOException {
        String type = data.substring(0, 1);
        if (type.equals(RedisDataTypeEnum.NULL.getType())) {
            return null;
        }
        String value = data.substring(1);
        if (type.equals(RedisDataTypeEnum.BOOLEAN.getType())) {
            return (E) Boolean.valueOf(value);
        } else if (type.equals(RedisDataTypeEnum.STRING.getType())) {
            return (E) value;
        } else if (type.equals(RedisDataTypeEnum.NUMBER.getType())) {
            return (E) Double.valueOf(value);
        } else {
            return objectMapper.readValue(value, clazz);
        }
    }

    public Long clearSymbolQuote(String symbol) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        String key = Constants.REDIS_KEY_SYMBOL_QUOTE + "_" + symbol;
        Long size = listOperations.size(key);
        if (size != null && size > 0) {
            listOperations.trim(key, 1, 0);
            return size;
        }
        return 0L;
    }

    public void addSymbolQuote(SymbolQuote symbolQuote) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        try {
            listOperations.leftPush(Constants.REDIS_KEY_SYMBOL_QUOTE + "_" + symbolQuote.getCode(), objectToString(symbolQuote));
        } catch (JsonProcessingException e) {
            log.error("error while addSymbolQuote: {0}", e);
        }
    }

    public Long clearSymbolQuoteMinute(String symbol) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        String key = Constants.REDIS_KEY_SYMBOL_QUOTE_MINUTE + "_" + symbol;
        Long size = listOperations.size(key);
        if (size != null && size > 0) {
            listOperations.trim(key, 1, 0);
            return size;
        }
        return 0L;
    }

    public void addSymbolQuoteMinute(SymbolQuoteMinute symbolQuoteMinute) {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        try {
            listOperations.leftPush(Constants.REDIS_KEY_SYMBOL_QUOTE_MINUTE + "_" + symbolQuoteMinute.getCode(), objectToString(symbolQuoteMinute));
        } catch (JsonProcessingException e) {
            log.error("error while addSymbolQuoteMinute: {0}", e);
        }
    }
}
