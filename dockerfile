FROM localhost:5000/ubuntu-w-java
RUN mkdir -p /app/logs
COPY /target/#{bamboo.jar.name} /app/
COPY application.yml.dl /app/application.yml
WORKDIR /app
ENTRYPOINT java -Xmx1G -jar #{bamboo.jar.name}